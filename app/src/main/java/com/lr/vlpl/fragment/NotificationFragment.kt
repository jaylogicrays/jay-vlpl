package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.adapter.NotificationAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.StickyHeaderLayout
import com.lr.vlpl.databinding.FragmentNotificationBinding
import com.lr.vlpl.model.Notification
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.NotificationViewModel

class NotificationFragment :
    BaseFragment<FragmentNotificationBinding, NotificationViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewModel: NotificationViewModel
    lateinit var viewBinding: FragmentNotificationBinding
    private lateinit var notificationAdapter: NotificationAdapter

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentNotificationBinding,
        viewModel: NotificationViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        getNotification()
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
        BaseApplication.getInstance().notificationUpdate.set(false)
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentNotificationBinding
        get() = FragmentNotificationBinding::inflate

    override val bindingViewModel: Class<NotificationViewModel>
        get() = NotificationViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun getNotification() {
        viewModel.notificationResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    viewBinding.rvNotificationAll.hideShimmerAdapter()
                    response.data!!.let { data ->
                        setAdapter(data)
                        notificationAdapter.submitList(data)
                        viewBinding.isEmptyList = data.isEmpty()
                    }
                }
                is Resource.Error -> {
                    viewBinding.rvNotificationAll.hideShimmerAdapter()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    viewBinding.rvNotificationAll.showShimmerAdapter()
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getNotification()
                    }
                }
            }
        }
    }

    private fun setAdapter(data: List<Notification>) {
        val sectionItemDecoration = StickyHeaderLayout(
            0,
            true,
            getSectionCallback(data)
        )
        viewBinding.rvNotificationAll.addItemDecoration(sectionItemDecoration)
        notificationAdapter = NotificationAdapter()
        viewBinding.rvNotificationAll.adapter = notificationAdapter
    }

    private fun getSectionCallback(people: List<Notification>): StickyHeaderLayout.SectionCallback {
        return object : StickyHeaderLayout.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return (position == 0 || people[position].date[0] != people[position - 1].date[0])
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position].date
            }
        }
    }
}