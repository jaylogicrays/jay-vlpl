package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.ArticleAdapter
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.FragmentArticlesBinding
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ArticleViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ArticlesFragment : BaseFragment<FragmentArticlesBinding, ArticleViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentArticlesBinding
    private lateinit var viewModel: ArticleViewModel
    private lateinit var adapterArticle: ArticleAdapter
    private var isLoad = true

    override fun onViewCreated(
        instance: Bundle?, viewBinding: FragmentArticlesBinding,
        viewModel: ArticleViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        setAdapter()
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    private fun setAdapter() {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvHealthArticle.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvHealthArticle.layoutManager = GridLayoutManager(requireContext(), 2)
        adapterArticle = ArticleAdapter()

        lifecycleScope.launchWhenStarted {
            adapterArticle.loadStateFlow.collectLatest { loadState ->
                viewBinding.appendProgress.isVisible = loadState.source.append is LoadState.Loading
                if (loadState.refresh is LoadState.Loading) {
                    isLoad = true
                    viewBinding.rvHealthArticle.showShimmerAdapter()
                } else {
                    if (isLoad) {
                        isLoad = false
                        viewBinding.rvHealthArticle.hideShimmerAdapter()
                    }
                }

                if (loadState.refresh is LoadState.Error) {
                    viewBinding.rvHealthArticle.hideShimmerAdapter()
                    (loadState.refresh as LoadState.Error).error.let {
                        viewBinding.rootLayout.errorSnack(
                            it.message.toString(),
                            Snackbar.LENGTH_SHORT
                        )
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getArticleResponse()
            } else {
                viewBinding.rvHealthArticle.hideShimmerAdapter()
                Utility.getInstance().showNoInternetDialog(requireContext()) {
                    getArticleResponse()
                }
            }
        }

        adapterArticle.rootClick = { slug ->
            (activity as DashboardActivity).addFragment(ArticleDetailFragment.newInstance(slug))
        }

    }

    private fun getArticleResponse() {
        lifecycleScope.launch {
            viewModel.getArticleList.collectLatest { articleList ->
                viewBinding.rvHealthArticle.adapter = adapterArticle
                adapterArticle.submitData(articleList)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentArticlesBinding
        get() = FragmentArticlesBinding::inflate

    override val bindingViewModel: Class<ArticleViewModel>
        get() = ArticleViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }
}
