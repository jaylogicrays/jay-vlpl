package com.lr.vlpl.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.Observable
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.TopDealProductAdapter
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentTopDealProductBinding
import com.lr.vlpl.databinding.ItemSearchProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.utils.showSnack
import com.lr.vlpl.viewmodel.TopDealProductViewModel
import kotlinx.coroutines.*

class TopDealProductFragment :
    BaseFragment<FragmentTopDealProductBinding, TopDealProductViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var _list: List<Products>
    private lateinit var viewBinding: FragmentTopDealProductBinding
    private lateinit var viewModel: TopDealProductViewModel
    private var adapter: TopDealProductAdapter? = null
    private var textChangedJob: Job? = null
    private lateinit var categoryName: String
    private var productId: Int = 0
    private var categoryId: Int = 0
    private var searchProductAdapter: TopDealProductAdapter? = null
    private var myProducts: Products? = null
    private var myItemSearchBinding: ItemSearchProductBinding? = null
    private var adapterPosition: Int? = 0
    private var totalCartQTY: Int? = 0
    var sequence = 0

    companion object {
        fun newInstance(
            categoryName: String,
            categoryId: Int,
            productId: Int
        ): TopDealProductFragment {
            val fragment = TopDealProductFragment()
            val bundle = Bundle()
            bundle.putString("categoryName", categoryName)
            bundle.putInt("categoryId", categoryId)
            bundle.putInt("productId", productId)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            categoryName = bundle.getString("categoryName")!!
            categoryId = bundle.getInt("categoryId")
            productId = bundle.getInt("productId")
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentTopDealProductBinding, viewModel: TopDealProductViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        _list = arrayListOf()
        adapter = TopDealProductAdapter()
        getSearchCategoryProduct()
        viewBinding.clickListener = this
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        viewModel.topDealCategoryProduct(getParameters(""))
        searchFunctionality()
        updateList()
        adapterClickHandle()
        manageAddToCartResponse()
    }

    private fun updateList() {
        BaseApplication.getInstance().searchScreenUpdate.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            @SuppressLint("NotifyDataSetChanged")
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                _list.forEach { products ->
                    if (products.id == BaseApplication.getInstance().productId.get()) {
                        if (products.discount_type == 3) {
                            when (BaseApplication.getInstance().from) {
                                "add" -> {
                                    products.cart_qty += products.ratio
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                    products.sequence = BaseApplication.getInstance().sequence.get()
                                }
                                "delete" -> {
                                    products.cart_qty = 0
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                    products.sequence = BaseApplication.getInstance().sequence.get()
                                }
                                else -> {
                                    products.cart_qty -= products.ratio
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                    products.sequence = BaseApplication.getInstance().sequence.get()
                                }
                            }
                        } else {
                            products.cart_qty = BaseApplication.getInstance().productBillQTY.get()
                        }
                        adapter!!.notifyDataSetChanged()
                    }
                }
            }
        })

        BaseApplication.getInstance().totalCartQTY.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                checkCartSize(BaseApplication.getInstance().totalCartQTY.get())
            }
        })
    }

    private fun searchFunctionality() {
        viewBinding.etSearch.requestFocus()
        viewBinding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                var lastInput = ""
                if (editable != null) {
                    val newtInput = editable.toString()
                    textChangedJob?.cancel()
                    if (lastInput != newtInput) {
                        lastInput = newtInput
                        textChangedJob = CoroutineScope(Dispatchers.IO).launch {
                            delay(1000)
                            if (lastInput == newtInput) {
                                viewModel.topDealCategoryProduct(getParameters(newtInput))
                            }
                        }
                    } else {
                        textChangedJob = CoroutineScope(Dispatchers.IO).launch {
                            delay(1000)
                            viewModel.topDealCategoryProduct(getParameters(""))
                        }
                    }
                } else {
                    textChangedJob = CoroutineScope(Dispatchers.IO).launch {
                        delay(1000)
                        viewModel.topDealCategoryProduct(getParameters(""))
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun adapterClickHandle() {
        // Add product in cart
        adapter?.onIncreaseQTY = { products, searchProductAdapter, myBinding, myPosition ->
            BaseApplication.getInstance().from = "add"
            adapterPosition = myPosition
            this.searchProductAdapter = searchProductAdapter
            myProducts = products
            myItemSearchBinding = myBinding
            if (myProducts!!.discount_type == 3) {
                sequence = myProducts!!.sequence + 1
            }
            viewModel.addToCart(products.id, products.ratio, sequence)
        }
        // remove product from cart
        adapter?.onDecreaseQTY = { products, searchProductAdapter, myBinding, myPosition ->
            BaseApplication.getInstance().from = "remove"
            adapterPosition = myPosition
            val negativeVal: Int = (products.ratio - 1).inv()
            this.searchProductAdapter = searchProductAdapter
            myProducts = products
            myItemSearchBinding = myBinding
            if (myProducts!!.discount_type == 3) {
                sequence = myProducts!!.sequence - 1
            }
            viewModel.addToCart(products.id, negativeVal, sequence)
        }
    }

    override val bindingViewModel: Class<TopDealProductViewModel>
        get() = TopDealProductViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentTopDealProductBinding
        get() = FragmentTopDealProductBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
            R.id.rlCart -> {
                if (totalCartQTY != 0) {
                    (activity as DashboardActivity).addFragment(CartFragment.newInstance(true, ""))
                } else {
                    viewBinding.rootLayout.showSnack(getString(R.string.str_please_add_item_in_cart))
                }
            }
        }
    }

    private fun getSearchCategoryProduct() {
        viewModel.topDealCategoryProductResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    response.data!!.let { data ->
                        totalCartQTY = data.total_cart_qty
                        viewBinding.isProductListEmpty = data.top_deals_list.isEmpty()
                        // Sticky category name
//                        if (data.category_list.isNotEmpty()) {
//                            val sectionItemDecoration = StickySearchHeaderLayout(0, true, getSectionCallback(data.category_list))
//                            viewBinding.rvCategory.addItemDecoration(sectionItemDecoration)
//                        }
                        adapter!!.submitList(data.top_deals_list)
                        _list = data.top_deals_list
                        viewBinding.rvCategory.adapter = adapter
                        checkCartSize(response.data.total_cart_qty)
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.topDealCategoryProduct(getParameters(""))
                    }
                }
            }
        }
    }

    private fun manageAddToCartResponse() {
        viewModel.getAddToCartResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    if (myItemSearchBinding != null && searchProductAdapter != null) {
                        myItemSearchBinding!!.tvCartItemCounter.text =
                            response.data!!.total_product_qty.toString()
                        if (myProducts != null) {
                            if (BaseApplication.getInstance().from == "add") {
                                searchProductAdapter!!.increaseCartValue(
                                    myProducts!!,
                                    adapterPosition!!,
                                    myItemSearchBinding!!
                                )
                            } else {
                                searchProductAdapter!!.decreaseCartValue(
                                    myProducts!!,
                                    adapterPosition!!,
                                    myItemSearchBinding!!
                                )
                            }
                        }
                        BaseApplication.getInstance().totalCartQTY.set(response.data.total_cart_qty)
                    }
                    totalCartQTY = response.data!!.total_cart_qty
                    checkCartSize(response.data.total_cart_qty)
                    CustomDialog.getInstance().hide()
                    BaseApplication.getInstance().cartScreenUpdate = true
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_SHORT)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addToCart(myProducts!!.id, myProducts!!.ratio, sequence)
                    }
                }
            }
        }
    }

    private fun checkCartSize(cartSize: Int) {
        totalCartQTY = cartSize
        viewBinding.totalCartQty = cartSize
    }

    private fun getParameters(
        searchKey: String,
    ): HashMap<String, String> {
        val queryMap = hashMapOf<String, String>()
        queryMap[RequestParams.SEARCH_KEY] = searchKey
        return queryMap
    }
}