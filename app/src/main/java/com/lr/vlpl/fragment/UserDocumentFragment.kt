package com.lr.vlpl.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.CustomBrowserActivity
import com.lr.vlpl.activity.RegisterActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentUserDocumentBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.utils.Constants
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack


class UserDocumentFragment : BaseViewBindingFragment<FragmentUserDocumentBinding>() {

    private lateinit var binding: FragmentUserDocumentBinding
    private var selectedUserType = ""

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentUserDocumentBinding) {
        binding = viewBinding
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        binding.btnNext.setOnClickListener {
            if (isValidate()) {
                binding.tvCheckboxError.visibility = View.GONE
                if (isTermConditionAndPrivacySelected()) {
                    val activity = (activity as RegisterActivity)
                    activity.viewModel.gstNo = binding.etGstNo.et.text.toString()
                    activity.viewModel.drugLicenceNo = binding.etDrugLicence.et.text.toString()
                    activity.viewModel.drugLicenceNo2 = binding.etDrugLicence2.et.text.toString()
                    activity.viewModel.designation = binding.etDesignation.et.text.toString()
                    activity.viewModel.idProofDoc = binding.documentId.selectedFile!!
                    if (selectedUserType != "mr") {
                        binding.documentGst.selectedFile.let { documentGst ->
                            activity.viewModel.gstDoc = documentGst
                        }
                        binding.documentDrugLicence.selectedFile.let { documentDrugLicence ->
                            activity.viewModel.drugDoc = documentDrugLicence
                        }
                        binding.documentDrugLicence2.selectedFile.let { documentDrugLicence2 ->
                            activity.viewModel.drugDoc2 = documentDrugLicence2
                        }
                        binding.documentFssai.selectedFile.let { documentFssai ->
                            activity.viewModel.fssaiDoc = documentFssai
                        }
                    }
                    activity.viewModel.postRegister()
                } else {
                    binding.tvCheckboxError.visibility = View.VISIBLE
                }
            }
        }
        binding.tvEmail.setOnClickListener {
            Utility.getInstance().openEmail(requireActivity())
        }
        binding.tvCall.setOnClickListener {
            Utility.getInstance().openDial(requireActivity())
        }
        registerCallback()
        hideShowViews()
        checkboxListeners()
    }

    private fun checkboxListeners() {
        binding.tvTermCondition.setOnClickListener {
            openBrowser(Constants.TERMS_CONDITION)
        }
        binding.rbTermCondition.setOnClickListener {
            toggleRadioButton(binding.rbTermCondition, !binding.rbTermCondition.isSelected)
        }
        binding.tvPrivacyPolicy.setOnClickListener {
            openBrowser(Constants.PRIVACY_POLICY)
        }
        binding.rbPrivacyPolicy.setOnClickListener {
            toggleRadioButton(binding.rbPrivacyPolicy, !binding.rbPrivacyPolicy.isSelected)
        }
    }

    private fun hideShowViews() {
        (activity as RegisterActivity).selectedUserType.observe(viewLifecycleOwner) {
            selectedUserType = it
            if (it == "mr") {
                binding.llMultiDoc.visibility = View.GONE
                binding.etDesignation.visibility = View.VISIBLE
            } else {
                binding.llMultiDoc.visibility = View.VISIBLE
                binding.etDesignation.visibility = View.GONE
            }
        }
    }

    private fun registerCallback() {
        (activity as RegisterActivity).viewModel.getRegister
            .observe(viewLifecycleOwner) { response ->
                when (response) {
                    is Resource.Success -> {
                        CustomDialog.getInstance().hide()
                        (activity as RegisterActivity).onNextFragmentNavigation(RegisterActivity.NEXT)
                    }
                    is Resource.Error -> {
                        CustomDialog.getInstance().hide()
                        binding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                    is Resource.ConnectionError -> {
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            (activity as RegisterActivity).viewModel.postRegister()
                        }
                    }
                }
            }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUserDocumentBinding
        get() = FragmentUserDocumentBinding::inflate

    private fun isValidate(): Boolean {
        var isValid = true
        if (selectedUserType == "mr") {
            if (!binding.etDesignation.isValidate())
                isValid = false
        } else {
            if (binding.etGstNo.et.text.isNotBlank()) {
                if (!binding.etGstNo.isValidate())
                    isValid = false
                if (!binding.documentGst.isValidate())
                    isValid = false
            } else {
                binding.etGstNo.removeErrorMessage()
                binding.documentGst.removeErrorMessage()
            }
            if (!binding.etDrugLicence.isValidate())
                isValid = false
            if (!binding.documentDrugLicence.isValidate())
                isValid = false
            if (!binding.etDrugLicence2.isValidate())
                isValid = false
            if (!binding.documentDrugLicence2.isValidate())
                isValid = false
        }
        if (!binding.documentId.isValidate())
            isValid = false
        return isValid
    }

    private fun isTermConditionAndPrivacySelected(): Boolean {
        return binding.rbTermCondition.isSelected && binding.rbPrivacyPolicy.isSelected
    }

    private fun toggleRadioButton(rb: RadioButton, isSelected: Boolean) {
        rb.isChecked = isSelected
        rb.isSelected = isSelected
        val drawableRes =
            if (isSelected) R.drawable.ic_radio_selected else R.drawable.ic_radio_unselect
        rb.setButtonDrawable(drawableRes)
    }

    private fun openBrowser(key: String) {
        if (BaseApplication.getInstance().isConnectionAvailable()) {
            val intent = Intent(requireContext(), CustomBrowserActivity::class.java)
            intent.putExtra(Constants.URL_KEY, key)
            startActivity(intent)
        } else {
            ToastHelper.showMessage(
                requireActivity(),
                resources.getString(R.string.str_please_check_your_internet)
            )
        }
    }
}