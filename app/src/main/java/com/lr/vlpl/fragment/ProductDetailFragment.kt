package com.lr.vlpl.fragment

import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.Observable
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.ImageSliderAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentProductDetailBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ProductDetailsViewModel

class ProductDetailFragment :
    BaseFragment<FragmentProductDetailBinding, ProductDetailsViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentProductDetailBinding
    private lateinit var viewModel: ProductDetailsViewModel
    private lateinit var productData: Products
    private lateinit var slug: String

    companion object {
        fun newInstance(slug: String): ProductDetailFragment {
            val fragment = ProductDetailFragment()
            val bundle = Bundle()
            bundle.putString("slug", slug)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            slug = bundle.getString("slug")!!
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentProductDetailBinding,
        viewModel: ProductDetailsViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewModel.getProductDetails(slug)
        manageProductDetailsResponse()
        manageAddToCartResponse()

        BaseApplication.getInstance().productCartCount.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                changeColorAndVisibility(BaseApplication.getInstance().productCartCount.get())
            }
        })

        BaseApplication.getInstance().totalCartQTY.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                checkCartSize(BaseApplication.getInstance().totalCartQTY.get())
            }
        })
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
            R.id.btnViewCart -> {
                (activity as DashboardActivity).addFragment(
                    CartFragment.newInstance(
                        true,
                        productData.slug
                    )
                )
            }
            R.id.btnAddToCart -> {
                (activity as DashboardActivity).viewModel.addToCart(productData.id, 1)
            }
            R.id.ivDecrementCart -> {
                (activity as DashboardActivity).viewModel.addToCart(productData.id, -1)
            }
            R.id.ivIncrementCart -> {
                (activity as DashboardActivity).viewModel.addToCart(productData.id, 1)
            }
            R.id.rlCart -> {
                (activity as DashboardActivity).addFragment(
                    CartFragment.newInstance(
                        true,
                        productData.slug
                    )
                )
            }
            R.id.etSearch -> {
                (activity as DashboardActivity).addFragment(SearchFragment())
            }
        }
    }

    private fun changeColorAndVisibility(itemCounter: Int) {
        if (itemCounter == 0) {
            viewBinding.btnAddToCart.visibility = View.VISIBLE
            viewBinding.rlViewCart.visibility = View.GONE
        } else {
            viewBinding.tvCartItemCounter.text = itemCounter.toString()
            viewBinding.btnAddToCart.visibility = View.GONE
            viewBinding.rlViewCart.visibility = View.VISIBLE
        }
    }

    private fun checkCartSize(cartSize: Int) {
        viewBinding.totalCartQty = cartSize
    }

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override val bindingViewModel: Class<ProductDetailsViewModel>
        get() = ProductDetailsViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentProductDetailBinding
        get() = FragmentProductDetailBinding::inflate

    private fun manageProductDetailsResponse() {
        viewModel.responseProductDetails.observe(viewLifecycleOwner)
        { response ->
            when (response) {
                is Resource.Loading -> {
                    viewBinding.apply {
                        SLclProductDetail.visibility = View.VISIBLE
                        SLclProductDetail.startShimmerAnimation()
                        scroll.visibility = View.GONE
                        llCart.visibility = View.GONE
                        tvSomeThingWrong.visibility = View.GONE
                    }
                }
                is Resource.Success -> {
                    checkCartSize(response.data!!.total_cart_qty)
                    viewBinding.SLclProductDetail.visibility = View.GONE
                    viewBinding.SLclProductDetail.stopShimmerAnimation()
                    viewBinding.scroll.visibility = View.VISIBLE
                    viewBinding.llCart.visibility = View.VISIBLE
                    viewBinding.tvSomeThingWrong.visibility = View.GONE
                    setUIData(response.data.product_data)
                }
                is Resource.Error -> {
                    viewBinding.SLclProductDetail.visibility = View.GONE
                    viewBinding.SLclProductDetail.stopShimmerAnimation()
                    viewBinding.scroll.visibility = View.GONE
                    viewBinding.llCart.visibility = View.GONE
                    viewBinding.tvSomeThingWrong.visibility = View.VISIBLE
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getProductDetails(slug)
                    }
                }
            }
        }
    }

    private fun setUIData(data: Products?) {
        if (data != null) {
            productData = data
        }

        viewBinding.productList = data // set product details in XML file
        viewBinding.clickListener = this

        //set product QTY if product already  available in cart
        if (data!!.cart_qty != 0) {
            changeColorAndVisibility(data.cart_qty)
        } else {
            changeColorAndVisibility(0)
        }

        // set product images in ViewPager
        val adapter = ImageSliderAdapter(requireContext(), productData.photo)
        viewBinding.vpProductImage.adapter = adapter
        viewBinding.pageIndicator.attachTo(viewBinding.vpProductImage)
        if (data.description.length > 100) {
            addReadMore(
                Utility.getInstance().htmlToString(data.description),
                viewBinding.tvDescription
            )
        } else {
            viewBinding.tvDescription.text =
                Utility.getInstance().htmlToString(data.description)
        }
    }

    private fun manageAddToCartResponse() {
        (activity as DashboardActivity).viewModel.getAddToCartResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    BaseApplication.getInstance().cartScreenUpdate = true
                    BaseApplication.getInstance().totalCartQTY.set(response.data!!.total_cart_qty)
                    BaseApplication.getInstance().productCartCount.set(response.data.total_product_qty)
                    CustomDialog.getInstance().hide()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(
                        response.message!!, Snackbar.LENGTH_LONG
                    )
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        (activity as DashboardActivity).viewModel.addToCart(productData.id, 1)
                    }
                }
            }
        }
    }

    private fun addReadMore(text: String, textView: TextView) {
        val ss = SpannableString(text.substring(0, 100) + getString(R.string.str_read_more))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadLess(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                } else {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun addReadLess(text: String, textView: TextView) {
        val ss = SpannableString("$text " + getString(R.string.str_read_less))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadMore(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                } else {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
    }
}
