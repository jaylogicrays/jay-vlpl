package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.databinding.FragmentUploadedDocumentBinding
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.utils.Utility

class UploadedDocumentFragment : BaseViewBindingFragment<FragmentUploadedDocumentBinding>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentUploadedDocumentBinding

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentUploadedDocumentBinding) {
        this.viewBinding = viewBinding
        setDocumentDetails()

        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUploadedDocumentBinding
        get() = FragmentUploadedDocumentBinding::inflate

    private fun setDocumentDetails() {
        val loginHelper = LoginHelper.getInstance()
        if (loginHelper!!.getRoleSlug() == "mr") {
            viewBinding.cvGST.visibility = View.GONE
            viewBinding.cvLicence.visibility = View.GONE
        } else {
            //show Gst Doc
            if (checkPDf(loginHelper.getGstDoc()))
                Utility.getInstance().loadThumbnailImageUrl(
                    requireContext(),
                    loginHelper.getGstDoc(),
                    viewBinding.wvGstUrl
                )
            else {
                viewBinding.wvGstUrl.visibility = View.GONE
                viewBinding.ivGstUrl.visibility = View.VISIBLE
                Utility.getInstance()
                    .loadImageUrl(requireContext(), loginHelper.getGstDoc(), viewBinding.ivGstUrl)
            }
            //show drug licence Doc
            if (checkPDf(loginHelper.getDrugDoc())) {
                Utility.getInstance().loadThumbnailImageUrl(
                    requireContext(), loginHelper.getDrugDoc(),
                    viewBinding.wvLicenceUrl
                )
            } else {
                viewBinding.wvLicenceUrl.visibility = View.GONE
                viewBinding.ivLicenceUrl.visibility = View.VISIBLE
                Utility.getInstance().loadImageUrl(
                    requireContext(),
                    loginHelper.getDrugDoc(),
                    viewBinding.ivLicenceUrl
                )
            }

            viewBinding.tvGstDocNo.text = loginHelper.getGstNo()
            viewBinding.tvLicenceNo.text = loginHelper.getDrugNo()
        }
        //show Id proof Doc
        if (checkPDf(loginHelper.getIdDoc())) {
            Utility.getInstance().loadThumbnailImageUrl(
                requireContext(),
                loginHelper.getIdDoc(),
                viewBinding.wvAadhaarUrl
            )
        } else {
            viewBinding.wvAadhaarUrl.visibility = View.GONE
            viewBinding.ivAadhaarUrl.visibility = View.VISIBLE
            Utility.getInstance()
                .loadImageUrl(requireContext(), loginHelper.getIdDoc(), viewBinding.ivAadhaarUrl)
        }
    }

    private fun checkPDf(url: String): Boolean {
        return url.split(".").last() == "pdf"
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }
}