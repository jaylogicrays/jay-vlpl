package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentIncentiveBinding
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack

class IncentiveFragment : BaseViewBindingFragment<FragmentIncentiveBinding>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentIncentiveBinding
    private var isShowBackIcon = false

    companion object {
        fun newInstance(isShowBackIcon: Boolean = false): IncentiveFragment {
            val fragment = IncentiveFragment()
            val bundle = Bundle()
            bundle.putBoolean("isShowBackIcon", isShowBackIcon)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            isShowBackIcon = bundle.getBoolean("isShowBackIcon")
        }
    }

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentIncentiveBinding) {
        getExtra()
        this.viewBinding = viewBinding
        (activity as DashboardActivity).viewModel.getUserIncentive()  // get user incentive point api
        manageUserIncentivePointApiResult()
        viewBinding.ctbToolbar.ivBackIcon.visibility = if (isShowBackIcon) {
            View.VISIBLE
        } else {
            View.GONE
        }
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentIncentiveBinding
        get() = FragmentIncentiveBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun manageUserIncentivePointApiResult() {
        (activity as DashboardActivity).viewModel.getUserIncentivePointResult.observe(
            viewLifecycleOwner
        ) { response ->
            when (response) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    viewBinding.totalPoint = response.data?.total_points
                }
                is Resource.Error -> {
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        (activity as DashboardActivity).viewModel.getUserIncentive()  // get user incentive point api
                    }
                }
            }
        }
    }
}