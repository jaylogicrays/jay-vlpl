package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.LoginActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentMyAccountBinding
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.ActivityNav
import com.lr.vlpl.utils.SharedPreference
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.MyAccountViewModel

class MyAccountFragment :
    BaseFragment<FragmentMyAccountBinding, MyAccountViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentMyAccountBinding
    private lateinit var viewModel: MyAccountViewModel
    private var isEditProfile: Boolean = false

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentMyAccountBinding,
        viewModel: MyAccountViewModel,
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewModel.viewBinding = viewBinding
        viewBinding.clickListener = this
        viewModel.setEditMode(isEditProfile)
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        toolBarItemClick()
        getProfileData()
        getUpdateProfileResponse()
        manageDeleteUserAccountResponse()    // delete user account response
    }

    private fun toolBarItemClick() {
        viewBinding.ctbToolbar.tvEndIcon.setOnClickListener {
            if (!isEditProfile) {
                enableEditMode()
            } else {
                callUpdateApiAndDisableEditMode()
            }
        }
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    private fun callUpdateApiAndDisableEditMode() {
        if (viewModel.isValidate()) {
            if (!viewModel.checkEditOrNot()) {
                viewModel.updateProfile()
            } else {
                disableEditMode()
            }
        }
    }

    private fun enableEditMode() {
        isEditProfile = true
        viewBinding.ctbToolbar.tvEndIcon.text = getString(R.string.str_save)
        viewBinding.ctbToolbar.tvEndIcon.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        viewModel.setEditMode(isEditProfile)
    }

    private fun disableEditMode() {
        isEditProfile = false
        viewBinding.ctbToolbar.tvEndIcon.text = getString(R.string.str_edit)
        viewBinding.ctbToolbar.tvEndIcon.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_edit,
            0,
            0,
            0
        )
        viewModel.setEditMode(isEditProfile)
    }

    override val bindingViewModel: Class<MyAccountViewModel>
        get() = MyAccountViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMyAccountBinding
        get() = FragmentMyAccountBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
            R.id.tvDeleteAccount -> {
                Utility.getInstance().showConfirmationDialog(
                    requireContext(),
                    requireContext().getString(R.string.str_delete_account_header),
                    requireContext().resources.getString(R.string.str_dialog_delete_account),
                    requireContext().getString(R.string.str_no),
                ) {
                    viewModel.deleteUserAccount()
                }
            }
        }
    }

    private fun getProfileData() {
        viewModel.getProfileData.observe(
            viewLifecycleOwner
        ) { response ->
            when (response) {
                is Resource.Success -> {
                    response.data?.let { data ->
                        LoginHelper.getInstance()?.saveLogin(data)
                        setValue()
                    }
                    CustomDialog.getInstance().hide()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.callAllApi()
                    }
                }
            }
        }
    }

    private fun setValue() {
        val loginHelper = LoginHelper.getInstance()
        viewBinding.etUserType.et.setText(loginHelper!!.getRole())
        viewBinding.etFirmName.et.setText(loginHelper.getFirmName())
        viewBinding.etFirstName.et.setText(loginHelper.getFirstName())
        viewBinding.etLastName.et.setText(loginHelper.getLastName())
        viewBinding.etEmail.et.setText(loginHelper.getEmail())
        viewBinding.etPinCode.et.setText(loginHelper.getPinCode())
        viewBinding.etCity.et.setText(loginHelper.getCityName())
        viewBinding.etState.et.setText(loginHelper.getStateName())
        viewBinding.etContactNo1.et.setText(loginHelper.getContactNo1())
        viewBinding.etContactNo2.et.setText(loginHelper.getContactNo2())
//        getStateCity()
    }

    private fun getUpdateProfileResponse() {
        viewModel.updateProfileResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    ToastHelper.showMessage(requireActivity(), response.message.toString())
                    requireActivity().onBackPressed()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        callUpdateApiAndDisableEditMode()
                    }
                }
            }
        }
    }

    private fun manageDeleteUserAccountResponse() {
        viewModel.deleteUserAccountResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.deleteUserAccount()
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.Success -> {
                    ToastHelper.showMessage(requireActivity(), response.message!!)
                    logoutOrDeleteUser()
                }
            }
        }
    }

    private fun logoutOrDeleteUser() {
        val token = SharedPreference.getValue(SharedPreference.FCM_TOKEN, "")
        CustomDialog.getInstance().hide()
        SharedPreference.clearPreferences()
        SharedPreference.setValue(SharedPreference.FCM_TOKEN, token)
        ActivityNav.getInstance()?.callNewActivity(requireActivity(), LoginActivity::class.java)
    }
}
