package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.ProductListAdapter
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.FragmentProductListBinding
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ProductListViewModel
import kotlinx.coroutines.flow.collectLatest

class ProductListFragment : BaseFragment<FragmentProductListBinding,
        ProductListViewModel, AppRepository>(), View.OnClickListener {

    private lateinit var viewBinding: FragmentProductListBinding
    private lateinit var viewModel: ProductListViewModel
    lateinit var adapter: ProductListAdapter
    private var isFirst = true
    private lateinit var categoryName: String
    private var categoryId: Int = 0
    private var tvAddToCart: TextView? = null

    companion object {
        fun newInstance(categoryName: String, categoryId: Int): ProductListFragment {
            val fragment = ProductListFragment()
            val bundle = Bundle()
            bundle.putString("categoryName", categoryName)
            bundle.putInt("categoryId", categoryId)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            categoryName = bundle.getString("categoryName")!!
            categoryId = bundle.getInt("categoryId")
        }
    }

    override fun onViewCreated(
        instance: Bundle?, viewBinding: FragmentProductListBinding, viewModel: ProductListViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        viewBinding.tvTitle.text = categoryName
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing1)
        viewBinding.rvProducts.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvProducts.setHasFixedSize(true)
        viewBinding.rvProducts.layoutManager = GridLayoutManager(context, 2)
        adapter = ProductListAdapter()
        setAdapter("", "")
        manageAddToCartResponse()
    }

    override val bindingViewModel: Class<ProductListViewModel>
        get() = ProductListViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentProductListBinding
        get() = FragmentProductListBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
            R.id.ivSearch -> (activity as DashboardActivity).addFragment(SearchFragment())
            R.id.tvSortBy -> {
                sortClick()
            }
        }
    }

    private fun setAdapter(sortBy: String, sortKey: String) {
        lifecycleScope.launchWhenStarted {
            adapter.loadStateFlow.collectLatest { loadState ->
                viewBinding.appendProgress.isVisible = loadState.source.append is LoadState.Loading
                if (loadState.refresh is LoadState.Loading) {
                    isFirst = true
                    viewBinding.isProductListEmpty = false
                    viewBinding.rvProducts.showShimmerAdapter()
                } else {
                    if (isFirst) {
                        isFirst = false
                        viewBinding.rvProducts.hideShimmerAdapter()
                    }
                }

                if (loadState.refresh is LoadState.Error) {
                    viewBinding.rvProducts.hideShimmerAdapter()
                    (loadState.refresh as LoadState.Error).error.let {
                        if (it.message == "empty")
                            viewBinding.isProductListEmpty = true
                        else
                            viewBinding.rootLayout.errorSnack(
                                it.message.toString(),
                                Snackbar.LENGTH_SHORT
                            )
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val queryMap = hashMapOf<String, String>()
            queryMap[RequestParams.CATEGORY_ID] = categoryId.toString()
            queryMap[RequestParams.SORT_BY] = sortBy
            queryMap[RequestParams.SORT_KEY] = sortKey
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getProductResponse(queryMap)
            } else {
                Utility.getInstance().showNoInternetDialog(requireContext()) {
                    getProductResponse(queryMap)
                }
            }
        }
//        adapter.apply {
//            rootClick = {
//                (activity as DashboardActivity).addFragment(ProductDetailFragment.newInstance(it.slug))
//            }
//            addToCartClick = { productId, addToCart ->
//                tvAddToCart = addToCart
//                (activity as DashboardActivity).viewModel.addToCart(productId, 1)
//            }
//        }
    }

    private fun getProductResponse(queries: HashMap<String, String>) {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getProductDataList(queries)
                .collectLatest { productData ->
                    viewBinding.rvProducts.adapter = adapter
                    adapter.submitData(productData)
                }
        }
    }

    private fun manageAddToCartResponse() {
        (activity as DashboardActivity).viewModel.getAddToCartResponse.observe(viewLifecycleOwner)
        { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    BaseApplication.getInstance().cartScreenUpdate = true
                    CustomDialog.getInstance().hide()
                    tvAddToCart?.text = resources.getString(R.string.str_added)
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {

                    }
                }
            }
        }
    }

    private fun sortClick() {
        Utility.getInstance().showFilterBottomDialog(requireContext()) {
            when (it.id) {
                R.id.tvReset -> {
                    setAdapter("", "")
                }
                R.id.tvTitleAtoZ -> {
                    setAdapter(RequestParams.TITLE, RequestParams.ASC)
                }
                R.id.tvTitleZtoA -> {
                    setAdapter(RequestParams.TITLE, RequestParams.DESC)
                }
                R.id.tvPriceLowToHigh -> {
                    setAdapter(RequestParams.MRP_PRICE, RequestParams.ASC)
                }
                R.id.tvPriceHighToLow -> {
                    setAdapter(RequestParams.MRP_PRICE, RequestParams.DESC)
                }
            }
        }
    }
}
