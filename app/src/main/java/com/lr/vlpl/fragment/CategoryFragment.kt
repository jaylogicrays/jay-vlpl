package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.CategoryAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.FragmentCategoryBinding
import com.lr.vlpl.model.Category
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.CategoryViewModel

class CategoryFragment : BaseFragment<FragmentCategoryBinding, CategoryViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentCategoryBinding
    lateinit var viewModel: CategoryViewModel

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentCategoryBinding,
        viewModel: CategoryViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        setUpView()
        getCategory()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentCategoryBinding
        get() = FragmentCategoryBinding::inflate

    override val bindingViewModel: Class<CategoryViewModel>
        get() = CategoryViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun setUpView() {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvCategory.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvCategory.setHasFixedSize(true)
        viewBinding.rvCategory.layoutManager = GridLayoutManager(requireContext(), 2)
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    private fun getCategory() {
        viewModel.categoryResult.observe(viewLifecycleOwner,
            Observer { response ->
                when (response) {
                    is Resource.Success -> {
                        viewBinding.rvCategory.hideShimmerAdapter()
                        setAdapter(response.data!!)
                    }
                    is Resource.Error -> {
                        viewBinding.rvCategory.hideShimmerAdapter()
                        viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        viewBinding.rvCategory.showShimmerAdapter()
                    }
                    is Resource.ConnectionError -> {
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            viewModel.getCategory()
                        }
                    }
                }
            })
    }

    private fun setAdapter(categoryList: List<Category>) {
        val categoryAdapter = CategoryAdapter(requireContext(), categoryList.size)
        categoryAdapter.submitList(categoryList)
        viewBinding.rvCategory.adapter = categoryAdapter
        categoryAdapter.onCategoryClick = { categoryList ->
            (activity as DashboardActivity).addFragment(
                SearchFragment.newInstance(
                    categoryList.title,
                    categoryList.id,
                    0
                )
            )
        }
    }
}