package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.activity.LoginActivity
import com.lr.vlpl.databinding.FragmentUserSuccessBinding
import com.lr.vlpl.utils.ActivityNav

class UserSuccessFragment : BaseViewBindingFragment<FragmentUserSuccessBinding>() {

    private lateinit var binding: FragmentUserSuccessBinding
    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentUserSuccessBinding) {
        binding = viewBinding
        binding.btnContinue.setOnClickListener {
            ActivityNav.getInstance()?.callActivity(requireActivity(), LoginActivity::class.java)
            requireActivity().finish()
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUserSuccessBinding
        get() = FragmentUserSuccessBinding::inflate
}