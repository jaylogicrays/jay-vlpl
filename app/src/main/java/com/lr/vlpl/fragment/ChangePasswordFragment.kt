package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentChangePasswordBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ChangePasswordViewModel

class ChangePasswordFragment :
    BaseFragment<FragmentChangePasswordBinding, ChangePasswordViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentChangePasswordBinding
    private lateinit var viewModel: ChangePasswordViewModel

    override fun onViewCreated(
        instance: Bundle?, viewBinding: FragmentChangePasswordBinding,
        viewModel: ChangePasswordViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewModel.viewBinding = viewBinding
        viewBinding.clickListener = this
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
        getChangePasswordResponse()
        onEyePasswordClick()
        Int.MAX_VALUE
    }

    private fun onEyePasswordClick() {
        viewBinding.etOldPassword.passwordEyeVisible()
        viewBinding.etOldPassword.passwordEyeClick()

        viewBinding.etNewPassword.passwordEyeVisible()
        viewBinding.etNewPassword.passwordEyeClick()

        viewBinding.etComformPassword.passwordEyeVisible()
        viewBinding.etComformPassword.passwordEyeClick()
    }

    override val bindingViewModel: Class<ChangePasswordViewModel>
        get() = ChangePasswordViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentChangePasswordBinding
        get() = FragmentChangePasswordBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnUpdate -> {
                if (viewModel.isValidate(requireContext()))
                    viewModel.changePasswordCall()
            }
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun getChangePasswordResponse() {
        viewModel.getChangePassword.observe(viewLifecycleOwner,
            Observer { response ->
                when (response) {
                    is Resource.Success -> {
                        CustomDialog.getInstance().hide()
                        ToastHelper.showMessage(requireActivity(), response.message.toString())
                        requireActivity().onBackPressed()
                    }
                    is Resource.Error -> {
                        CustomDialog.getInstance().hide()
                        viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                    is Resource.ConnectionError -> {
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            if (viewModel.isValidate(requireContext()))
                                viewModel.changePasswordCall()
                        }
                    }
                }
            })
    }
}