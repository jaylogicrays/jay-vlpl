package com.lr.vlpl.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.databinding.Observable
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.CategoryAdapter
import com.lr.vlpl.adapter.HomeArticleAdapter
import com.lr.vlpl.adapter.HomeImageSliderAdapter
import com.lr.vlpl.adapter.TopDealAdapter
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.DialogConnectUsBinding
import com.lr.vlpl.databinding.FragmentHomeBinding
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.model.*
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.HomeViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel, AppRepository>(),
    View.OnClickListener {
    lateinit var viewBinding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var dialog: Dialog

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentHomeBinding,
        viewModel: HomeViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewModel.viewBinding = viewBinding
        viewBinding.clickListener = this
        updateNotificationStatus()
        checkNotificationStatus()
        manageHomeProductLists()
        manageConnectUsResponse()
        val authToken: String = LoginHelper.getInstance()?.getAuthToken().toString()
        Log.d("authToken: ", authToken)
    }

    private fun checkNotificationStatus() {
        if (LoginHelper.getInstance()!!.getIsNewPush() == 1) {
            BaseApplication.getInstance().notificationUpdate.set(true)
        } else {
            BaseApplication.getInstance().notificationUpdate.set(false)
        }
    }

    private fun manageHomeProductLists() {
        viewModel.homeProductListResponse.observe(viewLifecycleOwner)
        { response ->
            when (response) {
                is Resource.Loading -> {
                    viewBinding.dataLoad = false
                    viewBinding.shimmerBanner.startShimmerAnimation()
                    viewBinding.rvHealthArticle.showShimmerAdapter()
                    viewBinding.rvProduct.showShimmerAdapter()
                    viewBinding.rvCategory.showShimmerAdapter()
                }
                is Resource.Success -> {
                    viewBinding.dataLoad = true
                    viewBinding.shimmerBanner.stopShimmerAnimation()
                    viewBinding.rvHealthArticle.hideShimmerAdapter()
                    viewBinding.rvProduct.hideShimmerAdapter()
                    viewBinding.rvCategory.hideShimmerAdapter()
                    response.data?.let { homeModel ->
                        handleCategoryResponse(homeModel)
                        handleProductResponse(homeModel)
                        handleArticleResponse(homeModel)
                        handleBannerResponse(homeModel)
                        if (homeModel.promotion.image.isNotEmpty()) {
                            showOffersDialog(homeModel)
                        }
                        BaseApplication.getInstance().totalCartQTY.set(homeModel.total_cart_qty)
                    }
                }
                is Resource.Error -> {
                    viewBinding.dataLoad = false
                    viewBinding.shimmerBanner.stopShimmerAnimation()
                    viewBinding.rvHealthArticle.hideShimmerAdapter()
                    viewBinding.rvProduct.hideShimmerAdapter()
                    viewBinding.rvCategory.hideShimmerAdapter()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }

                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getDashBoardProductList()
                    }
                }
            }
        }
    }

    private fun handleCategoryResponse(homeModel: HomeModel) {
        if (homeModel.category.isEmpty()) {
            viewBinding.tvNoCategory.visibility = View.VISIBLE
            viewBinding.rvCategory.visibility = View.GONE
            viewBinding.tvCategoryViewAll.visibility = View.GONE
        } else {
            viewBinding.tvNoCategory.visibility = View.GONE
            viewBinding.rvCategory.visibility = View.VISIBLE
            viewBinding.tvCategoryViewAll.visibility = View.VISIBLE
            setCategoryAdapter(homeModel.category)
        }
    }

    private fun showOffersDialog(homeModel: HomeModel) {
        Utility.getInstance().showOffersDialog(requireContext(), homeModel.promotion.image)
    }

    private fun handleProductResponse(homeModel: HomeModel) {
        if (homeModel.product.isEmpty()) {
            viewBinding.tvNoProduct.visibility = View.VISIBLE
            viewBinding.rvProduct.visibility = View.GONE
            viewBinding.tvViewAllTopDeal.visibility = View.GONE
        } else {
            viewBinding.tvNoProduct.visibility = View.GONE
            viewBinding.rvProduct.visibility = View.VISIBLE
            viewBinding.tvViewAllTopDeal.visibility = View.VISIBLE
            setProductList(homeModel.product)
        }
    }

    private fun handleArticleResponse(homeModel: HomeModel) {
        if (homeModel.blog.isEmpty()) {
            viewBinding.tvNoHealthArticle.visibility = View.VISIBLE
            viewBinding.rvHealthArticle.visibility = View.GONE
            viewBinding.tvViewAllArticle.visibility = View.GONE
        } else {
            viewBinding.tvNoHealthArticle.visibility = View.GONE
            viewBinding.rvHealthArticle.visibility = View.VISIBLE
            viewBinding.tvViewAllArticle.visibility = View.VISIBLE
            setArticleAdapter(homeModel.blog)
        }
    }

    private fun handleBannerResponse(homeModel: HomeModel) {
        if (homeModel.banners.isEmpty()) {
            viewBinding.imgNoBanner.visibility = View.VISIBLE
            viewBinding.llbannerView.visibility = View.GONE
        } else {
            viewBinding.imgNoBanner.visibility = View.GONE
            viewBinding.llbannerView.visibility = View.VISIBLE
            setBanner(homeModel.banners)
        }
    }

    private fun setBanner(banners: List<Banner>) {
        val adapter = HomeImageSliderAdapter(requireContext(), banners)
        viewBinding.vpProductImage.adapter = adapter
        viewBinding.pageIndicator.attachTo(viewBinding.vpProductImage)
//      adapter.bannerClick = { product_id ->
//         (activity as DashboardActivity).addFragment(SearchFragment.newInstance("", 0, product_id))
//      }
    }

    override val bindingViewModel: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = FragmentHomeBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tvSearch -> (activity as DashboardActivity).addFragment(SearchFragment())
            R.id.cardOrderHistory -> (activity as DashboardActivity).addFragment(
                OrderHistoryFragment()
            )
            R.id.cardOrderPastItem -> (activity as DashboardActivity).addFragment(
                OrderPastItemFragment()
            )
            R.id.cardScanQR -> (activity as DashboardActivity).addFragment(ScannerFragment())
            R.id.cardIncentiveStructure -> (activity as DashboardActivity).addFragment(
                IncentiveFragment.newInstance(true)
            )
            R.id.ivNotification -> (activity as DashboardActivity).addFragment(NotificationFragment())
            R.id.tvViewAllTopDeal -> (activity as DashboardActivity).addFragment(
                TopDealProductFragment()
            )
            R.id.tvViewAllArticle -> (activity as DashboardActivity).addFragment(ArticlesFragment())
            R.id.tvCategoryViewAll -> (activity as DashboardActivity).addFragment(SearchFragment())
            R.id.cardContactUs -> openContactUsDialog()
            else -> (activity as DashboardActivity).addFragment(HomeFragment())
        }
    }

    //set product data in recyclerView
    private fun setProductList(product: List<Products>) {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvProduct.addItemDecoration(SpacesItemDecoration(spacingInPixels, false))
        viewBinding.rvProduct.setHasFixedSize(true)
        val topDealAdapter = TopDealAdapter(product)
        viewBinding.rvProduct.adapter = topDealAdapter
        topDealAdapter.onProductClick = { productList ->
            (activity as DashboardActivity).addFragment(
                SearchFragment.newInstance(
                    "",
                    0,
                    productList.id
                )
            )
        }
    }

    //set category Adapter
    private fun setCategoryAdapter(category: List<Category>) {
        val categoryAdapter = CategoryAdapter(requireContext(), category.size)
        val spacingInPixels = this.resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvCategory.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvCategory.setHasFixedSize(true)
        viewBinding.rvCategory.layoutManager =
            GridLayoutManager(
                requireContext(),
                if (category.size > 2) 2 else 1,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        viewBinding.rvCategory.adapter = categoryAdapter
        categoryAdapter.submitList(category)
        categoryAdapter.onCategoryClick = { categoryList ->
            (activity as DashboardActivity).addFragment(
                SearchFragment.newInstance(
                    categoryList.title,
                    categoryList.id,
                    0
                )
            )
        }
    }

    //set Article data in recyclerView
    private fun setArticleAdapter(blog: List<Articles>) {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvHealthArticle.addItemDecoration(SpacesItemDecoration(spacingInPixels, false))
        viewBinding.rvHealthArticle.setHasFixedSize(true)
        val adapterHomeArticle = HomeArticleAdapter(blog)
        viewBinding.rvHealthArticle.adapter = adapterHomeArticle
        adapterHomeArticle.rootClick = {
            (activity as DashboardActivity).addFragment(ArticleDetailFragment.newInstance(it.slug))
        }
    }

    private fun manageConnectUsResponse() {
        viewModel.connectUsResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {}
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    dialog.dismiss()
                    CustomDialog.getInstance().hide()
                    ToastHelper.showMessage(requireActivity(), response.message.toString())
                }
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun openContactUsDialog() {
        var dialogBinding: DialogConnectUsBinding
        dialog = Dialog(requireContext(), R.style.DialogTheme).apply {
            setCancelable(false)
            setCanceledOnTouchOutside(true)
            dialogBinding = DialogConnectUsBinding.inflate(layoutInflater).also {
                setContentView(it.root)
            }
        }

        with(dialogBinding) {
            etTitle.addTextChangedListener(createTextWatcher(tvTitleError, etTitle))
            etDescription.addTextChangedListener(
                createTextWatcher(
                    tvTitleDescription,
                    etDescription
                )
            )

            imgClose.setOnClickListener { dialog.dismiss() }
            tvEmail.setOnClickListener { Utility.getInstance().openEmail(requireContext()) }
            tvPhone.setOnClickListener { Utility.getInstance().openDial(requireContext()) }

            btnSubmit.setOnClickListener {
                val titleIsEmpty = etTitle.text.isNullOrBlank()
                val descriptionIsEmpty = etDescription.text.isNullOrBlank()

                tvTitleError.visibility = if (titleIsEmpty) View.VISIBLE else View.INVISIBLE
                tvTitleDescription.visibility =
                    if (descriptionIsEmpty) View.VISIBLE else View.INVISIBLE

                etTitle.background = getEditTextBackground(titleIsEmpty)
                etDescription.background = getEditTextBackground(descriptionIsEmpty)

                if (!titleIsEmpty && !descriptionIsEmpty) {
                    val queryMap = hashMapOf<String, Any>(
                        RequestParams.DESCRIPTION to etDescription.text.toString(),
                        RequestParams.TITLE to etTitle.text.toString()
                    )
                    viewModel.connectUs(queryMap)
                }
            }
        }
        dialog.show()
    }

    private fun createTextWatcher(errorView: View, editText: EditText) = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            errorView.visibility = View.INVISIBLE
            editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_solid_bg_blue_radius_5)
        }
    }

    private fun getEditTextBackground(isEmpty: Boolean) = ContextCompat.getDrawable(
        requireContext(),
        if (isEmpty) R.drawable.bg_solid_with_stroke_bg_blue_radius_5 else R.drawable.bg_solid_bg_blue_radius_5
    )

    private fun updateNotificationStatus() {
        BaseApplication.getInstance().notificationUpdate.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (BaseApplication.getInstance().notificationUpdate.get()) {
                    viewBinding.isNewPush = 1
                } else {
                    viewBinding.isNewPush = 0
                }
            }
        })
    }
}
