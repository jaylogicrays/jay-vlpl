package com.lr.vlpl.fragment

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.Observable
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.OrderAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentOrderHistoryBinding
import com.lr.vlpl.databinding.ItemOrderHistoryBinding
import com.lr.vlpl.model.Order
import com.lr.vlpl.permission.PermissionCheck
import com.lr.vlpl.permission.PermissionHandler
import com.lr.vlpl.picker.MediaPicker
import com.lr.vlpl.picker.PickerResultHandler
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.utils.showSnack
import com.lr.vlpl.viewmodel.OrderViewModel
import java.io.File

class OrderHistoryFragment :
    BaseFragment<FragmentOrderHistoryBinding, OrderViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentOrderHistoryBinding
    lateinit var viewModel: OrderViewModel
    private lateinit var adapter: OrderAdapter
    private var enableReOrderButton: Boolean = true
    private var totalCartQTY: Int? = 0
    private var uploadedInvoicePosition: Int? = 0
    private lateinit var itemOrderHistoryBinding: ItemOrderHistoryBinding
    private lateinit var order: Order

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentOrderHistoryBinding,
        viewModel: OrderViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        viewModel.getOrderList()
        getOrderListResult()
        updateCartCount()
        adapter.orderId = { orderId ->
            (activity as DashboardActivity).addFragment(OrderDetailFragment.newInstance(orderId))
        }
        adapter.reorder = { orderId ->
            viewModel.orderId = orderId
            if (enableReOrderButton) {
                viewModel.reOrder()
            }
        }
        adapter.uploadInvoice = { orderList, position, itemOrderHistory ->
            uploadedInvoicePosition = position
            itemOrderHistoryBinding = itemOrderHistory
            order = orderList
            checkPermission(orderList.id)
        }
    }

    private fun updateCartCount() {
        BaseApplication.getInstance().totalCartQTY.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                checkCartSize(BaseApplication.getInstance().totalCartQTY.get())
            }
        })
    }

    private fun checkPermission(orderId: Int) {
        PermissionCheck.check(requireContext(), Utility.getInstance().getPermissionForSDK33(),
            "External storage permission is necessary", object : PermissionHandler() {
                override fun onGranted() {
                    openImagePicker(orderId)
                }

                override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                }
            })
    }

    private fun openImagePicker(orderId: Int) {
        MediaPicker.getImageFromMedia(requireContext(),
            isFromGallery = true,
            isFromCamera = true,
            isAttachFile = true,
            isCropping = false,
            handler = object : PickerResultHandler() {
                override fun onSuccess(bitmap: Bitmap?, file: File?, fileName: String) {
                    viewModel.orderId = orderId
                    viewModel.idInvoiceDoc = file!!
                    viewModel.uploadInvoice()
                }

                override fun onFailure(message: String) {

                }
            })
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentOrderHistoryBinding
        get() = FragmentOrderHistoryBinding::inflate

    override val bindingViewModel: Class<OrderViewModel>
        get() = OrderViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
            R.id.rlCart -> {
                if (totalCartQTY != 0) {
                    (activity as DashboardActivity).addFragment(CartFragment.newInstance(true, ""))
                } else {
                    viewBinding.rootLayout.showSnack(getString(R.string.str_please_add_item_in_cart))
                }
            }
        }
    }

    private fun getOrderListResult() {
        adapter = OrderAdapter()
        viewModel.orderListResult.observe(
            viewLifecycleOwner
        ) { response ->
            when (response) {
                is Resource.Success -> {
                    viewBinding.rvOrderHistory.hideShimmerAdapter()
                    CustomDialog.getInstance().hide()
                    if (response.data != null) {
                        viewBinding.isOrderListEmpty = response.data.orders_list.isEmpty()
                        adapter.submitList(response.data.orders_list)
                        viewBinding.rvOrderHistory.adapter = adapter
                        checkCartSize(response.data.total_cart_qty)
                    } else {
                        viewBinding.isOrderListEmpty = true
                    }
                }
                is Resource.Error -> {
                    viewBinding.rvOrderHistory.hideShimmerAdapter()
                    viewBinding.rootLayout.errorSnack(
                        response.message!!,
                        Snackbar.LENGTH_LONG
                    )
                }
                is Resource.Loading -> {
                    viewBinding.rvOrderHistory.showShimmerAdapter()
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getOrderList()
                    }
                }
            }
        }

        viewModel.reOrderGetResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    enableReOrderButton = true
                    CustomDialog.getInstance().hide()
                    BaseApplication.getInstance().cartScreenUpdate = true
                    viewBinding.rootLayout.showSnack(
                        resources.getString(R.string.str_message_after_reorder),
                        Snackbar.LENGTH_LONG
                    )
                    BaseApplication.getInstance().totalCartQTY.set(response.data!!.total_cart_qty)
                }
                is Resource.ConnectionError -> {
                    enableReOrderButton = true
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.reOrder()
                    }
                }
                is Resource.Error -> {
                    enableReOrderButton = true
                    viewBinding.rootLayout.errorSnack(
                        response.message!!,
                        Snackbar.LENGTH_LONG
                    )
                }
                is Resource.Loading -> {
                    enableReOrderButton = false
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
            }
        }

        viewModel.uploadInvoiceGetResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.showSnack(
                        response.message!!,
                        Snackbar.LENGTH_LONG
                    )
                    adapter.updateIsInvoiceUploadedValue(
                        order,
                        uploadedInvoicePosition!!,
                        itemOrderHistoryBinding
                    )
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.uploadInvoice()
                    }
                }
                is Resource.Error -> {
                    viewBinding.rootLayout.errorSnack(
                        response.message!!,
                        Snackbar.LENGTH_LONG
                    )
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
            }
        }
    }

    private fun checkCartSize(cartSize: Int) {
        totalCartQTY = cartSize
        viewBinding.totalCartQty = cartSize
    }
}