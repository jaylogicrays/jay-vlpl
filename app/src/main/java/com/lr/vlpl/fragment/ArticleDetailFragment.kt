package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.databinding.FragmentArticleDetailBinding
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ArticleDetailViewModel

class ArticleDetailFragment :
    BaseFragment<FragmentArticleDetailBinding, ArticleDetailViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentArticleDetailBinding
    private lateinit var viewModel: ArticleDetailViewModel
    private var slug = ""

    companion object {
        fun newInstance(slug: String): ArticleDetailFragment {
            val fragment = ArticleDetailFragment()
            val bundle = Bundle()
            bundle.putString("slug", slug)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            slug = bundle.getString("slug")!!
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentArticleDetailBinding,
        viewModel: ArticleDetailViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        getArticleDetailResponse()
        viewModel.getArticleDetail(slug)
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentArticleDetailBinding
        get() = FragmentArticleDetailBinding::inflate

    override val bindingViewModel: Class<ArticleDetailViewModel>
        get() = ArticleDetailViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun getArticleDetailResponse() {
        viewModel.articleDetailResponse.observe(viewLifecycleOwner,
            Observer { response ->
                when (response) {
                    is Resource.Success -> {
                        viewBinding.sl.stopShimmerAnimation()
                        response.data!!.let { data ->
                            viewBinding.articleList = data
                            viewBinding.ctbToolbar.tvTitle.text = data.title.split(":").first()
                            viewBinding.simmerVisibility = true
                        }
                    }
                    is Resource.Error -> {
                        viewBinding.sl.stopShimmerAnimation()
                        viewBinding.simmerVisibility = true
                        viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        viewBinding.sl.startShimmerAnimation()
                        viewBinding.simmerVisibility = false
                    }
                    is Resource.ConnectionError -> {
                        viewBinding.sl.stopShimmerAnimation()
                        viewBinding.simmerVisibility = true
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            viewModel.getArticleDetail(slug)
                        }
                    }
                }
            })
    }
}
