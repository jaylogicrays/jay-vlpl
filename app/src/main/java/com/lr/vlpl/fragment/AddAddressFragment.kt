package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResult
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentAddAddressBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.model.Address
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.AddAddressViewModel

class AddAddressFragment :
    BaseFragment<FragmentAddAddressBinding, AddAddressViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentAddAddressBinding
    private lateinit var viewModel: AddAddressViewModel
    private lateinit var addressDetail: Address

    companion object {
        fun newInstance(address: Address): AddAddressFragment {
            val fragment = AddAddressFragment()
            val bundle = Bundle()
            bundle.putSerializable("address", address)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            addressDetail = bundle.getSerializable("address") as Address
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentAddAddressBinding,
        viewModel: AddAddressViewModel,
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewModel.viewBinding = viewBinding
        viewBinding.clickListener = this
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        clickOnToolbarItem()
        getUpdateAddressResponse()
        viewModel.setValue(addressDetail)
    }

    private fun clickOnToolbarItem() {
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener { requireActivity().onBackPressed() }
        viewBinding.ctbToolbar.tvTitle.text = getString(R.string.str_edit_address)
    }

    override val bindingViewModel: Class<AddAddressViewModel>
        get() = AddAddressViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAddAddressBinding
        get() = FragmentAddAddressBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnSave -> {
                if (viewModel.isValidate()) {
                    updateAndAddAddressApiCall()
                }
            }
        }
    }

    private fun updateAndAddAddressApiCall() {
        if (!viewModel.checkEditOrNot(addressDetail))
            viewModel.updateAddress()
        else
            requireActivity().onBackPressed()
    }

    //for Update Address
    private fun getUpdateAddressResponse() {
        viewModel.updateAddressResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    ToastHelper.showMessage(requireActivity(), response.message.toString())
                    sendFragmentResult()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        updateAndAddAddressApiCall()
                    }
                }
            }
        }
    }

    private fun sendFragmentResult() {
        val result = Bundle().apply {
            putBoolean("isAddressChanged", true)
        }
        setFragmentResult("isAddressListChanged", result)
        requireActivity().onBackPressed()
    }
}
