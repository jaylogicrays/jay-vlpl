package com.lr.vlpl.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.CustomBrowserActivity
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.activity.LoginActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentProfileBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.*
import com.lr.vlpl.viewmodel.ProfileViewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentProfileBinding,
        viewModel: ProfileViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        viewBinding.version = Utility.getInstance().getAppVersion()
        logoutResponse()
    }

    override val bindingViewModel: Class<ProfileViewModel>
        get() = ProfileViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentProfileBinding
        get() = FragmentProfileBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        val activity = (activity as DashboardActivity)
        when (p0!!.id) {
            R.id.cardMyAccount -> activity.addFragment(MyAccountFragment())
            R.id.cardChangePassword -> activity.addFragment(ChangePasswordFragment())
            R.id.cardOrderHistory -> activity.addFragment(OrderHistoryFragment())
            R.id.cardUploadDocument -> activity.addFragment(UploadedDocumentFragment())
            R.id.cardManageAddress -> activity.addFragment(ManageAddressFragment())
            R.id.cardDeleteAccount -> {}
            R.id.cardLogout -> {
                Utility.getInstance().showConfirmationDialog(
                    requireContext(),
                    requireContext().getString(R.string.str_logout_header),
                    resources.getString(R.string.str_logout_dialog_desc),
                    requireContext().getString(R.string.str_no),
                ) { viewModel.logout() }
            }
            R.id.tvPrivacyPolicy -> {
                if (BaseApplication.getInstance().isConnectionAvailable()) {
                    val intent = Intent(requireContext(), CustomBrowserActivity::class.java)
                    intent.putExtra(Constants.URL_KEY, Constants.PRIVACY_POLICY)
                    startActivity(intent)
                } else {
                    ToastHelper.showMessage(
                        requireActivity(),
                        "Please check your internet connection"
                    )
                }
            }
            R.id.tvTermsCondition -> {
                if (BaseApplication.getInstance().isConnectionAvailable()) {
                    val intent = Intent(requireContext(), CustomBrowserActivity::class.java)
                    intent.putExtra(Constants.URL_KEY, Constants.TERMS_CONDITION)
                    startActivity(intent)
                } else {
                    ToastHelper.showMessage(
                        requireActivity(),
                        "Please check your internet connection"
                    )
                }
            }
        }
    }

    private fun logoutResponse() {
        viewModel.logoutResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    logoutOrDeleteUser()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> CustomDialog.getInstance().showDialog(requireActivity())
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.logout()
                    }
                }
            }
        }
    }

    private fun logoutOrDeleteUser() {
        val token = SharedPreference.getValue(SharedPreference.FCM_TOKEN, "")
        CustomDialog.getInstance().hide()
        SharedPreference.clearPreferences()
        SharedPreference.setValue(SharedPreference.FCM_TOKEN, token)
        ActivityNav.getInstance()?.callNewActivity(requireActivity(), LoginActivity::class.java)
    }
}