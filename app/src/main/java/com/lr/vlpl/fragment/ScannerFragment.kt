package com.lr.vlpl.fragment

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.DialogBarcodePointsDetailBinding
import com.lr.vlpl.databinding.FragmentBarCodeScanBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.model.Barcode
import com.lr.vlpl.permission.PermissionCheck
import com.lr.vlpl.permission.PermissionHandler
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ScannerViewModel

class ScannerFragment : BaseFragment<FragmentBarCodeScanBinding, ScannerViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentBarCodeScanBinding
    lateinit var viewModel: ScannerViewModel
    private lateinit var client: FusedLocationProviderClient
    var latitude: String = ""
    var longitude: String = ""
    private var barcodeNumber: String = ""

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentBarCodeScanBinding,
        viewModel: ScannerViewModel,
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        // Initialize location client
        client = LocationServices.getFusedLocationProviderClient(requireActivity())
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
        checkPermission()
        manageBarcodeResult()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentBarCodeScanBinding
        get() = FragmentBarCodeScanBinding::inflate

    override val bindingViewModel: Class<ScannerViewModel>
        get() = ScannerViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    override fun onPause() {
        super.onPause()
        viewBinding.scannerView.stopCamera()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewBinding.scannerView.stopCamera()
    }

    private fun checkPermission() {
        PermissionCheck.check(requireContext(),
            arrayListOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION),
            "Camera permission is necessary to scan code",
            object : PermissionHandler() {
                override fun onGranted() {
                    getCurrentLocation()
                }

                override fun onDenied(
                    context: Context,
                    deniedPermissions: java.util.ArrayList<String>
                ) {
                    requireActivity().onBackPressed()
                }
            })
    }

    fun openScanner() {
        viewBinding.scannerView.startCamera()
        viewBinding.scannerView.setResultHandler {
            val barCodeSplit1 = it.text.split("V")
            if (barCodeSplit1.size == 2 && barCodeSplit1[0] == "") {
                if (it.text.split("B").size == 2) {
                    barcodeNumber = it.text
                    viewModel.getBarcodeDetails(it.text, latitude, longitude)
                } else {
                    ToastHelper.showMessage(requireActivity(), "Invalid Barcode")
                    openScanner()
                }
            } else {
                ToastHelper.showMessage(requireActivity(), "Invalid Barcode")
                openScanner()
            }
        }
    }

    //for current Lat, long
    fun getCurrentLocation() {
        // Initialize Location manager
        val locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // Check condition
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        ) {
            // When location service is enabled
            // Get last location
            client.lastLocation.addOnCompleteListener { task ->
                val location = task.result
                // Check condition
                if (location != null) {
                    // When location result is not null
                    latitude = location.latitude.toString()
                    longitude = location.longitude.toString()
                    openScanner()
                } else {
                    // When location result is null
                    // initialize location request
                    val locationRequest = LocationRequest.create()
                    locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    locationRequest.interval = 10000
                    locationRequest.fastestInterval = 10000 / 2
                    locationRequest.numUpdates = 1

                    // Initialize location call back
                    val locationCallback: LocationCallback = object : LocationCallback() {
                        override fun onLocationResult(locationResult: LocationResult) {
                            // Initialize location
                            val location1 = locationResult.lastLocation
                            latitude = location1?.latitude.toString()
                            longitude = location1?.longitude.toString()
                            openScanner()
                        }
                    }
                    // Request location updates
                    client.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.myLooper()
                    )
                }
            }
        } else {
            // When location service is not enabled
            // open location setting
            getGpsPermission(requireContext())
        }
    }

    private fun getGpsPermission(context: Context) {
        val googleApiClient: GoogleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = Priority.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result: PendingResult<LocationSettingsResult> =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())

        result.setResultCallback {
            val status: Status = it.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.d(
                    "GPSTag",
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.d(
                        "GPSTag",
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        startIntentSenderForResult(
                            status.resolution!!.intentSender,
                            100, null, 0, 0, 0, null
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Log.d("GPSTag", "PendingIntent unable to execute request.")
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                    Log.d(
                        "GPSTag",
                        "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                    )
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> when (resultCode) {
                Activity.RESULT_OK -> {
                    openScanner()
                }   // All required changes were successfully made
                Activity.RESULT_CANCELED -> {
                    ToastHelper.showMessage(requireActivity(), "Gps Canceled")
                    requireActivity().onBackPressed()
                }  // The user was asked to change settings, but chose not to
                else -> {
                    ToastHelper.showMessage(requireActivity(), "Please Try Again")
                }
            }
        }
    }

    private fun manageBarcodeResult() {
        viewModel.barcodeResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    if (response.data != null) {
                        showGetPointsDialog(response.data)
                    } else {
                        openScanner()
                        ToastHelper.showMessage(requireActivity(), response.message!!)
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getBarcodeDetails(barcodeNumber, latitude, longitude)
                    }
                }
            }
        }
    }

    private fun showGetPointsDialog(data: Barcode?) {
        val dialog = Dialog(requireContext(), R.style.DialogTheme)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        val binding = DialogBarcodePointsDetailBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)

        binding.tvIncentivePoint.text = data?.redeem_point.toString()
        binding.tvMedicinesName.text = data?.title.toString()

        dialog.show()

        Handler(Looper.getMainLooper()).postDelayed({
            dialog.dismiss()
            openScanner()
        }, 3000)
    }
}