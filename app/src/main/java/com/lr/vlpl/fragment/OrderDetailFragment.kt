package com.lr.vlpl.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.adapter.OrderDetailProductAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.DialogAddReviewBinding
import com.lr.vlpl.databinding.FragmentOrderDetailBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.utils.showSnack
import com.lr.vlpl.viewmodel.OrderDetailViewModel

class OrderDetailFragment :
    BaseFragment<FragmentOrderDetailBinding, OrderDetailViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentOrderDetailBinding
    lateinit var viewModel: OrderDetailViewModel
    val adapter = OrderDetailProductAdapter()
    private lateinit var tvAddReview: TextView
    lateinit var products: Products
    private lateinit var dialog: Dialog
    private var rate: Int? = 0
    private var orderId: Int = 0
    var review: String? = null

    companion object {
        fun newInstance(orderId: Int): OrderDetailFragment {
            val fragment = OrderDetailFragment()
            val bundle = Bundle()
            bundle.putInt("orderId", orderId)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            orderId = bundle.getInt("orderId")
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentOrderDetailBinding,
        viewModel: OrderDetailViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        getOrderDetails()
        viewModel.getOrderDetails(orderId)
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
        manageAddReviewResponse()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentOrderDetailBinding
        get() = FragmentOrderDetailBinding::inflate

    override val bindingViewModel: Class<OrderDetailViewModel>
        get() = OrderDetailViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun getOrderDetails() {
        viewModel.orderDetailsResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    viewBinding.dataLoad = true
                    viewBinding.shimmerOrderDetails.stopShimmerAnimation()
                    response.data!!.let { data ->
                        viewBinding.orderDetailsList = data
                        adapter.submitList(data.product_info)
                        viewBinding.rvOrderItem.adapter = adapter
                    }
                }
                is Resource.Error -> {
                    viewBinding.dataLoad = false
                    viewBinding.shimmerOrderDetails.stopShimmerAnimation()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    viewBinding.shimmerOrderDetails.startShimmerAnimation()
                    viewBinding.dataLoad = false
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getOrderDetails(orderId)
                    }
                }
            }
        }

        adapter.tvAddReviewClick = { productData, tvAddReview ->
            openReviewDialog(productData, tvAddReview)
        }
    }

    private fun openReviewDialog(products: Products, tvAddReview: TextView) {
        this.tvAddReview = tvAddReview
        this.products = products
        dialog = Dialog(requireContext(), R.style.DialogTheme)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(true)
        val dialogAddReviewBinding = DialogAddReviewBinding.inflate(layoutInflater)
        dialog.setContentView(dialogAddReviewBinding.root)

        dialogAddReviewBinding.btnSubmit.setOnClickListener {
            if (dialogAddReviewBinding.rbProductRating.rating.toInt() == 0) {
                dialogAddReviewBinding.tvErrorMsg.visibility = View.GONE
                ToastHelper.showMessage(requireActivity(), "Set Rating", Snackbar.LENGTH_LONG)
            } else if (dialogAddReviewBinding.etProductReview.text.toString() == "") {
                dialogAddReviewBinding.tvErrorMsg.visibility = View.VISIBLE
            } else {
                dialogAddReviewBinding.tvErrorMsg.visibility = View.GONE
                rate = dialogAddReviewBinding.rbProductRating.rating.toInt()
                review = dialogAddReviewBinding.etProductReview.text.toString()
                viewModel.addReview(
                    product_id = products.id,
                    rate = dialogAddReviewBinding.rbProductRating.rating.toInt(),
                    review = dialogAddReviewBinding.etProductReview.text.toString()
                )
            }
        }
        dialog.show()
    }

    private fun manageAddReviewResponse() {
        viewModel.addReviewResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    dialog.dismiss()
                    viewBinding.rootLayout.showSnack(response.message!!, Snackbar.LENGTH_LONG)
                    viewModel.getOrderDetails(orderId)
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    dialog.dismiss()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addReview(
                            product_id = products.id,
                            rate = rate!!,
                            review = review!!
                        )
                    }
                }
            }
        }
    }
}