package com.lr.vlpl.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.Observable
import androidx.databinding.ObservableInt
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.RegisterActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentUserAddressBinding
import com.lr.vlpl.model.City
import com.lr.vlpl.model.SpinnerModel
import com.lr.vlpl.model.StateCity
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import kotlinx.coroutines.*

class UserAddressFragment : BaseViewBindingFragment<FragmentUserAddressBinding>() {

    private lateinit var binding: FragmentUserAddressBinding
    private lateinit var states: ArrayList<StateCity>
    private var textChangedJob: Job? = null

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentUserAddressBinding) {
        binding = viewBinding
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        binding.btnNext.setOnClickListener {
            if (isValidate()) {
                val activity = (activity as RegisterActivity)
                activity.viewModel.buildingName = viewBinding.etBuilding.et.text.toString()
                activity.viewModel.streetName = viewBinding.etStreetName.et.text.toString()
                activity.viewModel.stateName = viewBinding.etState.et.text.toString()
                activity.viewModel.cityName = viewBinding.etCity.et.text.toString()
                activity.viewModel.pincode = viewBinding.etPinCode.et.text.toString()
                activity.viewModel.contactNo1 = viewBinding.etContactNo1.et.text.toString()
                activity.viewModel.contactNo2 = viewBinding.etContactNo2.et.text.toString()
                activity.onNextFragmentNavigation(RegisterActivity.NEXT)
            }
        }
//      getStateCity()
        setEditMode()
        getCityStateFromZipCode()
        manageGetCityStateFromZipCodeResponse()
    }

    private fun setEditMode() {
        binding.etCity.setBackgroundNonEditableColor(false)
        binding.etCity.setEnable(false)
        binding.etState.setBackgroundNonEditableColor(false)
        binding.etState.setEnable(false)
    }

    private fun manageGetCityStateFromZipCodeResponse() {
        (activity as RegisterActivity).viewModel
            .getCityStateFromZipcodeResponse
            .observe(viewLifecycleOwner) { response ->
                when (response) {
                    is Resource.ConnectionError -> {
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            (activity as RegisterActivity).viewModel.getCityStateFromZipCode(binding.etPinCode.et.text.toString())
                        }
                    }
                    is Resource.Error -> {
                        CustomDialog.getInstance().hide()
                        binding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                    is Resource.Success -> {
                        CustomDialog.getInstance().hide()
                        response.data?.let { data ->
                            binding.etCity.et.setText(data.city)
                            binding.etState.et.setText(data.state)
                        }
                    }
                }
            }
    }

    private fun getCityStateFromZipCode() {
        binding.etPinCode.et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                var lastInput = ""
                if (editable != null) {
                    val newtInput = editable.toString()
                    textChangedJob?.cancel()
                    if (lastInput != newtInput) {
                        lastInput = newtInput
                        textChangedJob = CoroutineScope(Dispatchers.IO).launch {
                            delay(1000)
                            if (lastInput == newtInput) {
                                (activity as RegisterActivity).viewModel.getCityStateFromZipCode(
                                    newtInput
                                )
                            }
                        }
                    } else {
                        textChangedJob = CoroutineScope(Dispatchers.IO).launch {
                            delay(1000)
                            (activity as RegisterActivity).viewModel.getCityStateFromZipCode(
                                newtInput
                            )
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUserAddressBinding
        get() = FragmentUserAddressBinding::inflate


    private fun getStateCity() {
        (activity as RegisterActivity).viewModel.getStateCity.observe(
            viewLifecycleOwner
        ) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    response.data?.let { data ->
                        states = arrayListOf()
                        states.addAll(data)
                        setStateSpinner()
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    binding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
//                  CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        (activity as RegisterActivity).viewModel.fetchStateAndCity()
                    }
                }
            }
        }
    }

    private fun setStateSpinner() {
        val stateList: ArrayList<SpinnerModel> = arrayListOf()
        stateList.add(SpinnerModel(0, requireContext().getString(R.string.str_choose_state)))
        states.forEach { state ->
            stateList.add(SpinnerModel(state.id, state.name))
        }
        binding.spinnerState.setSpinnerItems(stateList, 0)
        setStateChangeObserver()
        setCitySpinner(arrayListOf())
    }

    private fun setStateChangeObserver() {
        binding.spinnerState.selectedId.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                for (s in states.indices) {
                    if (states[s].id == (sender as ObservableInt).get()) {
                        setCitySpinner(states[s].city)
                        break
                    }
                }
            }
        })
    }

    private fun setCitySpinner(cities: List<City>) {
        val cityList: ArrayList<SpinnerModel> = arrayListOf()
        cityList.add(SpinnerModel(0, requireContext().getString(R.string.str_choose_city)))
        cities.forEach { city ->
            cityList.add(SpinnerModel(city.id, city.name))
        }
        binding.spinnerCity.setSpinnerItems(cityList, 0)
    }

    private fun isValidate(): Boolean {
        var isValid = true
        if (!binding.etBuilding.isValidate())
            isValid = false
        if (!binding.etStreetName.isValidate())
            isValid = false
        /*  if (!binding.etCity.isValidate())
              isValid = false
          if (!binding.etState.isValidate())
              isValid = false*/
        if (!binding.etPinCode.isValidate())
            isValid = false
        if (!binding.etContactNo1.isValidate())
            isValid = false
        return isValid
    }
}
