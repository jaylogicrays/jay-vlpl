package com.lr.vlpl.fragment

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.text.color
import androidx.core.text.scale
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.LoginActivity
import com.lr.vlpl.activity.RegisterActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentUserinfoBinding
import com.lr.vlpl.model.SpinnerModel
import com.lr.vlpl.model.UserType
import com.lr.vlpl.utils.ActivityNav
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack

class UserInfoFragment : BaseViewBindingFragment<FragmentUserinfoBinding>(), View.OnClickListener {

    private lateinit var viewBinding: FragmentUserinfoBinding
    private lateinit var userTypes: ArrayList<UserType>

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentUserinfoBinding) {
        this.viewBinding = viewBinding
        viewBinding.clickListener = this
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        setSpannableString()
        getUserTypes()
        onEyePasswordClick()
    }

    private fun onEyePasswordClick() {
        viewBinding.etPassword.passwordEyeVisible()
        viewBinding.etPassword.passwordEyeClick()

        viewBinding.etConfirmPassword.passwordEyeVisible()
        viewBinding.etConfirmPassword.passwordEyeClick()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUserinfoBinding
        get() = FragmentUserinfoBinding::inflate

    private fun setSpannableString() {
        val haveAccountText = SpannableStringBuilder()
            .append(requireContext().resources.getString(R.string.str_already_have_account))
            .bold {
                scale(1.1f) {
                    color(ContextCompat.getColor(requireContext(), R.color.colorTheme)) {
                        append(" " + requireContext().resources.getString(R.string.str_log_in))
                    }
                }
            }
        viewBinding.tvHaveAnAccount.text = haveAccountText
    }

    private fun getUserTypes() {
        (activity as RegisterActivity).viewModel.getUserTypes.observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response) {
                    is Resource.Success -> {
                        CustomDialog.getInstance().hide()
                        response.data?.let { data ->
                            userTypes = arrayListOf()
                            userTypes.addAll(data)
                            setUserTypeSpinner()
                        }
                    }
                    is Resource.Error -> {
                        CustomDialog.getInstance().hide()
                        viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                    }
                    is Resource.Loading -> {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                    is Resource.ConnectionError -> {
                        Utility.getInstance().showNoInternetDialog(requireContext()) {
                            (activity as RegisterActivity).viewModel.fetchUserTypes()
                        }
                    }
                }
            })
    }

    private fun setUserTypeSpinner() {
        val userTypeList: ArrayList<SpinnerModel> = arrayListOf()
        userTypeList.add(SpinnerModel(0, "Choose user type"))
        userTypes.forEach {
            userTypeList.add(SpinnerModel(it.id, it.name))
        }
        //setUserTypeChangeObserver()
        viewBinding.spinnerUserType.setSpinnerItems(userTypeList, 0)
    }

    private fun setUserTypeChangeObserver() {
        viewBinding.spinnerUserType.selectedId.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                (activity as RegisterActivity).viewModel.userType = try {
                    userTypes.first { e -> (e.id == viewBinding.spinnerUserType.selectedId.get()) }.slug
                } catch (e: NoSuchElementException) {
                    ""
                }
            }
        })
    }

    private fun isValidate(): Boolean {
        var isValid = true
        if (!viewBinding.spinnerUserType.isValidate())
            isValid = false
        if (!viewBinding.etFirstName.isValidate())
            isValid = false
        if (!viewBinding.etLastName.isValidate())
            isValid = false
        if (!viewBinding.etTradeName.isValidate())
            isValid = false
        if (!viewBinding.etEmail.isValidate())
            isValid = false
        if (!viewBinding.etPassword.isValidate())
            isValid = false
        if (!viewBinding.etConfirmPassword.isValidate()) {
            isValid = false
        }
        return isValid
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnNext -> {
                if (isValidate()) {
                    val activity = (activity as RegisterActivity)
                    activity.viewModel.userType =
                        userTypes.first { e ->
                            e.id == viewBinding.spinnerUserType.selectedId.get()
                        }.slug
                    activity.selectedUserType.value = activity.viewModel.userType
                    activity.viewModel.firstName = viewBinding.etFirstName.et.text.toString()
                    activity.viewModel.lastName = viewBinding.etLastName.et.text.toString()
                    activity.viewModel.firmName = viewBinding.etTradeName.et.text.toString()
                    activity.viewModel.email = viewBinding.etEmail.et.text.toString()
                    activity.viewModel.password = viewBinding.etPassword.et.text.toString()
                    activity.viewModel.confirmPassword =
                        viewBinding.etConfirmPassword.et.text.toString()
                    activity.onNextFragmentNavigation(RegisterActivity.NEXT)
                }
            }
            R.id.tvHaveAnAccount -> {
                ActivityNav.getInstance()?.callActivity(requireContext(), LoginActivity::class.java)
                ActivityNav.getInstance()!!.killActivity(requireActivity())
            }
        }
    }
}