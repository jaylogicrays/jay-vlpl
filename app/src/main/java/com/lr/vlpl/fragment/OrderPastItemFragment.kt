package com.lr.vlpl.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.Observable
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.adapter.ProductListAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.FragmentOrderPastItemBinding
import com.lr.vlpl.databinding.ItemSearchProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.PastOrderViewModel
import kotlinx.coroutines.flow.collectLatest

class OrderPastItemFragment :
    BaseFragment<FragmentOrderPastItemBinding, PastOrderViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentOrderPastItemBinding
    private lateinit var viewModel: PastOrderViewModel
    private lateinit var adapter: ProductListAdapter
    private var isLoad = true
    private var myProducts: Products? = null
    private var myItemSearchBinding: ItemSearchProductBinding? = null
    private var adapterPosition: Int? = 0
    private var productListAdapter: ProductListAdapter? = null
    private lateinit var _list: List<Products>
    var sequence = 0

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentOrderPastItemBinding,
        viewModel: PastOrderViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        _list = arrayListOf()
        adapter = ProductListAdapter()
        setAdapter()
        adapterClickHandle()
        manageAddToCartResponse()
        updateList()
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    private fun setAdapter() {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing1)
        viewBinding.rvProducts.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvProducts.setHasFixedSize(true)
        viewBinding.rvProducts.layoutManager = GridLayoutManager(context, 1)

        lifecycleScope.launchWhenStarted {
            adapter.loadStateFlow.collectLatest { loadState ->
                viewBinding.appendProgress.isVisible = loadState.source.append is LoadState.Loading
                if (loadState.refresh is LoadState.Loading) {
                    isLoad = true
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                    viewBinding.isOrderListEmpty = false
                } else {
                    if (isLoad) {
                        isLoad = false
                        CustomDialog.getInstance().hide()
                    }
                }

                if (loadState.refresh is LoadState.Error) {
                    CustomDialog.getInstance().hide()
                    (loadState.refresh as LoadState.Error).error.let {
                        if (it.message == "null")
                            viewBinding.isOrderListEmpty = true
                        else
                            viewBinding.isOrderListEmpty = true
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                CustomDialog.getInstance().hide()
                getProductResponse()
            } else {
                CustomDialog.getInstance().hide()
                Utility.getInstance().showNoInternetDialog(requireContext()) {
                    getProductResponse()
                }
            }
        }
    }

    private fun getProductResponse() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getPastOrderItemList.collectLatest { productData ->
                CustomDialog.getInstance().hide()
                viewBinding.rvProducts.adapter = adapter
                adapter.submitData(productData)
                _list = adapter.snapshot().items
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentOrderPastItemBinding
        get() = FragmentOrderPastItemBinding::inflate

    override val bindingViewModel: Class<PastOrderViewModel>
        get() = PastOrderViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
        }
    }

    private fun manageAddToCartResponse() {
        viewModel.getAddToCartResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    if (myItemSearchBinding != null && productListAdapter != null) {
                        myItemSearchBinding!!.tvCartItemCounter.text =
                            response.data!!.total_product_qty.toString()
                        if (myProducts != null) {
                            if (BaseApplication.getInstance().from == "add") {
                                productListAdapter!!.increaseCartValue(
                                    myProducts!!,
                                    adapterPosition!!,
                                    myItemSearchBinding!!
                                )
                            } else {
                                productListAdapter!!.decreaseCartValue(
                                    myProducts!!,
                                    adapterPosition!!,
                                    myItemSearchBinding!!
                                )
                            }
                        }
                    }
                    CustomDialog.getInstance().hide()
                    BaseApplication.getInstance().cartScreenUpdate = true
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_SHORT)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addToCart(
                            myProducts!!.id,
                            myProducts!!.ratio,
                            sequence
                        )
                    }
                }
            }
        }
    }

    private fun adapterClickHandle() {
        // Add product in cart
        adapter.onIncreaseQTY = { products, productListAdapter, myBinding, myPosition ->
            BaseApplication.getInstance().from = "add"
            adapterPosition = myPosition
            this.productListAdapter = productListAdapter
            myProducts = products
            myItemSearchBinding = myBinding
            if (myProducts!!.discount_type == 3) {
                sequence = myProducts!!.sequence + 1
            }
            viewModel.addToCart(products.id, products.ratio, sequence)
        }
        // remove product from cart
        adapter.onDecreaseQTY = { products, productListAdapter, myBinding, myPosition ->
            BaseApplication.getInstance().from = "remove"
            adapterPosition = myPosition
            val negativeVal: Int = (products.ratio - 1).inv()
            this.productListAdapter = productListAdapter
            myProducts = products
            myItemSearchBinding = myBinding
            if (myProducts!!.discount_type == 3) {
                sequence = myProducts!!.sequence - 1
            }
            viewModel.addToCart(products.id, negativeVal, sequence)
        }
    }

    private fun updateList() {
        BaseApplication.getInstance().searchScreenUpdate.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            @SuppressLint("NotifyDataSetChanged")
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                _list.forEach { products ->
                    if (products.id == BaseApplication.getInstance().productId.get()) {
                        if (products.discount_type == 3) {
                            when (BaseApplication.getInstance().from) {
                                "add" -> {
                                    products.cart_qty += products.ratio
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                }
                                "delete" -> {
                                    products.cart_qty = 0
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                }
                                else -> {
                                    products.cart_qty -= products.ratio
                                    products.cart_free_qty =
                                        BaseApplication.getInstance().productFreeQTY.get()
                                    products.total_cart_qty =
                                        BaseApplication.getInstance().productTotalQTY.get()
                                }
                            }
                        } else {
                            products.cart_qty = BaseApplication.getInstance().productBillQTY.get()
                        }
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        })
    }
}