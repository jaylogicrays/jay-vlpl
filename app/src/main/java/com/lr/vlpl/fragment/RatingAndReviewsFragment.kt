package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.adapter.RatingReviewAdapter
import com.lr.vlpl.databinding.FragmentRatingAndReviewsBinding
import com.lr.vlpl.model.Reviews

class RatingAndReviewsFragment(val reviews: Reviews) :
    BaseViewBindingFragment<FragmentRatingAndReviewsBinding>() {

    private lateinit var viewBinding: FragmentRatingAndReviewsBinding

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentRatingAndReviewsBinding) {
        this.viewBinding = viewBinding
        val adapter = RatingReviewAdapter(reviews.reviews_data)
        viewBinding.reviews = reviews
        viewBinding.rvRatingReview.adapter = adapter
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentRatingAndReviewsBinding
        get() = FragmentRatingAndReviewsBinding::inflate
}