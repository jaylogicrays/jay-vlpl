package com.lr.vlpl.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResultListener
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.adapter.ManageAddressAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.databinding.FragmentManageAddressBinding
import com.lr.vlpl.model.Address
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.AddressViewModel

class ManageAddressFragment :
    BaseFragment<FragmentManageAddressBinding, AddressViewModel, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentManageAddressBinding
    lateinit var viewModel: AddressViewModel

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentManageAddressBinding, viewModel: AddressViewModel
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        getAddressResponse()
        viewBinding.clickListener = this
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener("isAddressListChanged") { _: String, _: Bundle ->
            viewModel.getAddress()
        }
    }

    override val bindingViewModel: Class<AddressViewModel>
        get() = AddressViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentManageAddressBinding
        get() = FragmentManageAddressBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
        }
    }

    private fun getAddressResponse() {
        viewModel.getAddressResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    if (response.data != null) {
                        viewBinding.listSize = false
                        setupAddressAdapter(response.data)
                    } else {
                        viewBinding.listSize = true
                    }
                    viewBinding.rvAddress.hideShimmerAdapter()
                }
                is Resource.Error -> {
                    viewBinding.rvAddress.hideShimmerAdapter()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    viewBinding.rvAddress.showShimmerAdapter()
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.getAddress()
                    }
                }
            }
        }
    }

    private fun setupAddressAdapter(data: List<Address>) {
        val addressAdapter = ManageAddressAdapter()
        addressAdapter.submitList(data)
        addressAdapter.addressDetail = {
            (activity as DashboardActivity).addFragment(AddAddressFragment.newInstance(it))
        }
        viewBinding.rvAddress.adapter = addressAdapter
    }
}