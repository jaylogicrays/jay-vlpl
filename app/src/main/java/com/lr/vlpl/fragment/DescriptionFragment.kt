package com.lr.vlpl.fragment

import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.lr.vlpl.BaseViewBindingFragment
import com.lr.vlpl.R
import com.lr.vlpl.databinding.FragmentDescriptionBinding
import com.lr.vlpl.utils.Utility


class DescriptionFragment(private val productDescription: String?) :
    BaseViewBindingFragment<FragmentDescriptionBinding>() {

    private lateinit var viewBinding: FragmentDescriptionBinding

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentDescriptionBinding) {
        this.viewBinding = viewBinding
        if (productDescription != null) {
            if (productDescription.length > 100) {
                addReadMore(
                    Utility.getInstance().htmlToString(productDescription),
                    viewBinding.tvDescription
                )
            } else {
                viewBinding.tvDescription.text =
                    Utility.getInstance().htmlToString(productDescription)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentDescriptionBinding
        get() = FragmentDescriptionBinding::inflate

    private fun addReadMore(text: String, textView: TextView) {
        val ss = SpannableString(text.substring(0, 100) + getString(R.string.str_read_more))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadLess(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                } else {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun addReadLess(text: String, textView: TextView) {
        val ss = SpannableString("$text " + getString(R.string.str_read_less))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                addReadMore(text, textView)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                } else {
                    ds.color = ResourcesCompat.getColor(resources, R.color.colorYellow, null)
                }
            }
        }
        ss.setSpan(clickableSpan, ss.length - 10, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
    }
}