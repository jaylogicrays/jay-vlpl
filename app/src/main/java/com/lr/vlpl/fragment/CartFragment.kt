package com.lr.vlpl.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.BaseFragment
import com.lr.vlpl.R
import com.lr.vlpl.activity.DashboardActivity
import com.lr.vlpl.activity.PaymentActivity
import com.lr.vlpl.adapter.CartTopDealAdapter
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.customclasses.SpacesItemDecoration
import com.lr.vlpl.databinding.FragmentCartBinding
import com.lr.vlpl.databinding.ItemCartTopDealProductBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.model.Address
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.*
import com.lr.vlpl.viewmodel.CartViewModel

class CartFragment : BaseFragment<FragmentCartBinding, CartViewModel, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewBinding: FragmentCartBinding
    private lateinit var viewModel: CartViewModel
    var totalPoint = 0
    private var pointAmountValue = 0
    private var totalAmountFromPoint: MutableLiveData<Int> = MutableLiveData(0)
    private lateinit var topDealAdapter: CartTopDealAdapter
    var address: Address? = null
    private var isShowBackIcon = false
    private var productSlug = ""
    private lateinit var cartTopDealAdapter: CartTopDealAdapter
    private var adapterPosition: Int? = 0
    private var topDealProduct: Products? = null
    private lateinit var itemCartTopDealProductBinding: ItemCartTopDealProductBinding
    var sequence = 0
    private lateinit var _topDealProduct: List<Products>


    companion object {
        fun newInstance(isShowBackIcon: Boolean = false, productSlug: String = ""): CartFragment {
            val fragment = CartFragment()
            val bundle = Bundle()
            bundle.putBoolean("isShowBackIcon", isShowBackIcon)
            bundle.putString("productSlug", productSlug)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun getExtra() {
        val bundle = this.arguments
        if (bundle != null) {
            isShowBackIcon = bundle.getBoolean("isShowBackIcon")
            productSlug = bundle.getString("productSlug", "")
        }
    }

    override fun onViewCreated(
        instance: Bundle?,
        viewBinding: FragmentCartBinding,
        viewModel: CartViewModel
    ) {
        getExtra()
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        // This is used so that the binding can observe LiveData updates
        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.btnOrderConfirm.setVisibilityForMR()                                             // If user type mr than he won't able to place order
        viewBinding.llStokiestDetails.setStockiestViewVisibility()
        viewBinding.llMrDetails.setStockiestViewVisibility()
        viewBinding.ctbToolbar.ivBackIcon.visibility = if (isShowBackIcon) {
            View.VISIBLE
        } else {
            View.GONE
        }
        viewBinding.tvPriceLable.setPriceLableInCartAccordingUserType()
        Utility.getInstance()
            .hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)                    // get user incentive point api
        setAdapter()
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener(this)
        errorPointText()
        manageResponses()

        // top deal product recyclerview
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.stpi_spacing)
        viewBinding.rvProduct.addItemDecoration(SpacesItemDecoration(spacingInPixels, true))
        viewBinding.rvProduct.setHasFixedSize(true)
    }

    private fun manageResponses() {
        manageDeleteProductFromCartResponse()                                                        // delete product form cart
        manageGetCartProductData()                                                                   // get all cart products
        manageApplyPointResponse()
        manageAddToCartResponse()
        manageUpdateCartProductData()                                                                // Update all cart products
        manageTopDealProductAddToCartResponse()                                                      // Manage top deal product for add and remove from cart
        manageUpdateTopDealProductData()                                                             // update list after add or remove top deal product
    }

    private fun setAdapter() {
        viewBinding.amountPoints = totalAmountFromPoint.value
        viewModel.addCartProduct(requireContext())
        viewModel.viewBinding = viewBinding
        viewBinding.cartViewModel = viewModel
    }

    override val bindingViewModel: Class<CartViewModel>
        get() = CartViewModel::class.java

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentCartBinding
        get() = FragmentCartBinding::inflate

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> requireActivity().onBackPressed()
            R.id.btnOrderConfirm -> orderConfirm()
            R.id.tvEditAddress -> (activity as DashboardActivity).addFragment(
                AddAddressFragment.newInstance(
                    address!!
                )
            )
            R.id.btnApply -> {
                viewBinding.tvPointError.visibility =
                    if (viewBinding.etPoints.text!!.trim().isEmpty()) {
                        View.VISIBLE
                    } else {
                        viewModel.selectedPoints.value =
                            viewBinding.etPoints.text.toString().toInt()
                        View.GONE
                    }
                if (viewBinding.etPoints.text!!.trim().isNotEmpty()) {
                    viewModel.applyPoints(viewModel.selectedPoints.value!!)
                }
            }
        }
    }

    /**
     * Function for confirm order and redirect to payment screen
     */
    private fun orderConfirm() {
        val paymentArguments = PaymentActivity.PaymentArguments()
        paymentArguments.subTotal = viewBinding.tvSubTotal.text.toString().replace("Rs.", "")
        paymentArguments.addressId = address!!.id
        paymentArguments.gst = viewBinding.tvGst.text.toString().replace("Rs.", "")
        paymentArguments.mainTotal = viewBinding.tvTotalAmount.text.toString().replace("Rs.", "")
        paymentArguments.orderInstruction = viewBinding.etAdditionalInstruction.text.toString()
        if (pointAmountValue != 0) {
            paymentArguments.inputPoints = pointAmountValue
        }
        val intent = Intent(requireContext(), PaymentActivity::class.java)
        intent.putExtra(Constants.PAYMENT_PARAMS, paymentArguments)
        startActivity(intent)
    }

    /**
     * Function for handle error in redeem points
     */
    private fun errorPointText() {
        viewBinding.etPoints.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                viewBinding.tvPointError.visibility = View.GONE
                if (p0!!.isNotEmpty()) {
                    if (totalPoint < p0.toString().toInt()) {
                        viewBinding.etPoints.setText(totalPoint.toString())
                        ToastHelper.showMessage(
                            requireActivity(),
                            "You have only $totalPoint points"
                        )
                    }
                }
            }
        })
    }

    /**
     *  Function for remove product from cart and update list
     */
    private fun manageDeleteProductFromCartResponse() {
        viewModel.deleteCartItemResponse.observe(
            viewLifecycleOwner
        ) { response ->
            when (response) {
                is Resource.Loading -> {}
                is Resource.Success -> {
                    handleResponse()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {}
            }
        }
    }

    /**
     * handle UI after delete product from cart
     */
    private fun handleResponse() {
        val baseAppInstance = BaseApplication.getInstance()
        with(baseAppInstance) {
            cartScreenUpdate = true
            productFreeQTY.set(0)
            productTotalQTY.set(0)
            productBillQTY.set(0)
            sequence.set(0)
            searchScreenUpdate.set(!searchScreenUpdate.get())
        }
        CustomDialog.getInstance().hide()
        viewModel.itemDeleteFromCart()
        viewBinding.amountPoints = 0
        checkProductAvailableInTopDeal()
    }

    /**
     * function for show user cart products
     */
    private fun manageGetCartProductData() {
        viewModel.getCartProductResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (viewBinding.clCartView.visibility == View.GONE) {
                        viewBinding.clCartView.visibility = View.VISIBLE
                    }
                    viewBinding.isDataLoad = false
                    viewBinding.shimmerCartItem.startShimmerAnimation()                              // shimmer animation for list view
                    viewBinding.shimmerBottomPrice.startShimmerAnimation()
                    viewBinding.rvProduct.showShimmerAdapter()// shimmer animation for bottom price view
                }
                is Resource.Success -> {
                    viewBinding.isDataLoad = true
                    viewBinding.shimmerCartItem.stopShimmerAnimation()
                    viewBinding.shimmerBottomPrice.stopShimmerAnimation()
                    viewBinding.rvProduct.hideShimmerAdapter()
                    response.data?.let {
                        viewBinding.amountPoints = 0
                        viewBinding.cartModel =
                            it                                                    // cart data pass in data binding
                        address =
                            it.address                                                          // set address global for passing forward
                        totalPoint =
                            it.total_incentive_points                                        // check incentive point if point 0 view visibility gone
                        viewModel.setCartAdapter(it.product_list)
                        handleProductResponse(it.top_deals_list)
                        _topDealProduct =
                            it.top_deals_list                                           // top deal product store in array
                        BaseApplication.getInstance().totalCartQTY.set(it.total_cart_qty)             // set total cart QTY
                    }
                }
                is Resource.Error -> {
                    viewBinding.shimmerCartItem.stopShimmerAnimation()
                    viewBinding.shimmerBottomPrice.stopShimmerAnimation()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addCartProduct(requireActivity())
                    }
                }
            }
        }
    }

    /**
     * function for update user cart products
     */
    private fun manageUpdateCartProductData() {
        viewModel.updateCartProductResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    BaseApplication.getInstance().cartScreenUpdate = true
                    response.data?.let {
                        if (viewModel.myItemAddCartProductBinding != null) {
                            if (viewModel.myProducts != null) {
                                if (BaseApplication.getInstance().from == "add") {
                                    viewModel.onIncreaseQTYCalculation(
                                        viewModel.myProducts!!,
                                        viewModel.adapterPosition!!,
                                        viewModel.myItemAddCartProductBinding!!
                                    )
                                } else {
                                    viewModel.onDecreaseQTYCalculation(
                                        viewModel.myProducts!!,
                                        viewModel.adapterPosition!!,
                                        viewModel.myItemAddCartProductBinding!!
                                    )
                                }
                            }
                        }
                        viewBinding.amountPoints = 0
                        viewBinding.cartModel = it  // cart data pass in data binding
                        BaseApplication.getInstance().totalCartQTY.set(it.total_cart_qty)            // set total cart QTY
                        checkProductAvailableInTopDeal()
                    }
                    CustomDialog.getInstance().hide()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addCartProduct(requireActivity())
                    }
                }
            }
        }
    }

    /**
     * get Amount from point and set amount
     */
    private fun manageApplyPointResponse() {
        viewModel.getAmountPointResult.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(requireActivity())
                }
                is Resource.Success -> {
                    BaseApplication.getInstance().cartScreenUpdate = true
                    CustomDialog.getInstance().hide()
                    totalAmountFromPoint.value = response.data!!.amount
                    pointAmountValue = totalAmountFromPoint.value!!
                    viewBinding.amountPoints = totalAmountFromPoint.value
                    viewBinding.etPoints.setText("")
                    (activity as DashboardActivity).viewModel.getUserIncentive()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    totalAmountFromPoint.value = 0
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        if (viewBinding.etPoints.text!!.trim().isNotEmpty()) {
                            viewModel.applyPoints(viewModel.selectedPoints.value!!)
                        }
                    }
                }
            }
        }
    }

    override fun setMenuVisibility(isVisibleToUser: Boolean) {
        super.setMenuVisibility(isVisibleToUser)
        if (isVisibleToUser && BaseApplication.getInstance().cartScreenUpdate) {
            viewModel.addCartProduct(requireContext())
            BaseApplication.getInstance().cartScreenUpdate = false
        }
    }

    /**
     * function for handle add and remove product from cart
     */
    private fun manageAddToCartResponse() {
        viewModel.getAddToCartResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    viewModel.updateCartProduct(requireContext())
                    BaseApplication.getInstance().productId.set(viewModel.myProducts!!.id)
                    BaseApplication.getInstance().productBillQTY.set(response.data!!.total_product_qty)
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_SHORT)
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addToCart(
                            viewModel.myProducts!!.id,
                            viewModel.myProducts!!.ratio,
                            viewModel.sequence
                        )
                    }
                }
            }
        }
    }

    /**
     * Section for top deal product
     * add to cart and remove from cart function
     */
    private fun handleProductResponse(topDealsList: List<Products>) {
        if (topDealsList.isEmpty()) {
            viewBinding.tvNoProduct.visibility = View.VISIBLE
            viewBinding.rvProduct.visibility = View.GONE
        } else {
            viewBinding.tvNoProduct.visibility = View.GONE
            viewBinding.rvProduct.visibility = View.VISIBLE
            setProductList(topDealsList)
        }
    }

    /**
     * set product data in recyclerView
     */
    private fun setProductList(product: List<Products>) {
        topDealAdapter = CartTopDealAdapter(product, viewModel)
        viewBinding.rvProduct.adapter = topDealAdapter
        adapterClickHandle(topDealAdapter)
    }

    /**
     * function for handle click listsner of top deal products
     */
    private fun adapterClickHandle(topDealAdapter: CartTopDealAdapter) {
        // Add product in cart
        topDealAdapter.onIncreaseQTY = { products, cartTopDealAdapter, myBinding, myPosition ->
            with(products) {
                BaseApplication.getInstance().from = "add"
                adapterPosition = myPosition
                cartTopDealAdapter.apply {
                    topDealProduct = this@with
                    itemCartTopDealProductBinding = myBinding
                }
                val sequence = if (discount_type == 3) this.sequence + 1 else this.sequence
                viewModel.topDealProductAddToCart(id, ratio, sequence)
            }
        }

        // remove product from cart
        topDealAdapter.onDecreaseQTY = { products, cartTopDealAdapter, myBinding, myPosition ->
            with(products) {
                BaseApplication.getInstance().from = "remove"
                adapterPosition = myPosition
                cartTopDealAdapter.apply {
                    topDealProduct = this@with
                    itemCartTopDealProductBinding = myBinding
                }
                val sequence = if (discount_type == 3) this.sequence - 1 else this.sequence
                val negativeVal = (ratio - 1).inv()
                viewModel.topDealProductAddToCart(id, negativeVal, sequence)
            }
        }
    }

    /**
     * function for add and remove form cart for top deal products
     */
    private fun manageTopDealProductAddToCartResponse() {
        viewModel.getTopDealProductAddToCartResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                    if (!CustomDialog.getInstance().isDialogShowing) {
                        CustomDialog.getInstance().showDialog(requireActivity())
                    }
                }
                is Resource.Success -> {
                    viewModel.updateTopDealProductProduct(requireContext())
                    BaseApplication.getInstance().productId.set(topDealProduct!!.id)
                    BaseApplication.getInstance().productBillQTY.set(response.data!!.total_product_qty)
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_SHORT)

                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(requireContext()) {
                        viewModel.addToCart(topDealProduct!!.id, topDealProduct!!.ratio, sequence)
                    }
                }
            }
        }
    }

    /**
     * function for update user cart products after add top deal products
     */
    private fun manageUpdateTopDealProductData() {
        viewModel.updateTopDealProductResponse.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    response.data?.let {
                        if (topDealProduct != null) {
                            if (BaseApplication.getInstance().from == "add") {
                                cartTopDealAdapter.increaseCartValue(
                                    topDealProduct!!,
                                    adapterPosition!!,
                                    itemCartTopDealProductBinding
                                )
                            } else {
                                cartTopDealAdapter.decreaseCartValue(
                                    topDealProduct!!,
                                    adapterPosition!!,
                                    itemCartTopDealProductBinding
                                )
                            }
                        }
                        viewBinding.amountPoints = 0
                        viewBinding.cartModel = it  // cart data pass in data binding
                        BaseApplication.getInstance().totalCartQTY.set(it.total_cart_qty)            // set total cart QTY
                        viewModel.checkProductAvailableInCart()
                    }
                    CustomDialog.getInstance().hide()
                    BaseApplication.getInstance().cartScreenUpdate = true
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
                    CustomDialog.getInstance().hide()
                }
            }
        }
    }

    /**
     * Function for check and update top deal product if user change QTY in cart product
     * using user id update a
     * @param = cart QTY
     * @param = cart free QTY
     * @param = cart total QTY
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun checkProductAvailableInTopDeal() {
        _topDealProduct.forEach { product ->
            if (product.id == BaseApplication.getInstance().productId.get()) {
                if (product.discount_type == 3) {
                    when (BaseApplication.getInstance().from) {
                        "add" -> {
                            product.cart_qty += product.ratio
                            product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            product.sequence = BaseApplication.getInstance().sequence.get()
                        }
                        "delete" -> {
                            product.cart_qty = 0
                            product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            product.sequence = BaseApplication.getInstance().sequence.get()
                        }
                        else -> {
                            product.cart_qty -= product.ratio
                            product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            product.sequence = BaseApplication.getInstance().sequence.get()
                        }
                    }
                } else {
                    product.cart_qty = BaseApplication.getInstance().productBillQTY.get()
                }
                topDealAdapter.notifyDataSetChanged()
            }
        }
    }
}
