package com.lr.vlpl

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import com.lr.vlpl.activity.LoginActivity
import com.lr.vlpl.api.Resource
import com.lr.vlpl.utils.ActivityNav
import com.lr.vlpl.utils.CheckInternetConnection
import com.lr.vlpl.utils.SharedPreference

class BaseApplication : Application() {

    private lateinit var cld: LiveData<Boolean>
    private var isConnected: Boolean = false
    lateinit var productCartCount: ObservableInt
    lateinit var totalCartQTY: ObservableInt
    var cartScreenUpdate: Boolean = false
    lateinit var searchScreenUpdate: ObservableBoolean
    lateinit var notificationUpdate: ObservableBoolean
    lateinit var productId: ObservableInt
    lateinit var productBillQTY: ObservableInt
    lateinit var productFreeQTY: ObservableInt
    lateinit var productTotalQTY: ObservableInt
    lateinit var sequence: ObservableInt
    var from = ""

    //any written in this companion object is static you can access this variable using ApplicationLoader.REQUEST_TIMEOUT
    companion object {
        var enterAnimationFragment: Int = 0
        var exitAnimationFragment: Int = 0
        var enterAnimationActivity: Int = 0
        var exitAnimationActivity: Int = 0
        var tempPassword: String = "0"
        lateinit var appInstance: BaseApplication
        var byteArray: ByteArray? = null

        @Synchronized
        fun getInstance(): BaseApplication {
            return appInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        productCartCount = ObservableInt(0)
        totalCartQTY = ObservableInt(0)
        productId = ObservableInt(0)
        productBillQTY = ObservableInt(0)
        productFreeQTY = ObservableInt(0)
        productTotalQTY = ObservableInt(0)
        sequence = ObservableInt(0)
        searchScreenUpdate = ObservableBoolean(false)
        notificationUpdate = ObservableBoolean(false)
        cld = CheckInternetConnection(this)
        observeConnectionStatus()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    fun isConnectionAvailable(): Boolean {
        return isConnected
    }

    private fun observeConnectionStatus() {
        cld.observeForever { t ->
            isConnected = t!!
        }
    }

    fun <T> userInactive(): Resource.Error<T> {
        val token = SharedPreference.getValue(SharedPreference.FCM_TOKEN, "")
        SharedPreference.clearPreferences()
        SharedPreference.setValue(SharedPreference.FCM_TOKEN, token)
        ActivityNav.getInstance()?.callNewActivity(applicationContext, LoginActivity::class.java)
        return Resource.Error("User inactive by admin")
    }
}