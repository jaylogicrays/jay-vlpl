package com.lr.vlpl.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.lr.vlpl.model.BaseModel
import com.lr.vlpl.model.PaginationResults
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class BasePagingSource<T : Any>(private val endPoint: suspend (Int) -> Response<BaseModel<PaginationResults<T>>>) :
    PagingSource<Int, T>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> {
        return try {
            val nextPageNumber = params.key ?: 1
            val response = endPoint(nextPageNumber)
            val results = response.body()!!.result.values.iterator().next()
            val responseData: MutableList<T> = mutableListOf()

            if (results.list.isNotEmpty()) {
                responseData.addAll(results.list)
                LoadResult.Page(
                    data = responseData,
                    prevKey = if (nextPageNumber == 1) null else nextPageNumber,
                    nextKey = if (nextPageNumber < results.last_page) nextPageNumber + 1 else null
                )
            } else {
                LoadResult.Error(Throwable("empty"))
            }
        } catch (exception: IOException) {
            LoadResult.Error(Throwable(exception.message))
        } catch (exception: HttpException) {
            LoadResult.Error(Throwable(exception.message))
        } catch (e: Exception) {
            LoadResult.Error(Throwable(e.message))
        }
    }

    override fun getRefreshKey(state: PagingState<Int, T>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}
