package com.lr.vlpl.repository

import com.lr.vlpl.api.ApiService
import okhttp3.MultipartBody

class AppRepository(private val apiService: ApiService) : BaseRepository() {

    suspend fun getHomeProducts() = safeApiCall { apiService.getHomeProducts() }

    suspend fun getArticleDetail(slug: String) = safeApiCall { apiService.getArticleDetail(slug) }

    suspend fun getNotification() = safeApiCall { apiService.getNotification() }

    suspend fun getProductDetails(slug: String) = safeApiCall { apiService.getProductDetails(slug) }

    suspend fun orderList() = safeApiCall { apiService.orderList() }

    suspend fun orderDetails(orderId: Int) = safeApiCall { apiService.orderDetails(orderId) }

    suspend fun getCategory() = safeApiCall { apiService.getCategory() }

    suspend fun getCartProduct() = safeApiCall { apiService.getCartProduct() }

    suspend fun addReview(params: HashMap<String, Any>) =
        safeApiCall { apiService.addReview(params) }

    suspend fun placeOrder(params: HashMap<String, Any>) =
        safeApiCall { apiService.placeOrder(params) }

    suspend fun getBarcodeDetails(params: HashMap<String, Any>) =
        safeApiCall { apiService.getBarcodeDetails(params) }

    suspend fun getAddress() = safeApiCall { apiService.getAddress() }

    suspend fun updateAddress(params: HashMap<String, Any>) =
        safeApiCall { apiService.updateAddress(params) }

    suspend fun deleteProductFromCart(cartID: Int) =
        safeApiCall { apiService.deleteProductFromCart(cartID) }

    suspend fun getApplyPoint(params: HashMap<String, Any>) =
        safeApiCall { apiService.getApplyPoint(params) }

    suspend fun login(params: HashMap<String, Any>) = safeApiCall { apiService.login(params) }

    suspend fun getUserTypes() = safeApiCall { apiService.getUserTypes() }

    suspend fun getStateCity() = safeApiCall { apiService.getStateCity() }

    suspend fun register(multipartBody: MultipartBody) =
        safeApiCall {
            apiService.createUser(
                "multipart/form-data; boundary=" + multipartBody.boundary,
                multipartBody
            )
        }

    suspend fun forgotPassword(params: HashMap<String, Any>) =
        safeApiCall { apiService.forgotPassword(params) }

    suspend fun getProfile() = safeApiCall { apiService.getProfile() }

    suspend fun updateProfile(params: HashMap<String, Any>) =
        safeApiCall { apiService.updateProfile(params) }

    suspend fun changePassword(params: HashMap<String, Any>) =
        safeApiCall { apiService.changePassword(params) }

    suspend fun logout() = safeApiCall { apiService.logout() }

    suspend fun addToCart(params: HashMap<String, Any>) =
        safeApiCall { apiService.addToCart(params) }

    suspend fun searchCategoryWiseProduct(queries: HashMap<String, String>) =
        safeApiCall { apiService.searchCategoryProduct(queries) }

    suspend fun topDealProduct(queries: HashMap<String, String>) =
        safeApiCall { apiService.topDealProduct(queries) }

    suspend fun getUserIncentivePoint() = safeApiCall { apiService.getUserIncentivePoint() }

    suspend fun deleteUserAccount() = safeApiCall { apiService.deleteUserAccount() }

    suspend fun connectUs(params: HashMap<String, Any>) =
        safeApiCall { apiService.connectUs(params) }

    suspend fun getInTouch(params: HashMap<String, Any>) =
        safeApiCall { apiService.getInTouch(params) }

    suspend fun getCityStateFromZipCode(pincode: String) =
        safeApiCall { apiService.getCityStateFromZipCode(pincode) }

    suspend fun reOrder(params: HashMap<String, Any>) = safeApiCall { apiService.reorder(params) }

    suspend fun uploadInvoice(multipartBody: MultipartBody) =
        safeApiCall {
            apiService.uploadInvoice(
                "multipart/form-data; boundary=" + multipartBody.boundary,
                multipartBody
            )
        }


//    unused Api's
//    suspend fun addAddress(params: HashMap<String, Any>) = safeApiCall { apiService.addAddress(params) }
//
//    suspend fun changePrimaryAddress(params: HashMap<String, Any>) = safeApiCall { apiService.changePrimaryAddress(params) }
//
//    suspend fun deleteAddress(id: Int) = safeApiCall { apiService.deleteAddress(id) }

}
