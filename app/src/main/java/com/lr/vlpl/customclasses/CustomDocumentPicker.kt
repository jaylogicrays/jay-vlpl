package com.lr.vlpl.customclasses

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.lr.vlpl.R
import com.lr.vlpl.permission.PermissionCheck
import com.lr.vlpl.permission.PermissionHandler
import com.lr.vlpl.picker.MediaPicker
import com.lr.vlpl.picker.PickerResultHandler
import com.lr.vlpl.utils.Utility
import java.io.File

class CustomDocumentPicker : LinearLayout {

    private lateinit var tvTitle: TextView
    private lateinit var tvError: TextView
    private lateinit var tvDocName: TextView
    private lateinit var llSelectDoc: LinearLayout
    private lateinit var rlDocAttach: RelativeLayout
    private lateinit var ivRemoveDoc: ImageView
    private var emptyValidateText: String? = null
    var selectedFile: File? = null

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        inIt(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        inIt(context, attrs)
    }

    private fun inIt(context: Context, attrs: AttributeSet) {
        val itemView: View =
            LayoutInflater.from(context).inflate(R.layout.custom_document_picker, this, true)
        tvTitle = itemView.findViewById(R.id.tvTitle)
        tvError = itemView.findViewById(R.id.tvError)
        tvDocName = itemView.findViewById(R.id.tvDocName)
        llSelectDoc = itemView.findViewById(R.id.llSelectDoc)
        rlDocAttach = itemView.findViewById(R.id.rlDocAttach)
        ivRemoveDoc = itemView.findViewById(R.id.ivRemoveDoc)

        attrs.let {
            val typedArray =
                context.obtainStyledAttributes(it, R.styleable.CustomDocumentPicker, 0, 0)
            val value = typedArray.getString(R.styleable.CustomDocumentPicker_titleText)
            if (typedArray.getString(R.styleable.CustomDocumentPicker_docEmptyValidateText) != null)
                emptyValidateText =
                    typedArray.getString(R.styleable.CustomDocumentPicker_docEmptyValidateText)
            tvTitle.text = value

            llSelectDoc.setOnClickListener {
                checkPermission()
            }
            ivRemoveDoc.setOnClickListener {
                hideShowViews(false)
            }
            typedArray.recycle()
        }
    }

    private fun checkPermission() {
        PermissionCheck.check(context, Utility.getInstance().getPermissionForSDK33(),
            "External storage permission is necessary", object : PermissionHandler() {
                override fun onGranted() {
                    openImagePicker()
                }

                override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                }
            })
    }

    private fun openImagePicker() {
        MediaPicker.getImageFromMedia(context,
            isFromGallery = true,
            isFromCamera = true,
            isAttachFile = true,
            isCropping = false,
            handler = object : PickerResultHandler() {
                override fun onSuccess(bitmap: Bitmap?, file: File?, fileName: String) {
                    tvDocName.text = fileName
                    selectedFile = file
                    hideShowViews(true)
                }

                override fun onFailure(message: String) {
                    hideShowViews(false)
                }
            })
    }

    private fun hideShowViews(isAttached: Boolean) {
        if (isAttached) {
            rlDocAttach.visibility = VISIBLE
            llSelectDoc.visibility = GONE
        } else {
            rlDocAttach.visibility = GONE
            llSelectDoc.visibility = VISIBLE
            tvDocName.text = ""
        }
        isValidate()
    }

    fun isValidate(): Boolean {
        return if (tvDocName.text.toString().trim().isEmpty()) {
            tvError.text = emptyValidateText
            tvError.visibility = View.VISIBLE
            false
        } else {
            tvError.text = ""
            tvError.visibility = View.GONE
            true
        }
    }

    fun removeErrorMessage() {
        tvError.text = ""
        tvError.visibility = View.GONE
    }
}