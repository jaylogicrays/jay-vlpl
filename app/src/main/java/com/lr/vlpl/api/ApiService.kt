package com.lr.vlpl.api

import com.lr.vlpl.model.*
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("user-types")
    suspend fun getUserTypes(): Response<BaseModel<List<UserType>>>

    @GET("state-with-city")
    suspend fun getStateCity(): Response<BaseModel<List<StateCity>>>

    @Headers("Accept:application/json")
    @POST("register")
    suspend fun createUser(
        @Header("Content-Type") contentType: String,
        @Body multipartBody: MultipartBody
    ): Response<BaseModel<String>>

    @POST("forgot-password")
    suspend fun forgotPassword(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("login")
    suspend fun login(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @GET("get-profile")
    suspend fun getProfile(): Response<BaseModel<UserDetails>>

    @GET("home")
    suspend fun getHomeProducts(): Response<BaseModel<HomeModel>>

    @POST("update-profile")
    suspend fun updateProfile(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("change-password")
    suspend fun changePassword(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("add-address")
    suspend fun addAddress(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @GET("get-address-list")
    suspend fun getAddress(): Response<BaseModel<List<Address>>>

    @DELETE("delete-address/{id}")
    suspend fun deleteAddress(@Path(RequestParams.ID) id: Int): Response<BaseModel<String>>

    @POST("update-address")
    suspend fun updateAddress(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("change-primary-address")
    suspend fun changePrimaryAddress(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("get-article-list")
    suspend fun getArticlesList(@Query(RequestParams.PAGE) page: Int): Response<BaseModel<PaginationResults<Articles>>>

    @GET("get-article-detail/{slug}")
    suspend fun getArticleDetail(@Path(RequestParams.SLUG) title: String): Response<BaseModel<Articles>>

    @GET("logout")
    suspend fun logout(): Response<BaseModel<String>>

    @GET("get-push-notification-list")
    suspend fun getNotification(): Response<BaseModel<List<Notification>>>

    @POST("product/getallproducts")
    suspend fun getProductList(
        @Query(RequestParams.PAGE) page: Int,
        @QueryMap(encoded = true) queries: Map<String, String>
    ): Response<BaseModel<PaginationResults<Products>>>

    @GET("product/details/{slug}")
    suspend fun getProductDetails(@Path(RequestParams.SLUG) slug: String): Response<BaseModel<ProductDetail>>

    @POST("cart/addToCart")
    suspend fun addToCart(@Body params: HashMap<String, Any>): Response<BaseModel<Products>>

    @POST("cart/cartDelete")
    suspend fun deleteProductFromCart(@Query(RequestParams.CART_ID) cartId: Int): Response<BaseModel<String>>

    @GET("cart/")
    suspend fun getCartProduct(): Response<BaseModel<CartProductModel>>

    @POST("product/categories-wise")
    suspend fun searchCategoryProduct(@QueryMap(encoded = true) queries: Map<String, String>): Response<BaseModel<SearchProductModel>>

    @POST("product/top-deals-product")
    suspend fun topDealProduct(@QueryMap(encoded = true) queries: Map<String, String>): Response<BaseModel<TopDealProductModel>>

    @POST("order")
    suspend fun orderList(): Response<BaseModel<OrderData>>

    @POST("order/detail/{id}")
    suspend fun orderDetails(@Path(RequestParams.ID) id: Int): Response<BaseModel<Order>>

    @GET("category/get-category")
    suspend fun getCategory(): Response<BaseModel<List<Category>>>

    @GET("get-incentive-points")
    suspend fun getUserIncentivePoint(): Response<BaseModel<UserDetails>>

    @POST("review")
    suspend fun addReview(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @GET("order/past-order-items")
    suspend fun getPastOrderItem(@Query(RequestParams.PAGE) page: Int): Response<BaseModel<PaginationResults<Products>>>

    @POST("apply-points")
    suspend fun getApplyPoint(@Body params: HashMap<String, Any>): Response<BaseModel<Products>>

    @POST("order/place")
    suspend fun placeOrder(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("use-barcode")
    suspend fun getBarcodeDetails(@Body params: HashMap<String, Any>): Response<BaseModel<Barcode>>

    @GET("remove-user")
    suspend fun deleteUserAccount(): Response<BaseModel<String>>

    @POST("/api/connect-us")
    suspend fun connectUs(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("/api/get-in-touch")
    suspend fun getInTouch(@Body params: HashMap<String, Any>): Response<BaseModel<String>>

    @POST("/api/get-city-state")
    suspend fun getCityStateFromZipCode(@Query(RequestParams.PIN_CODE) pincode: String): Response<BaseModel<Address>>

    @POST("/api/order/reorder-items")
    suspend fun reorder(@Body params: HashMap<String, Any>): Response<BaseModel<OrderData>>

    @Headers("Accept:application/json")
    @POST("/api/order/upload-invoice")
    suspend fun uploadInvoice(
        @Header("Content-Type") contentType: String,
        @Body multipartBody: MultipartBody
    ): Response<BaseModel<String>>
}
