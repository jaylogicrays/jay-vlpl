package com.lr.vlpl.api

import com.lr.vlpl.helper.LoginHelper
import okhttp3.Interceptor
import okhttp3.Response

internal class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
            .addHeader("Accept", "application/json")
        val authToken: String = LoginHelper.getInstance()?.getAuthToken().toString()
        if (authToken.isNotEmpty()) {
            builder.addHeader("Authorization", "Bearer $authToken")
        }
        return chain.proceed(builder.build())
    }
}