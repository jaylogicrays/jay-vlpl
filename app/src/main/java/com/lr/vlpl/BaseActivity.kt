package com.lr.vlpl

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.lr.vlpl.repository.BaseRepository
import com.lr.vlpl.viewmodel.ViewModelFactory

abstract class BaseActivity<VM : ViewModel, VB : ViewBinding, R : BaseRepository> :
    AppCompatActivity() {

    private var binding: ViewBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = bindingInflater.invoke(layoutInflater)
        setContentView(binding?.root)
        val viewModel = ViewModelProvider(this, ViewModelFactory(repository))[bindingViewModel]
        onCreate(savedInstanceState, viewModel, binding as VB)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    protected abstract fun onCreate(instance: Bundle?, viewModel: VM, viewBinding: VB)

    protected abstract val bindingInflater: (LayoutInflater) -> VB

    protected abstract val bindingViewModel: Class<VM>

    protected abstract val repository: R
}