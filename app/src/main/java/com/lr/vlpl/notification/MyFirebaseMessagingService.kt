package com.lr.vlpl.notification

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.utils.SharedPreference

/*
* Use for receive firebase notification
*/
class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        SharedPreference.setValue(SharedPreference.FCM_TOKEN, token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        BaseApplication.getInstance().notificationUpdate.set(true)
        if (applicationInForeground()) {
            sendNotification(message)
        }
    }

    //check app in foreground
    private fun applicationInForeground(): Boolean {
        var isActivityFound = false
        if ((getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .runningAppProcesses[0].processName
                .equals(packageName, ignoreCase = true)
        ) {
            isActivityFound = true
        }
        return isActivityFound
    }

    //Send notification
    @SuppressLint("UnspecifiedImmutableFlag")
    private fun sendNotification(message: RemoteMessage) {
        val intent = Intent().apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent: PendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getActivity(
                this, (System.currentTimeMillis() and 0xfffffff).toInt(),
                intent, PendingIntent.FLAG_IMMUTABLE
            )
        } else {
            PendingIntent.getActivity(
                this, (System.currentTimeMillis() and 0xfffffff).toInt(),
                intent, PendingIntent.FLAG_UPDATE_CURRENT
            )
        }

        val notificationChannelId = resources.getString(R.string.app_name)
        val notificationBuilder = NotificationCompat.Builder(this, notificationChannelId)
            .setWhen(System.currentTimeMillis())
            .setShowWhen(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setContentTitle(message.notification?.title)
            .setContentText(message.notification?.body)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)
            .setColor(ContextCompat.getColor(applicationContext, R.color.teal_200))
            .setAutoCancel(true)
            .setGroupSummary(true)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel =
                NotificationChannel(notificationChannelId, notificationChannelId, importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(true)
            notificationBuilder.setChannelId(notificationChannelId)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(
            (System.currentTimeMillis() and 0xfffffff).toInt(),
            notificationBuilder.build()
        )
    }
}


