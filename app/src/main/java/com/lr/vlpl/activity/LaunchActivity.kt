package com.lr.vlpl.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.lr.vlpl.R
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.utils.ActivityNav

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        Handler(Looper.getMainLooper()).postDelayed({
            if (LoginHelper.getInstance()!!.isUserLoggedIn()) {
                ActivityNav.getInstance()?.callActivity(this, DashboardActivity::class.java)
            } else {
                ActivityNav.getInstance()?.callActivity(this, WelcomeActivity::class.java)
            }
            ActivityNav.getInstance()?.killActivity(this)
        }, 3000)
    }
}