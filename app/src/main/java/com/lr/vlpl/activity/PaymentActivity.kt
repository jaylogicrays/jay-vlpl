package com.lr.vlpl.activity

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseActivity
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.FragmentPaymentBinding
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.ActivityNav
import com.lr.vlpl.utils.Constants
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.PaymentViewModel
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONObject
import java.io.Serializable
import kotlin.math.roundToInt

class PaymentActivity : BaseActivity<PaymentViewModel, FragmentPaymentBinding, AppRepository>(),
    View.OnClickListener, PaymentResultListener {

    lateinit var viewModel: PaymentViewModel
    lateinit var viewBinding: FragmentPaymentBinding
    private var paymentMethod = ""
    private var paymentArguments: PaymentArguments? = null
    lateinit var dashboardActivity: DashboardActivity

    class PaymentArguments : Serializable {
        var subTotal: String = ""
        var gst: String = ""
        var addressId: Int = 0
        var inputPoints: Int = 0
        var mainTotal: String = ""
        var orderInstruction: String = ""
    }

    override fun onCreate(
        instance: Bundle?,
        viewModel: PaymentViewModel,
        viewBinding: FragmentPaymentBinding
    ) {
        dashboardActivity = DashboardActivity()
        paymentArguments =
            intent.getSerializableExtra(Constants.PAYMENT_PARAMS) as? PaymentArguments
        this.viewBinding = viewBinding
        this.viewModel = viewModel

        viewBinding.productPoints = paymentArguments!!.inputPoints
        viewBinding.productTotal = paymentArguments!!.subTotal
        viewBinding.productGst = paymentArguments!!.gst
        viewBinding.productTotalAmount = paymentArguments!!.mainTotal
        viewBinding.clickListener = this
        managePlaceOrderResult()
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
    }

    override val bindingInflater: (LayoutInflater) -> FragmentPaymentBinding
        get() = FragmentPaymentBinding::inflate

    override val bindingViewModel: Class<PaymentViewModel>
        get() = PaymentViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnMakePayment ->
                if (viewBinding.rbRazorpay.isChecked) {
                    paymentMethod = Constants.PAYMENT_RAZORPAY
                    startPayment()
                } else {
                    paymentMethod = Constants.PAYMENT_COD
                    placeOrder(Constants.PAYMENT_COD, "0", "0", "")
                }
        }
    }

    private fun placeOrder(
        paymentMethod: String,
        paymentStatus: String,
        paymentId: String,
        paymentResponseLog: String
    ) {
        viewModel.placeOrder(
            addressId = paymentArguments!!.addressId,
            paymentMethod = paymentMethod,
            paymentStatus = paymentStatus,
            paymentId = paymentId,
            paymentResponseLog = paymentResponseLog,
            subTotal = viewBinding.tvSubTotal.text.toString().replace(",", "").replace("Rs.", "")
                .toDouble(),
            totalAmount = viewBinding.tvTotalAmount.text.toString().replace(",", "")
                .replace("Rs.", "").toDouble(),
            points = paymentArguments!!.inputPoints,
            orderInstruction = paymentArguments!!.orderInstruction
        )
    }

    private fun managePlaceOrderResult() {
        viewModel.placeOrderResult.observe(this) { response ->
            when (response) {
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(this)
                }
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    if (response.message!!.contains("failed")) {
                        ToastHelper.showMessage(this, response.message)
                        ActivityNav.getInstance()
                            ?.callNewActivity(this, PaymentFailedActivity::class.java)
                    } else {
                        ToastHelper.showMessage(this, response.message)
                        ActivityNav.getInstance()
                            ?.callNewActivity(this, ThankYouActivity::class.java)
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.ConnectionError -> {
//                    Utility.getInstance().showNoInternetDialog(this) {
//                        if (paymentMethod == Constants.PAYMENT_RAZORPAY) {
//                            startPayment()
//                        } else {
//                            placeOrder(Constants.PAYMENT_COD,"0", "0", "")
//                        }
//                    }
                }
            }
        }
    }

    private fun startPayment() {
//      amount convert rupee from paisa in for razorpay
        val amount = viewBinding.tvTotalAmount.text.toString().replace(",", "")
        val totalAmount: Double = amount.replace("Rs.", "").toDouble()
        val finalAmount = (totalAmount * 100).roundToInt()
        val checkout = Checkout()
        checkout.setKeyID(resources.getString(R.string.str_razorpay_api_key))
        checkout.setImage(R.mipmap.ic_launcher)
        val activity: Activity = this
        try {
            val options = JSONObject()
            options.put("name", resources.getString(R.string.app_name))
            options.put("theme.color", "#005778")
            options.put("currency", "INR")
            options.put("amount", finalAmount.toString()) //pass amount in currency subunits
            options.put("prefill.email", LoginHelper.getInstance()?.getEmail().toString())
            options.put("prefill.contact", LoginHelper.getInstance()?.getContactNo1().toString())
            checkout.open(activity, options)
        } catch (e: Exception) {
            viewBinding.rootLayout.errorSnack(e.message!!)
        }
    }

    override fun onPaymentSuccess(razorpayPaymentID: String?) {
        placeOrder(Constants.PAYMENT_RAZORPAY, "1", razorpayPaymentID!!, "Payment Success")
    }

    override fun onPaymentError(code: Int, response: String?) {
        val jsonObject = JSONObject(response!!)
        val errorObject = jsonObject.getJSONObject("error")
        val description = errorObject.getString("description")
        val failedResponse = errorObject.getString("reason")
        if (failedResponse != "payment_cancelled") {
            placeOrder(Constants.PAYMENT_RAZORPAY, "0", "0", description)
        }
    }
}