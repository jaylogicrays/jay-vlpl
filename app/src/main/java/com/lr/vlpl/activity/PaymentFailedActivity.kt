package com.lr.vlpl.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.lr.vlpl.BaseViewBindingActivity
import com.lr.vlpl.R
import com.lr.vlpl.databinding.FragmentPaymentFailedBinding
import com.lr.vlpl.utils.ActivityNav

class PaymentFailedActivity : BaseViewBindingActivity<FragmentPaymentFailedBinding>(),
    View.OnClickListener {

    lateinit var viewBinding: FragmentPaymentFailedBinding

    override fun onCreate(instance: Bundle?, viewBinding: FragmentPaymentFailedBinding) {
        this.viewBinding = viewBinding
        viewBinding.clickListener = this
    }

    override val bindingInflater: (LayoutInflater) -> FragmentPaymentFailedBinding
        get() = FragmentPaymentFailedBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnContinueShopping -> {
                ActivityNav.getInstance()?.callNewActivity(this, DashboardActivity::class.java)
            }
        }
    }

    override fun onBackPressed() {}
}