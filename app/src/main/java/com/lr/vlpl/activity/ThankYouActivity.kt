package com.lr.vlpl.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.lr.vlpl.BaseViewBindingActivity
import com.lr.vlpl.R
import com.lr.vlpl.databinding.FragmentThankYouBinding
import com.lr.vlpl.utils.ActivityNav

class ThankYouActivity : BaseViewBindingActivity<FragmentThankYouBinding>(), View.OnClickListener {

    lateinit var viewBinding: FragmentThankYouBinding

    override fun onCreate(instance: Bundle?, viewBinding: FragmentThankYouBinding) {
        this.viewBinding = viewBinding
        viewBinding.clickListener = this
    }

    override val bindingInflater: (LayoutInflater) -> FragmentThankYouBinding
        get() = FragmentThankYouBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnContinueShopping -> {
                ActivityNav.getInstance()?.callNewActivity(this, DashboardActivity::class.java)
            }
        }
    }

    override fun onBackPressed() {}
}