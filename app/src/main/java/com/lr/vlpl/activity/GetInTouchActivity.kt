package com.lr.vlpl.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseActivity
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.ActivityGetInTouchBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.GetInTouchViewModel

class GetInTouchActivity :
    BaseActivity<GetInTouchViewModel, ActivityGetInTouchBinding, AppRepository>(),
    View.OnClickListener {

    private lateinit var viewModel: GetInTouchViewModel
    private lateinit var viewBinding: ActivityGetInTouchBinding

    override fun onCreate(
        instance: Bundle?,
        viewModel: GetInTouchViewModel,
        viewBinding: ActivityGetInTouchBinding
    ) {
        this.viewModel = viewModel
        this.viewBinding = viewBinding
        viewModel.myBinding = viewBinding
        viewBinding.clickListener = this
        viewBinding.ctbToolbar.ivBackIcon.setOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
        manageGetInTouchAPIResponse()
    }

    override val bindingInflater: (LayoutInflater) -> ActivityGetInTouchBinding
        get() = ActivityGetInTouchBinding::inflate

    override val bindingViewModel: Class<GetInTouchViewModel>
        get() = GetInTouchViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnSend -> {
                viewModel.getInTouchAPI()
            }
            R.id.tvCall -> {
                Utility.getInstance().openDial(this@GetInTouchActivity)
            }
            R.id.tvEmail -> {
                Utility.getInstance().openEmail(this@GetInTouchActivity)
            }
        }
    }

    private fun manageGetInTouchAPIResponse() {
        viewModel.getInTouchResponse.observe(this) { response ->
            when (response) {
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(this) {
                        viewModel.getInTouchAPI()
                    }
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(this)
                }
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    ToastHelper.showMessage(this, response.message.toString())
                    finish()
                }
            }
        }
    }
}