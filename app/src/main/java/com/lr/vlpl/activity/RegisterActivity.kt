package com.lr.vlpl.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.lr.vlpl.BaseActivity
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.databinding.ActivityRegisterBinding
import com.lr.vlpl.fragment.UserAddressFragment
import com.lr.vlpl.fragment.UserDocumentFragment
import com.lr.vlpl.fragment.UserInfoFragment
import com.lr.vlpl.fragment.UserSuccessFragment
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.viewmodel.RegisterViewModel
import com.lr.vlpl.viewpagernav.ViewPagerAdapter
import com.lr.vlpl.viewpagernav.ViewPagerModel

class RegisterActivity :
    BaseActivity<RegisterViewModel, ActivityRegisterBinding, AppRepository>() {

    private lateinit var viewBinding: ActivityRegisterBinding
    lateinit var viewModel: RegisterViewModel
    private lateinit var pageList: ArrayList<ViewPagerModel>
    var selectedUserType = MutableLiveData("")

    companion object {
        const val NEXT = "next"
        const val PREVIOUS = "previous"
    }

    override fun onCreate(
        instance: Bundle?,
        viewModel: RegisterViewModel,
        viewBinding: ActivityRegisterBinding
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        Utility.getInstance().hideKeyBoardWhenTouchOutside(viewBinding.rootLayout)
        addFragment()
        hideShowBackIcon()
        viewModel.fetchStateAndCity()
        viewBinding.ivBack.setOnClickListener { onNextFragmentNavigation(PREVIOUS) }
    }

    override val bindingInflater: (LayoutInflater) -> ActivityRegisterBinding
        get() = ActivityRegisterBinding::inflate

    override val bindingViewModel: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java

    override val repository: AppRepository
        get() {
            return AppRepository(RetrofitBuilder.apiService)
        }

    private fun addFragment() {
        pageList = arrayListOf()
        pageList.add(ViewPagerModel("1", UserInfoFragment()))
        pageList.add(ViewPagerModel("2", UserAddressFragment()))
        pageList.add(ViewPagerModel("3", UserDocumentFragment()))
        pageList.add(ViewPagerModel("4", UserSuccessFragment()))
        val pagerAdapter = ViewPagerAdapter(this, supportFragmentManager, pageList)
        viewBinding.vpRegister.adapter = pagerAdapter
        viewBinding.stepperIndicator.setViewPager(viewBinding.vpRegister, pageList.size - 1)
        viewBinding.vpRegister.offscreenPageLimit = pageList.size - 1
    }

    fun onNextFragmentNavigation(type: String) {
        var currPos: Int = viewBinding.vpRegister.currentItem
        if (NEXT == type) {
            currPos += 1
        } else {
            currPos -= 1
        }
        viewBinding.vpRegister.currentItem = currPos
        hideShowBackIcon()
    }

    private fun hideShowBackIcon() {
        if (viewBinding.vpRegister.currentItem == 0 || viewBinding.vpRegister.currentItem == (pageList.size - 1)) {
            viewBinding.ivBack.visibility = View.INVISIBLE
        } else {
            viewBinding.ivBack.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        val currPos: Int = viewBinding.vpRegister.currentItem
        if (currPos == 0)
            super.onBackPressed()
        if (currPos == 1)
            viewBinding.vpRegister.currentItem = currPos.minus(1)
        else if (currPos == 2)
            viewBinding.vpRegister.currentItem = currPos.minus(1)
    }
}