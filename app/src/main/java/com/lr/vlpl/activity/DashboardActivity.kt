package com.lr.vlpl.activity

import android.Manifest
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.Observable
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import com.lr.vlpl.BaseActivity
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.databinding.ActivityDashboardBinding
import com.lr.vlpl.fragment.CartFragment
import com.lr.vlpl.fragment.HomeFragment
import com.lr.vlpl.fragment.IncentiveFragment
import com.lr.vlpl.fragment.ProfileFragment
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.permission.PermissionCheck
import com.lr.vlpl.permission.PermissionHandler
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.FragmentNav
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.viewmodel.DashboardViewModel
import com.lr.vlpl.viewpagernav.ViewPagerAdapter
import com.lr.vlpl.viewpagernav.ViewPagerModel

class DashboardActivity :
    BaseActivity<DashboardViewModel, ActivityDashboardBinding, AppRepository>() {

    private lateinit var viewBinding: ActivityDashboardBinding
    lateinit var viewModel: DashboardViewModel

    override fun onCreate(
        instance: Bundle?,
        viewModel: DashboardViewModel,
        viewBinding: ActivityDashboardBinding,
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        getProfile()
        setupViews()
        checkPermission()
        setBadgeValue()
    }

    private fun setBadgeValue() {
        BaseApplication.getInstance().totalCartQTY.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                val navBar = findViewById<BottomNavigationView>(R.id.bnvDashboard)
                if (BaseApplication.getInstance().totalCartQTY.get() > 0) {
                    navBar.apply {
                        getOrCreateBadge(R.id.nvCart).isVisible = true
                        getOrCreateBadge(R.id.nvCart).number =
                            BaseApplication.getInstance().totalCartQTY.get()
                        getOrCreateBadge(R.id.nvCart).backgroundColor =
                            resources.getColor(R.color.colorYellow)
                    }
                } else {
                    navBar.getOrCreateBadge(R.id.nvCart).isVisible = false
                }
            }
        })
    }

    override val bindingInflater: (LayoutInflater) -> ActivityDashboardBinding
        get() = ActivityDashboardBinding::inflate

    override val bindingViewModel: Class<DashboardViewModel>
        get() = DashboardViewModel::class.java

    override val repository: AppRepository
        get() = AppRepository(RetrofitBuilder.apiService)


    private fun setupViews() {
        supportActionBar?.hide()
        setSupportActionBar(viewBinding.toolBar)
        setupFragment()
        viewBinding.bnvDashboard.setOnItemSelectedListener(onItemSelectedListener)
    }

    private fun setupFragment() {
        val list: ArrayList<ViewPagerModel> = arrayListOf()
        list.add(ViewPagerModel("", HomeFragment()))
        list.add(ViewPagerModel("", ProfileFragment()))
        list.add(ViewPagerModel("", IncentiveFragment.newInstance()))
        list.add(ViewPagerModel("", CartFragment.newInstance()))
        viewBinding.vpDashboard.adapter = ViewPagerAdapter(this, supportFragmentManager, list)
        viewBinding.vpDashboard.offscreenPageLimit = list.size - 1
    }

    private val onItemSelectedListener = NavigationBarView.OnItemSelectedListener { item ->
        viewBinding.vpDashboard.currentItem = when (item.itemId) {
            R.id.nvHome -> 0
            R.id.nvProfile -> 1
            R.id.nvIncentive -> 2
            R.id.nvCart -> 3
            else -> 0
        }
        true
    }

    private fun addFragment(fragment: Fragment) {
        FragmentNav.getInstance()
            ?.addFragmentBack(R.id.fcvDashboard, fragment, supportFragmentManager, false)
    }

    fun addFragment(fragment: Fragment, isShowBottomView: Boolean = false) {
        viewBinding.vpDashboard.visibility = View.GONE
        viewBinding.fcvDashboard.visibility = View.VISIBLE
        if (supportFragmentManager.backStackEntryCount > 0) {
            val currentFragment = FragmentNav.getInstance()
                ?.getCurrentFragment(R.id.fcvDashboard, supportFragmentManager)
            if (currentFragment != fragment) {
                addFragment(fragment)
            }
        } else {
            addFragment(fragment)
        }
        hideShowBottomNavigationView(isShowBottomView)
    }

    private fun hideShowBottomNavigationView(isShowBottomView: Boolean) {
        viewBinding.bnvDashboard.visibility = if (isShowBottomView) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun getProfile() {
        viewModel.getProfileResponse.observe(this) { response ->
            when (response) {
                is Resource.Success -> {
                    LoginHelper.getInstance()
                        ?.saveLogin(response.data!!)
                    checkNotificationStatus(response.data!!.is_new_push)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(this) {
                        if (BaseApplication.getInstance().isConnectionAvailable())
                            viewModel.getProfile()
                    }
                }
                else -> {}
            }
        }
    }

    override fun onBackPressed() {
        if (FragmentNav.getInstance()?.isRootFragment(supportFragmentManager)!!) {
            val currentPage = viewBinding.vpDashboard.currentItem
            if (currentPage == 0) {
                super.onBackPressed()
            } else {
                viewBinding.bnvDashboard.selectedItemId = R.id.nvHome
            }
        } else {
            FragmentNav.getInstance()?.removeFragment(supportFragmentManager)
            if (supportFragmentManager.backStackEntryCount == 1) {
                viewBinding.vpDashboard.visibility = View.VISIBLE
                viewBinding.fcvDashboard.visibility = View.GONE
                hideShowBottomNavigationView(true)
            }
        }
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= 33) {
            PermissionCheck.check(this,
                arrayListOf(Manifest.permission.POST_NOTIFICATIONS),
                "Notification permission is necessary to get notifications",
                object : PermissionHandler() {
                    override fun onGranted() {

                    }

                    override fun onDenied(
                        context: Context,
                        deniedPermissions: java.util.ArrayList<String>
                    ) {
                    }
                })
        }
    }

    private fun checkNotificationStatus(isNewPush: Int) {
        if (isNewPush == 1) {
            BaseApplication.getInstance().notificationUpdate.set(true)
        } else {
            BaseApplication.getInstance().notificationUpdate.set(false)
        }
    }
}
