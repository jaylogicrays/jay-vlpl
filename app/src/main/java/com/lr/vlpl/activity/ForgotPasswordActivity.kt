package com.lr.vlpl.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.lr.vlpl.BaseActivity
import com.lr.vlpl.R
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.customclasses.CustomDialog
import com.lr.vlpl.databinding.ActivityForgotPasswordBinding
import com.lr.vlpl.helper.ToastHelper
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import com.lr.vlpl.utils.errorSnack
import com.lr.vlpl.viewmodel.ForgotPasswordViewModel

class ForgotPasswordActivity :
    BaseActivity<ForgotPasswordViewModel, ActivityForgotPasswordBinding, AppRepository>(),
    View.OnClickListener {

    lateinit var viewBinding: ActivityForgotPasswordBinding
    lateinit var viewModel: ForgotPasswordViewModel

    override fun onCreate(
        instance: Bundle?,
        viewModel: ForgotPasswordViewModel,
        viewBinding: ActivityForgotPasswordBinding
    ) {
        this.viewBinding = viewBinding
        this.viewModel = viewModel
        viewBinding.clickListener = this
        forgotPasswordResponse()
    }

    override val bindingInflater: (LayoutInflater) -> ActivityForgotPasswordBinding
        get() = ActivityForgotPasswordBinding::inflate

    override val bindingViewModel: Class<ForgotPasswordViewModel>
        get() = ForgotPasswordViewModel::class.java

    override val repository: AppRepository
        get() {
            return AppRepository(RetrofitBuilder.apiService)
        }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBack -> onBackPressedDispatcher.onBackPressed()
            R.id.btnResetPassword -> {
                callForgetPassword()
            }
        }
    }

    private fun callForgetPassword() {
        if (viewBinding.etEmail.isValidate()) {
            viewModel.callForgotPassword(viewBinding.etEmail.et.text.toString())
        }
    }

    private fun forgotPasswordResponse() {
        viewModel.getForgetPassword.observe(this) { response ->
            when (response) {
                is Resource.Success -> {
                    CustomDialog.getInstance().hide()
                    ToastHelper.showMessage(this, response.message.toString())
                    onBackPressedDispatcher.onBackPressed()
                }
                is Resource.Error -> {
                    CustomDialog.getInstance().hide()
                    viewBinding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
                }
                is Resource.Loading -> {
                    CustomDialog.getInstance().showDialog(this)
                }
                is Resource.ConnectionError -> {
                    Utility.getInstance().showNoInternetDialog(this) {
                        callForgetPassword()
                    }
                }
            }
        }
    }
}