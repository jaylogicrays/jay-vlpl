package com.lr.vlpl.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.lr.vlpl.BuildConfig
import com.lr.vlpl.R
import com.lr.vlpl.databinding.ActivityCustomBrowserBinding
import com.lr.vlpl.utils.Constants


class CustomBrowserActivity : AppCompatActivity() {

    lateinit var myBinding: ActivityCustomBrowserBinding
    private var _from: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myBinding = ActivityCustomBrowserBinding.inflate(layoutInflater)
        setContentView(myBinding.root)
        _from = intent.getStringExtra(Constants.URL_KEY)
        myBinding.ctbToolbar.setOnClickListener { onBackPressedDispatcher.onBackPressed() }
        loadURL()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadURL() {
        myBinding.myWebView.settings.javaScriptEnabled = true
        myBinding.myWebView.webViewClient = object : WebViewClient() {
            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }
        }
        if (_from == Constants.PRIVACY_POLICY) {
            myBinding.ctbToolbar.tvTitle.text = resources.getString(R.string.str_privacy_policy)
            myBinding.myWebView.loadUrl(BuildConfig.PRIVACY_POLICY_URL)
        } else {
            myBinding.ctbToolbar.tvTitle.text =
                resources.getString(R.string.str_terms_and_conditions)
            myBinding.myWebView.loadUrl(BuildConfig.TERM_CONDITION_URL)
        }
    }
}