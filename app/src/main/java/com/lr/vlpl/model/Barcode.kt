package com.lr.vlpl.model

data class Barcode(
    val description: String,
    val id: Int,
    val photo: List<String>,
    val redeem_point: Int,
    val title: String
)