package com.lr.vlpl.model

data class SearchProductModel(
    var category_list: List<SearchProduct>,
    var total_cart_qty: Int
)

data class SearchProduct(
    val id: Int,
    val product_info: List<Products>,
    val slug: String,
    val title: String
)