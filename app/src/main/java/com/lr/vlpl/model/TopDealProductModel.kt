package com.lr.vlpl.model

data class TopDealProductModel(
    var top_deals_list: List<Products>,
    var total_cart_qty: Int
)