package com.lr.vlpl.model

data class ProductDetail(
    val product_data: Products,
    val total_cart_qty: Int
)

