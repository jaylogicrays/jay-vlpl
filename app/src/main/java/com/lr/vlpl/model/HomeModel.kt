package com.lr.vlpl.model

data class HomeModel(
    val banners: List<Banner>,
    val blog: List<Articles>,
    val category: List<Category>,
    val product: List<Products>,
    val promotion: Promotion,
    var total_cart_qty: Int
)

data class Category(
    val date: String,
    val photo: String,
    val slug: String,
    val title: String,
    val id: Int,
)

data class Banner(
    val id: Int,
    val photo: String,
    val slug: String,
    val title: String,
//    val product_id : Int
)

data class Promotion(
    val image: String
)