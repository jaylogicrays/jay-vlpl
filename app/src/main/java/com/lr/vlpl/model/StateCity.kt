package com.lr.vlpl.model

data class StateCity(
    val city: List<City>,
    val id: Int,
    val name: String
)

data class City(
    val id: Int,
    val name: String
)