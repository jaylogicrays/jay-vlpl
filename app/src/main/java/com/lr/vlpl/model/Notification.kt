package com.lr.vlpl.model

data class Notification(
    val date: String,
    val items: List<NotificationDetail>
)

data class NotificationDetail(
    val body: String,
    val image: String,
    val product_id: Int,
    val slug: String,
    val title: String,
    val type: Int
)