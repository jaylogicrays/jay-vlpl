package com.lr.vlpl.model

data class UserType(
    val id: Int,
    val name: String,
    val slug: String
)