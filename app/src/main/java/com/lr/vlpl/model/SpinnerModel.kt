package com.lr.vlpl.model

data class SpinnerModel(val id: Int, val name: String)