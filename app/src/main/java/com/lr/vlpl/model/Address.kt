package com.lr.vlpl.model

import java.io.Serializable

data class Address(
    val building_name: String,
    val city: String,
    val state: String,
    val id: Int,
    var is_primary: Int,
    val pincode: Int,
    val street_name: String,
) : Serializable