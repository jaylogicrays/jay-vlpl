package com.lr.vlpl.model

data class OrderData(
    val orders_list: List<Order>,
    val total_cart_qty: Int
)

data class Order(
    var date: String,
    var delivered: String,
    val id: Int,
    val order_number: String,
    val product_name: String,
    var status: String,
    val total_item: Int,
    val product_info: List<Products>,
    val sub_total_amount: String,
    val total_amount: String,
    val gst: String,
    val points: String,
    var invoice_already_uploaded: Int
)