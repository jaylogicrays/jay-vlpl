package com.lr.vlpl.viewpagernav

import androidx.fragment.app.Fragment

data class ViewPagerModel(
    var title: String,
    var fragment: Fragment
)
