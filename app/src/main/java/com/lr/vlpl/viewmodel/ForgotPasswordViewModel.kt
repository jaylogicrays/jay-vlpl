package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ForgotPasswordViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val forgotPasswordResponse: MutableLiveData<Resource<String>> = MutableLiveData()

    fun callForgotPassword(emailId: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                forgotPasswordResponse.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.EMAIL] = emailId
                forgotPasswordResponse.postValue(authRepository.forgotPassword(requestParams))
            } else {
                forgotPasswordResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val getForgetPassword: MutableLiveData<Resource<String>> = forgotPasswordResponse
}
