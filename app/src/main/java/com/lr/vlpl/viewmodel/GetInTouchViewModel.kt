package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.ActivityGetInTouchBinding
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class GetInTouchViewModel(private val authRepository: AppRepository) : ViewModel() {

    var myBinding: ActivityGetInTouchBinding? = null
    private var getInTouchResult: MutableLiveData<Resource<String>> = MutableLiveData()

    fun getInTouchAPI() {
        if (isValidate()) {
            viewModelScope.launch {
                if (BaseApplication.getInstance().isConnectionAvailable()) {
                    getInTouchResult.postValue(Resource.Loading())
                    val requestParams = HashMap<String, Any>()
                    requestParams[RequestParams.NAME] = myBinding!!.etName.et.text.toString()
                    requestParams[RequestParams.EMAIL] = myBinding!!.etEmail.et.text.toString()
                    requestParams[RequestParams.PHONE] =
                        myBinding!!.etPhoneNumber.et.text.toString()
                    requestParams[RequestParams.MESSAGE] = myBinding!!.etMessage.et.text.toString()
                    getInTouchResult.postValue(authRepository.getInTouch(requestParams))
                } else {
                    getInTouchResult.postValue(Resource.ConnectionError())
                }
            }
        }
    }

    var getInTouchResponse: LiveData<Resource<String>> = getInTouchResult

    fun isValidate(): Boolean {
        var isValid = true
        if (!myBinding!!.etName.isValidate())
            isValid = false
        if (!myBinding!!.etEmail.isValidate())
            isValid = false
        if (!myBinding!!.etPhoneNumber.isValidate())
            isValid = false
        if (!myBinding!!.etMessage.isValidate())
            isValid = false
        return isValid
    }
}