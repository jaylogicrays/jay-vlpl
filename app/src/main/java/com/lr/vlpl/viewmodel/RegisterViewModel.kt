package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Address
import com.lr.vlpl.model.StateCity
import com.lr.vlpl.model.UserType
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import java.io.File

class RegisterViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val userTypeList: MutableLiveData<Resource<List<UserType>>> = MutableLiveData()
    private val stateCityList: MutableLiveData<Resource<List<StateCity>>> = MutableLiveData()
    private val registerResult: MutableLiveData<Resource<String>> = MutableLiveData()
    private val getCityStateFromZipcodeResult: MutableLiveData<Resource<Address>> =
        MutableLiveData()

    var fullName = ""
    var firstName = ""
    var lastName = ""
    var firmName = ""
    var userType = ""
    var email = ""
    var password = ""
    var confirmPassword = ""
    var buildingName = ""
    var streetName = ""
    var stateName = ""
    var cityName = ""
    var pincode = ""
    var contactNo1 = ""
    var contactNo2 = ""
    var designation = ""
    var gstNo = ""
    var drugLicenceNo = ""
    var drugLicenceNo2 = ""
    var gstDoc: File? = null
    var drugDoc: File? = null
    var drugDoc2: File? = null
    var fssaiDoc: File? = null
    var idProofDoc: File? = null

    init {
        fetchUserTypes()
    }

    // for  Fetching User type
    fun fetchUserTypes() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                userTypeList.postValue(Resource.Loading())
                userTypeList.postValue(authRepository.getUserTypes())
            } else {
                userTypeList.postValue(Resource.ConnectionError())
            }
        }
    }

    val getUserTypes: MutableLiveData<Resource<List<UserType>>> = userTypeList

    // Fetching state And City
    fun fetchStateAndCity() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                stateCityList.postValue(Resource.Loading())
                stateCityList.postValue(authRepository.getStateCity())
            } else {
                stateCityList.postValue(Resource.ConnectionError())
            }
        }
    }

    val getStateCity: MutableLiveData<Resource<List<StateCity>>> = stateCityList

    fun postRegister() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                registerResult.postValue(Resource.Loading())
                registerResult.postValue(
                    authRepository.register(getRegisterRequestParams().build())
                )
            } else {
                registerResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getRegister: MutableLiveData<Resource<String>> = registerResult

    fun getCityStateFromZipCode(pinCode: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getCityStateFromZipcodeResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PIN_CODE] = pinCode
                getCityStateFromZipcodeResult.postValue(
                    authRepository.getCityStateFromZipCode(
                        pinCode
                    )
                )
            } else {
                getCityStateFromZipcodeResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getCityStateFromZipcodeResponse: LiveData<Resource<Address>> = getCityStateFromZipcodeResult

    private fun getRegisterRequestParams(): MultipartBody.Builder {
        val multipartBody = MultipartBody.Builder()
        multipartBody.addFormDataPart(RequestParams.ROLE_SLUG, userType)
            .addFormDataPart(RequestParams.ROLE_SLUG, userType)
            .addFormDataPart(RequestParams.FIRST_NAME, firstName)
            .addFormDataPart(RequestParams.LAST_NAME, lastName)
            .addFormDataPart(RequestParams.FIRM_NAME, firmName)
            .addFormDataPart(RequestParams.EMAIL, email)
            .addFormDataPart(RequestParams.PASSWORD, password)
            .addFormDataPart(RequestParams.CONFIRM_PASSWORD, confirmPassword)
            .addFormDataPart(RequestParams.BUILDING_NAME, buildingName)
            .addFormDataPart(RequestParams.STREET_NAME, streetName)
            .addFormDataPart(RequestParams.STATE, stateName)
            .addFormDataPart(RequestParams.CITY, cityName)
            .addFormDataPart(RequestParams.PIN_CODE, pincode)
            .addFormDataPart(RequestParams.CONTACT_NO_1, contactNo1)
            .addFormDataPart(RequestParams.CONTACT_NO_2, contactNo2)
            .addFormDataPart(RequestParams.GST_NO, gstNo)
            .addFormDataPart(RequestParams.DRUG_LIC_NO, drugLicenceNo)
            .addFormDataPart(RequestParams.DRUG_LIC_NO_2, drugLicenceNo2)
            .addFormDataPart(RequestParams.DESIGNATION, designation)
            .addPart(
                Utility.getInstance()
                    .getMultipartBody(RequestParams.ID_PROOF_DOC, idProofDoc!!)
            )
        if (userType != "mr") {
            if (gstDoc != null) {
                multipartBody.addPart(
                    Utility.getInstance().getMultipartBody(RequestParams.GST_DOC, gstDoc!!)
                )
            }
            if (fssaiDoc != null) {
                multipartBody.addPart(
                    Utility.getInstance().getMultipartBody(RequestParams.FSSAI_DOC, fssaiDoc!!)
                )
            }
            multipartBody.addPart(
                Utility.getInstance().getMultipartBody(RequestParams.DRUG_DOC, drugDoc!!)
            )
            multipartBody.addPart(
                Utility.getInstance().getMultipartBody(RequestParams.DRUG_DOC_2, drugDoc2!!)
            )
        }
        return multipartBody
    }
}
