package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Products
import com.lr.vlpl.model.UserDetails
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class DashboardViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val getProfileResult: MutableLiveData<Resource<UserDetails>> = MutableLiveData()
    private val addToCartResult: MutableLiveData<Resource<Products>> = MutableLiveData()
    private val getUserIncentiveResponse: MutableLiveData<Resource<UserDetails>> = MutableLiveData()

    init {
        getProfile()
    }

    //fetching Profile details
    fun getProfile() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getProfileResult.postValue(authRepository.getProfile())
            } else {
                getProfileResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getProfileResponse: MutableLiveData<Resource<UserDetails>> = getProfileResult

    // add to cart API
    fun addToCart(product_id: Int, productQTY: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                addToCartResult.postValue(authRepository.addToCart(requestParams))
            } else {
                addToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddToCartResponse: LiveData<Resource<Products>> = addToCartResult

    //get User incentive point API
    fun getUserIncentive() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getUserIncentiveResponse.postValue(Resource.Loading())
                getUserIncentiveResponse.postValue(authRepository.getUserIncentivePoint())
            } else {
                getUserIncentiveResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val getUserIncentivePointResult: LiveData<Resource<UserDetails>> = getUserIncentiveResponse
}