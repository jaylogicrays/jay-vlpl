package com.lr.vlpl.viewmodel

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentChangePasswordBinding
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ChangePasswordViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val changePasswordResponse: MutableLiveData<Resource<String>> = MutableLiveData()
    var viewBinding: FragmentChangePasswordBinding? = null

    fun isValidate(context: Context?): Boolean {
        var isValid = true
        if (!viewBinding!!.etOldPassword.isValidate())
            isValid = false
        if (!viewBinding!!.etNewPassword.isValidate())
            isValid = false
        if (!viewBinding!!.etComformPassword.isValidate()) {
            isValid = false
        } else if (viewBinding!!.etNewPassword.et.text.trim()
                .toString() != viewBinding!!.etComformPassword.et.text.trim().toString()
        ) {
            viewBinding!!.etComformPassword.tvError.text =
                context!!.resources!!.getString(R.string.validation_conform_password)
            viewBinding!!.etComformPassword.tvError.visibility = View.VISIBLE
            isValid = false
        }
        return isValid
    }

    fun changePasswordCall() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                changePasswordResponse.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.OLD_PASSWORD] =
                    viewBinding!!.etOldPassword.et.text.toString()
                requestParams[RequestParams.NEW_PASSWORD] =
                    viewBinding!!.etNewPassword.et.text.toString()
                requestParams[RequestParams.CONFIRM_PASSWORD] =
                    viewBinding!!.etComformPassword.et.text.toString()
                changePasswordResponse.postValue(authRepository.changePassword(requestParams))
            } else {
                changePasswordResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val getChangePassword: MutableLiveData<Resource<String>> = changePasswordResponse
}