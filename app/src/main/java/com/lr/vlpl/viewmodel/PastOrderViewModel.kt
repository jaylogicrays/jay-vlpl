package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.model.Products
import com.lr.vlpl.pagination.BasePagingSource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class PastOrderViewModel(val repository: AppRepository) : ViewModel() {


    private val addToCartResult: MutableLiveData<Resource<Products>> = MutableLiveData()

    val getPastOrderItemList = Pager(PagingConfig(10)) {
        BasePagingSource(endPoint = { page: Int -> RetrofitBuilder.apiService.getPastOrderItem(page) })
    }.flow
        .flowOn(Dispatchers.IO)
        .cachedIn(viewModelScope)

    // add to cart API
    fun addToCart(product_id: Int, productQTY: Int, sequence: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                requestParams[RequestParams.SEQUENCE] = sequence
                addToCartResult.postValue(repository.addToCart(requestParams))
            } else {
                addToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddToCartResponse: LiveData<Resource<Products>> = addToCartResult
}