package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentMyAccountBinding
import com.lr.vlpl.helper.LoginHelper
import com.lr.vlpl.model.StateCity
import com.lr.vlpl.model.UserDetails
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class MyAccountViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val profileResponse: MutableLiveData<Resource<UserDetails>> = MutableLiveData()
    private val stateCityList: MutableLiveData<Resource<List<StateCity>>> = MutableLiveData()
    private val updateProfileResult: MutableLiveData<Resource<String>> = MutableLiveData()
    private val deleteAccountResponse: MutableLiveData<Resource<String>> = MutableLiveData()
    var viewBinding: FragmentMyAccountBinding? = null

    init {
        callAllApi()
    }

    fun callAllApi() {
        getProfile()
        fetchStateAndCity()
    }

    //fetching Profile details
    private fun getProfile() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                profileResponse.postValue(Resource.Loading())
                profileResponse.postValue(authRepository.getProfile())
            } else {
                profileResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val getProfileData: MutableLiveData<Resource<UserDetails>> = profileResponse

    // Fetching state And City
    private fun fetchStateAndCity() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                stateCityList.postValue(Resource.Loading())
                stateCityList.postValue(authRepository.getStateCity())
            } else {
                stateCityList.postValue(Resource.ConnectionError())
            }
        }
    }

    val getStateCity: MutableLiveData<Resource<List<StateCity>>> = stateCityList

    //For delete account
    fun deleteUserAccount() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                deleteAccountResponse.postValue(Resource.Loading())
                deleteAccountResponse.postValue(authRepository.deleteUserAccount())
            } else {
                deleteAccountResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val deleteUserAccountResult: LiveData<Resource<String>> = deleteAccountResponse

    //enable editable EditText
    fun setEditMode(isEditProfile: Boolean) {
        viewBinding!!.etFirstName.setEnable(isEditProfile)
        viewBinding!!.etLastName.setEnable(isEditProfile)
        viewBinding!!.etContactNo2.setEnable(isEditProfile)

        viewBinding!!.etPinCode.setEnable(false)
        viewBinding!!.etCity.setEnable(false)
        viewBinding!!.etState.setEnable(false)

        viewBinding!!.etFirmName.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etEmail.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etUserType.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etCity.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etState.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etPinCode.setBackgroundNonEditableColor(!isEditProfile)
        viewBinding!!.etContactNo1.setBackgroundNonEditableColor(!isEditProfile)
    }

    //update user Profile data
    fun updateProfile() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                updateProfileResult.postValue(Resource.Loading())
                val requireParams = HashMap<String, Any>()
                requireParams[RequestParams.ROLE_SLUG] = viewBinding!!.etUserType.et.text.toString()
                requireParams[RequestParams.FIRST_NAME] =
                    viewBinding!!.etFirstName.et.text.toString()
                requireParams[RequestParams.LAST_NAME] = viewBinding!!.etLastName.et.text.toString()
                requireParams[RequestParams.FIRM_NAME] = viewBinding!!.etFirmName.et.text.toString()
                requireParams[RequestParams.STATE_ID] =
                    viewBinding!!.spState.selectedId.get().toString()
                requireParams[RequestParams.CITY_ID] =
                    viewBinding!!.spCity.selectedId.get().toString()
                requireParams[RequestParams.PIN_CODE] = viewBinding!!.etPinCode.et.text.toString()
                requireParams[RequestParams.CONTACT_NO_1] =
                    viewBinding!!.etContactNo1.et.text.toString()
                requireParams[RequestParams.CONTACT_NO_2] =
                    viewBinding!!.etContactNo2.et.text.toString()
                updateProfileResult.postValue(authRepository.updateProfile(requireParams))
            } else {
                updateProfileResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val updateProfileResponse: MutableLiveData<Resource<String>> = updateProfileResult

    fun isValidate(): Boolean {
        var isValid = true
        if (!viewBinding!!.etUserType.isValidate())
            isValid = false
        if (!viewBinding!!.etFirstName.isValidate())
            isValid = false
        if (!viewBinding!!.etLastName.isValidate())
            isValid = false
        if (!viewBinding!!.etFirmName.isValidate())
            isValid = false
        if (!viewBinding!!.etState.isValidate())
            isValid = false
        if (!viewBinding!!.etCity.isValidate())
            isValid = false
        if (!viewBinding!!.etPinCode.isValidate())
            isValid = false
        if (!viewBinding!!.etContactNo1.isValidate())
            isValid = false
//        if (!viewBinding!!.etContactNo2.isValidate())
//            isValid = false
        return isValid
    }

    fun checkEditOrNot(): Boolean {
        val loginHelper = LoginHelper.getInstance()
        var isEdit = true
        if (loginHelper!!.getFirstName() != viewBinding!!.etFirstName.et.text.toString())
            isEdit = false
        if (loginHelper.getLastName() != viewBinding!!.etLastName.et.text.toString())
            isEdit = false
        if (loginHelper.getFirmName() != viewBinding!!.etFirmName.et.text.toString())
            isEdit = false
        if (loginHelper.getStateName() != viewBinding!!.etState.et.text.toString())
            isEdit = false
        if (loginHelper.getCityName() != viewBinding!!.etCity.et.text.toString())
            isEdit = false
        if (loginHelper.getPinCode() != (viewBinding!!.etPinCode.et.text.toString()))
            isEdit = false
        if (loginHelper.getContactNo1() != viewBinding!!.etContactNo1.et.text.toString())
            isEdit = false
        if (loginHelper.getContactNo2() != viewBinding!!.etContactNo2.et.text.toString())
            isEdit = false
        return isEdit
    }
}