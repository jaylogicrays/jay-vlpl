package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Barcode
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ScannerViewModel(val appRepository: AppRepository) : ViewModel() {
    private val barcodeResponse: MutableLiveData<Resource<Barcode>> = MutableLiveData()

    fun getBarcodeDetails(barcodeNumber: String, latitude: String, longitude: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                barcodeResponse.postValue(Resource.Loading())
                val responseParams = HashMap<String, Any>()
                responseParams[RequestParams.BARCODE] = barcodeNumber
                responseParams[RequestParams.LATITUDE] = latitude
                responseParams[RequestParams.LONGITUDE] = longitude
                barcodeResponse.postValue(appRepository.getBarcodeDetails(responseParams))
            } else {
                barcodeResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val barcodeResult: LiveData<Resource<Barcode>> = barcodeResponse
}