package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.ProductDetail
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ProductDetailsViewModel(private val appRepository: AppRepository) : ViewModel() {

    private val productDetails: MutableLiveData<Resource<ProductDetail>> = MutableLiveData()

    // get Product Details
    fun getProductDetails(slug: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                productDetails.run {
                    postValue(Resource.Loading())
                    postValue(appRepository.getProductDetails(slug))
                }
            } else {
                productDetails.postValue(Resource.ConnectionError())
            }
        }
    }

    val responseProductDetails: LiveData<Resource<ProductDetail>> = productDetails
}