package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class PaymentViewModel(val appRepository: AppRepository) : ViewModel() {
    private val placeOrderResponse: MutableLiveData<Resource<String>> = MutableLiveData()

    fun placeOrder(
        addressId: Int,
        paymentMethod: String,
        paymentStatus: String,
        paymentId: String,
        paymentResponseLog: String,
        subTotal: Double,
        totalAmount: Double,
        points: Int,
        orderInstruction: String
    ) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                placeOrderResponse.postValue(Resource.Loading())
                val responseParams = HashMap<String, Any>()
                responseParams[RequestParams.ADDRESS_ID] = addressId
                responseParams[RequestParams.PAYMENT_METHOD] = paymentMethod
                responseParams[RequestParams.PAYMENT_STATUS] = paymentStatus
                responseParams[RequestParams.PAYMENT_ID] = paymentId
                responseParams[RequestParams.PAYMENT_RESPONSE_LOG] = paymentResponseLog
                responseParams[RequestParams.SUB_TOTAL_AMOUNT] = subTotal
                responseParams[RequestParams.TOTAL_AMOUNT] = totalAmount
                responseParams[RequestParams.POINTS] = points
                responseParams[RequestParams.ORDER_INSTRUCTION] = orderInstruction
                placeOrderResponse.postValue(appRepository.placeOrder(responseParams))
            } else {
                placeOrderResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val placeOrderResult: LiveData<Resource<String>> = placeOrderResponse
}