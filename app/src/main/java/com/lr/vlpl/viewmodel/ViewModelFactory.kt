package com.lr.vlpl.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.repository.BaseRepository

class ViewModelFactory(val repository: BaseRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(RegisterViewModel::class.java) -> modelClass.cast(
                RegisterViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> modelClass.cast(
                LoginViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java) -> modelClass.cast(
                ForgotPasswordViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(MyAccountViewModel::class.java) -> modelClass.cast(
                MyAccountViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(DashboardViewModel::class.java) -> modelClass.cast(
                DashboardViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ChangePasswordViewModel::class.java) -> modelClass.cast(
                ChangePasswordViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(AddAddressViewModel::class.java) -> modelClass.cast(
                AddAddressViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(AddressViewModel::class.java) -> modelClass.cast(
                AddressViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ArticleDetailViewModel::class.java) -> modelClass.cast(
                ArticleDetailViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ScannerViewModel::class.java) -> modelClass.cast(
                ScannerViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(CartViewModel::class.java) -> modelClass.cast(
                CartViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> modelClass.cast(
                HomeViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ProfileViewModel::class.java) -> modelClass.cast(
                ProfileViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(NotificationViewModel::class.java) -> modelClass.cast(
                NotificationViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ArticleViewModel::class.java) -> modelClass.cast(
                ArticleViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ProductListViewModel::class.java) -> modelClass.cast(
                ProductListViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(ProductDetailsViewModel::class.java) -> modelClass.cast(
                ProductDetailsViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(SearchProductViewModel::class.java) -> modelClass.cast(
                SearchProductViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(SearchProductViewModel::class.java) -> modelClass.cast(
                SearchProductViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(OrderViewModel::class.java) -> modelClass.cast(
                OrderViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(OrderDetailViewModel::class.java) -> modelClass.cast(
                OrderDetailViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(CategoryViewModel::class.java) -> modelClass.cast(
                CategoryViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(PastOrderViewModel::class.java) -> modelClass.cast(
                PastOrderViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(PaymentViewModel::class.java) -> modelClass.cast(
                PaymentViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(GetInTouchViewModel::class.java) -> modelClass.cast(
                GetInTouchViewModel(repository as AppRepository)
            ) as T
            modelClass.isAssignableFrom(TopDealProductViewModel::class.java) -> modelClass.cast(
                TopDealProductViewModel(repository as AppRepository)
            ) as T
            else -> throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}