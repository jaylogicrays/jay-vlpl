package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Articles
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ArticleDetailViewModel(private val appRepository: AppRepository) : ViewModel() {

    private val articleDetailResult: MutableLiveData<Resource<Articles>> = MutableLiveData()

    fun getArticleDetail(slug: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                articleDetailResult.postValue(Resource.Loading())
                articleDetailResult.postValue(appRepository.getArticleDetail(slug))
            } else {
                articleDetailResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val articleDetailResponse: MutableLiveData<Resource<Articles>> = articleDetailResult
}