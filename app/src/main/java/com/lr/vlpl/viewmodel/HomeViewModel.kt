package com.lr.vlpl.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentHomeBinding
import com.lr.vlpl.model.HomeModel
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class HomeViewModel(private val appRepository: AppRepository) : ViewModel() {

    var viewBinding: FragmentHomeBinding? = null
    private val getHomeProduct: MutableLiveData<Resource<HomeModel>> = MutableLiveData()
    private val getConnectUs: MutableLiveData<Resource<String>> = MutableLiveData()

    init {
        getDashBoardProductList()
    }

    fun getDashBoardProductList() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getHomeProduct.postValue(Resource.Loading())
                getHomeProduct.postValue(appRepository.getHomeProducts())
            } else {
                getHomeProduct.postValue(Resource.ConnectionError())
            }
        }
    }

    val homeProductListResponse: LiveData<Resource<HomeModel>> = getHomeProduct

    fun connectUs(params: HashMap<String, Any>) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getConnectUs.postValue(Resource.Loading())
                getConnectUs.postValue(appRepository.connectUs(params))
            } else {
                getConnectUs.postValue(Resource.ConnectionError())
            }
        }
    }

    val connectUsResponse: LiveData<Resource<String>> = getConnectUs
}