package com.lr.vlpl.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.pagination.BasePagingSource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn

class ArticleViewModel(val repository: AppRepository) : ViewModel() {

    val getArticleList = Pager(PagingConfig(10)) {
        BasePagingSource(endPoint = { page: Int -> RetrofitBuilder.apiService.getArticlesList(page) })
    }.flow
        .flowOn(Dispatchers.IO)
        .cachedIn(viewModelScope)
}
