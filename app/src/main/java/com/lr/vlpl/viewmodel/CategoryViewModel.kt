package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Category
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class CategoryViewModel(private val appRepository: AppRepository) : ViewModel() {

    private val categoryListResponse: MutableLiveData<Resource<List<Category>>> = MutableLiveData()

    init {
        getCategory()
    }

    fun getCategory() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                categoryListResponse.postValue(Resource.Loading())
                categoryListResponse.postValue(appRepository.getCategory())
            } else {
                categoryListResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val categoryResult: MutableLiveData<Resource<List<Category>>> = categoryListResponse
}