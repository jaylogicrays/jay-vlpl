package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Order
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch


class OrderDetailViewModel(private val appRepository: AppRepository) : ViewModel() {

    private val orderDetailsResponse: MutableLiveData<Resource<Order>> = MutableLiveData()
    private val addReviewResponse: MutableLiveData<Resource<String>> = MutableLiveData()

    fun getOrderDetails(orderId: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                orderDetailsResponse.postValue(Resource.Loading())
                orderDetailsResponse.postValue(appRepository.orderDetails(orderId))
            } else {
                orderDetailsResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val orderDetailsResult: MutableLiveData<Resource<Order>> = orderDetailsResponse

    fun addReview(product_id: Int, rate: Int, review: String) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addReviewResponse.postValue(Resource.Loading())
                val responseParams = HashMap<String, Any>()
                responseParams[RequestParams.PRODUCT_ID] = product_id
                responseParams[RequestParams.RATE] = rate
                responseParams[RequestParams.REVIEW] = review
                addReviewResponse.postValue(appRepository.addReview(responseParams))
            } else {
                addReviewResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val addReviewResult: LiveData<Resource<String>> = addReviewResponse
}