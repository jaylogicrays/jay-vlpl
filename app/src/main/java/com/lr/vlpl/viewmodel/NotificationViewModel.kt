package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Notification
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class NotificationViewModel(val appRepository: AppRepository) : ViewModel() {

    private val notificationResult: MutableLiveData<Resource<List<Notification>>> =
        MutableLiveData()

    init {
        getNotification()
    }

    fun getNotification() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                notificationResult.postValue(Resource.Loading())
                notificationResult.postValue(appRepository.getNotification())
            } else {
                notificationResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val notificationResponse: MutableLiveData<Resource<List<Notification>>> = notificationResult
}