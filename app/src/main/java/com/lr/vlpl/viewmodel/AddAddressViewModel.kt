package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentAddAddressBinding
import com.lr.vlpl.model.Address
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class AddAddressViewModel(private val addressRepository: AppRepository) : ViewModel() {

    var viewBinding: FragmentAddAddressBinding? = null
    private var updateAddressResult: MutableLiveData<Resource<String>> = MutableLiveData()
    private var addressId = 0

    fun isValidate(): Boolean {
        var isValid = true
        if (!viewBinding!!.etBuilding.isValidate())
            isValid = false
        if (!viewBinding!!.etStreetName.isValidate())
            isValid = false
        if (!viewBinding!!.etPinCode.isValidate())
            isValid = false
        if (!viewBinding!!.etCity.isValidate())
            isValid = false

        return isValid
    }

    fun checkEditOrNot(addressDetail: Address?): Boolean {
        var isEdit = true
        if (addressDetail!!.building_name != viewBinding!!.etBuilding.et.text.toString())
            isEdit = false
        if (addressDetail.street_name != viewBinding!!.etStreetName.et.text.toString())
            isEdit = false
        if (addressDetail.pincode.toString() != viewBinding!!.etPinCode.et.text.toString())
            isEdit = false
        if (addressDetail.city != viewBinding!!.etCity.et.text.toString())
            isEdit = false
        return isEdit
    }

    fun setValue(addressDetail: Address?) {
        viewBinding!!.etBuilding.et.setText(addressDetail!!.building_name)
        viewBinding!!.etStreetName.et.setText(addressDetail.street_name)
        viewBinding!!.etCity.et.setText(addressDetail.city)
        viewBinding!!.etPinCode.et.setText(addressDetail.pincode.toString())
        addressId = addressDetail.id
    }

    fun updateAddress() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                updateAddressResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.BUILDING_NAME] =
                    viewBinding!!.etBuilding.et.text.toString()
                requestParams[RequestParams.STREET_NAME] =
                    viewBinding!!.etStreetName.et.text.toString()
                requestParams[RequestParams.PIN_CODE] = viewBinding!!.etPinCode.et.text.toString()
                requestParams[RequestParams.CITY] = viewBinding!!.etCity.et.text.toString()
                requestParams[RequestParams.ADDRESS_ID] = addressId.toString()
                updateAddressResult.postValue(addressRepository.updateAddress(requestParams))
            } else {
                updateAddressResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val updateAddressResponse: MutableLiveData<Resource<String>> = updateAddressResult
}