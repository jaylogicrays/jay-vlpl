package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class ProfileViewModel(private val authRepository: AppRepository) : ViewModel() {

    private val logoutResponse: MutableLiveData<Resource<Any>> = MutableLiveData()
    private val deleteAccountResponse: MutableLiveData<Resource<String>> = MutableLiveData()

    fun logout() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                logoutResponse.postValue(Resource.Loading())
                logoutResponse.postValue(authRepository.logout())
            } else {
                logoutResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val logoutResult: MutableLiveData<Resource<Any>> = logoutResponse
}