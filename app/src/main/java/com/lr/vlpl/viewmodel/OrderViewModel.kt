package com.lr.vlpl.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.OrderData
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import java.io.File

class OrderViewModel(private val appRepository: AppRepository) : ViewModel() {

    private val orderListResponse: MutableLiveData<Resource<OrderData>> = MutableLiveData()
    private val reOrderResponse: MutableLiveData<Resource<OrderData>> = MutableLiveData()
    private val uploadInvoiceResponse: MutableLiveData<Resource<String>> = MutableLiveData()
    var idInvoiceDoc: File? = null
    var orderId: Int? = 0

    init {
        getOrderList()
    }

    fun getOrderList() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                orderListResponse.postValue(Resource.Loading())
                orderListResponse.postValue(appRepository.orderList())
            } else {
                orderListResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val orderListResult: MutableLiveData<Resource<OrderData>> = orderListResponse

    fun reOrder() {
        val requestParams = HashMap<String, Any>()
        requestParams[RequestParams.ORDER_ID] = orderId!!
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                reOrderResponse.postValue(Resource.Loading())
                reOrderResponse.postValue(appRepository.reOrder(requestParams))
            } else {
                reOrderResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val reOrderGetResponse: MutableLiveData<Resource<OrderData>> = reOrderResponse

    fun uploadInvoice() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                Log.e("Multipart", "uploadInvoice: ${getUploadInvoiceRequestParams().build()}")
                uploadInvoiceResponse.postValue(Resource.Loading())
                uploadInvoiceResponse.postValue(
                    appRepository.uploadInvoice(getUploadInvoiceRequestParams().build())
                )
            } else {
                uploadInvoiceResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    private fun getUploadInvoiceRequestParams(): MultipartBody.Builder {
        val multipartBody = MultipartBody.Builder()
        if (idInvoiceDoc != null) {
            multipartBody.addFormDataPart(RequestParams.ORDER_ID, orderId.toString())
                .addPart(Utility.getInstance().getMultipartBody(RequestParams.FILE, idInvoiceDoc!!))
        }
        return multipartBody
    }

    val uploadInvoiceGetResponse: MutableLiveData<Resource<String>> = uploadInvoiceResponse
}