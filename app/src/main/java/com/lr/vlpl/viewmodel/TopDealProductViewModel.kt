package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Products
import com.lr.vlpl.model.TopDealProductModel
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class TopDealProductViewModel(private val productRepository: AppRepository) : ViewModel() {

    private val topDealCategoryProductResponse: MutableLiveData<Resource<TopDealProductModel>> =
        MutableLiveData()
    private val addToCartResult: MutableLiveData<Resource<Products>> = MutableLiveData()

    fun topDealCategoryProduct(queries: HashMap<String, String>) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                topDealCategoryProductResponse.postValue(Resource.Loading())
                topDealCategoryProductResponse.postValue(productRepository.topDealProduct(queries))
            } else {
                topDealCategoryProductResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val topDealCategoryProductResult: MutableLiveData<Resource<TopDealProductModel>> =
        topDealCategoryProductResponse

    // add to cart API
    fun addToCart(product_id: Int, productQTY: Int, sequence: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                requestParams[RequestParams.SEQUENCE] = sequence
                addToCartResult.postValue(productRepository.addToCart(requestParams))
            } else {
                addToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddToCartResponse: LiveData<Resource<Products>> = addToCartResult
}