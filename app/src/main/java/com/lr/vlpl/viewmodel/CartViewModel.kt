package com.lr.vlpl.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.adapter.CartItemAdapter
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.databinding.FragmentCartBinding
import com.lr.vlpl.databinding.ItemAddCartProductBinding
import com.lr.vlpl.model.CartProduct
import com.lr.vlpl.model.CartProductModel
import com.lr.vlpl.model.Products
import com.lr.vlpl.repository.AppRepository
import com.lr.vlpl.utils.Utility
import kotlinx.coroutines.launch
import kotlin.collections.set

class CartViewModel(private val addressRepository: AppRepository) : ViewModel() {

    private var cartItemList = arrayListOf<CartProduct>()
    var viewBinding: FragmentCartBinding? = null

    @SuppressLint("StaticFieldLeak")
    var context: Context? = null
    var listSize: MutableLiveData<Int> = MutableLiveData(0)
    var selectedPoints: MutableLiveData<Int> = MutableLiveData(0)
    private val deleteCartProduct: MutableLiveData<Resource<String>> = MutableLiveData()
    private val getCartProduct: MutableLiveData<Resource<CartProductModel>> = MutableLiveData()
    private val updateCartProduct: MutableLiveData<Resource<CartProductModel>> = MutableLiveData()
    private val updateTopDealProduct: MutableLiveData<Resource<CartProductModel>> =
        MutableLiveData()
    private val getAmountPoints: MutableLiveData<Resource<Products>> = MutableLiveData()
    private val addToCartResult: MutableLiveData<Resource<Products>> = MutableLiveData()
    private val topDealProductAddToCartResult: MutableLiveData<Resource<Products>> =
        MutableLiveData()
    lateinit var adapter: CartItemAdapter
    var myProducts: Products? = null
    var adapterPosition: Int? = 0
    private var cartId: Int? = 0
    var sequence = 0
    var myItemAddCartProductBinding: ItemAddCartProductBinding? = null
    private lateinit var _cartProduct: List<CartProduct>

    fun addCartProduct(context: Context) {
        this.context = context
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getCartProduct.postValue(Resource.Loading())
                getCartProduct.postValue(addressRepository.getCartProduct())
            } else {
                getCartProduct.postValue(Resource.ConnectionError())
            }
        }
    }

    val getCartProductResponse: LiveData<Resource<CartProductModel>> = getCartProduct

    // add to cart API
    fun addToCart(product_id: Int, productQTY: Int, sequence: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                requestParams[RequestParams.SEQUENCE] = sequence
                addToCartResult.postValue(addressRepository.addToCart(requestParams))
            } else {
                addToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddToCartResponse: LiveData<Resource<Products>> = addToCartResult

    // add to cart API for top deal product
    fun topDealProductAddToCart(product_id: Int, productQTY: Int, sequence: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                topDealProductAddToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                requestParams[RequestParams.SEQUENCE] = sequence
                topDealProductAddToCartResult.postValue(addressRepository.addToCart(requestParams))
            } else {
                topDealProductAddToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getTopDealProductAddToCartResponse: LiveData<Resource<Products>> =
        topDealProductAddToCartResult

    // Api call for after add or remove top deal product from cart update amount and gst date
    fun updateTopDealProductProduct(context: Context) {
        this.context = context
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                updateTopDealProduct.postValue(Resource.Loading())
                updateTopDealProduct.postValue(addressRepository.getCartProduct())
            } else {
                updateTopDealProduct.postValue(Resource.ConnectionError())
            }
        }
    }

    val updateTopDealProductResponse: LiveData<Resource<CartProductModel>> = updateTopDealProduct

    // Api call for after add or remove cart product from cart update amount and gst date
    fun updateCartProduct(context: Context) {
        this.context = context
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                updateCartProduct.postValue(Resource.Loading())
                updateCartProduct.postValue(addressRepository.getCartProduct())
            } else {
                updateCartProduct.postValue(Resource.ConnectionError())
            }
        }
    }

    val updateCartProductResponse: LiveData<Resource<CartProductModel>> = updateCartProduct

    fun applyPoints(points: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                getAmountPoints.postValue(Resource.Loading())
                val responseParams = HashMap<String, Any>()
                responseParams[RequestParams.POINTS] = points
                getAmountPoints.postValue(addressRepository.getApplyPoint(responseParams))
            } else {
                getAmountPoints.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAmountPointResult: LiveData<Resource<Products>> = getAmountPoints

    fun deleteProduct(cartID: Int, productId: Int) {
        Utility.getInstance().showConfirmationDialog(
            context!!,
            context!!.getString(R.string.str_delete_product_header),
            context!!.resources.getString(R.string.str_delete_dialog_desc)
        ) {
            cartId = cartID
            BaseApplication.getInstance().productId.set(productId)
            deleteProductFromCart(cartID = cartID)
        }
    }

    fun deleteProductFromCart(cartID: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                deleteCartProduct.postValue(Resource.Loading())
                deleteCartProduct.postValue(addressRepository.deleteProductFromCart(cartID))
            } else {
                deleteCartProduct.postValue(Resource.ConnectionError())
            }
        }
    }

    val deleteCartItemResponse: LiveData<Resource<String>> = deleteCartProduct

    // Function for item remove from cart and remove from list
    fun itemDeleteFromCart() {
        addCartProduct(context!!)
    }

    // Function from set cart adapter
    fun setCartAdapter(data: List<CartProduct>) {
        cartItemList.clear()
        cartItemList.addAll(data)
        _cartProduct =
            data                                               // Cart product data store in array
        if (cartItemList.size > 0) {
            viewBinding?.tvNoCartItemAvailable!!.visibility = View.GONE
            viewBinding?.clCartView!!.visibility = View.VISIBLE
            adapter = CartItemAdapter(data, this)
            viewBinding?.rvCartItems?.adapter = adapter
            listSize.value = cartItemList.size

            adapter.onIncreaseQTY = { products, itemAddCartProductBinding, position ->
                cartId = data[position].id
                BaseApplication.getInstance().from = "add"
                myProducts = products
                myItemAddCartProductBinding = itemAddCartProductBinding
                adapterPosition = position

                if (myProducts!!.discount_type == 3) {
                    sequence = myProducts!!.sequence + 1
                }
                addToCart(products.id, products.ratio, sequence)
            }
            adapter.onDecreaseQTY = { products, itemAddCartProductBinding, position ->
                cartId = data[position].id
                BaseApplication.getInstance().from = "remove"
                myProducts = products
                myItemAddCartProductBinding = itemAddCartProductBinding
                adapterPosition = position
                if (myProducts!!.discount_type == 3) {
                    sequence = myProducts!!.sequence - 1
                }
                addToCart(products.id, (products.ratio - 1).inv(), sequence)
            }
        } else {
            viewBinding?.tvNoCartItemAvailable!!.visibility = View.VISIBLE
            viewBinding?.clCartView!!.visibility = View.GONE
        }
    }

    fun onIncreaseQTYCalculation(
        myProducts: Products,
        adapterPosition: Int,
        myItemAddCartProductBinding: ItemAddCartProductBinding
    ) {
        adapter.onIncreaseQTYCalculation(myProducts, adapterPosition, myItemAddCartProductBinding)
    }

    fun onDecreaseQTYCalculation(
        myProducts: Products,
        adapterPosition: Int,
        myItemAddCartProductBinding: ItemAddCartProductBinding
    ) {
        adapter.onDecreaseQTYCalculation(myProducts, adapterPosition, myItemAddCartProductBinding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun checkProductAvailableInCart() {
        var isProductFound = false
        _cartProduct.forEach { categoryList ->
            if (categoryList.product.id == BaseApplication.getInstance().productId.get()) {
                if (categoryList.product.discount_type == 3) {
                    when (BaseApplication.getInstance().from) {
                        "add" -> {
                            categoryList.product.cart_qty += categoryList.product.ratio
                            categoryList.product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            categoryList.product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            categoryList.product.sequence =
                                BaseApplication.getInstance().sequence.get()
                        }

                        "delete" -> {
                            categoryList.product.cart_qty = 0
                            categoryList.product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            categoryList.product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            categoryList.product.sequence =
                                BaseApplication.getInstance().sequence.get()
                        }

                        else -> {
                            categoryList.product.cart_qty -= categoryList.product.ratio
                            categoryList.product.cart_free_qty =
                                BaseApplication.getInstance().productFreeQTY.get()
                            categoryList.product.total_cart_qty =
                                BaseApplication.getInstance().productTotalQTY.get()
                            categoryList.product.sequence =
                                BaseApplication.getInstance().sequence.get()
                        }
                    }
                } else {
                    categoryList.product.cart_qty =
                        BaseApplication.getInstance().productBillQTY.get()
                }
                adapter.notifyDataSetChanged()
                isProductFound = true
                return
            } else {
                isProductFound = false
            }
        }
        if (!isProductFound) {
            addCartProduct(context!!)
        }
    }
}
