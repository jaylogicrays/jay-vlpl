package com.lr.vlpl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.RequestParams
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Products
import com.lr.vlpl.model.SearchProductModel
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class SearchProductViewModel(private val productRepository: AppRepository) : ViewModel() {

    private val searchCategoryProductResponse: MutableLiveData<Resource<SearchProductModel>> =
        MutableLiveData()
    private val addToCartResult: MutableLiveData<Resource<Products>> = MutableLiveData()

    fun searchCategoryProduct(queries: HashMap<String, String>) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                searchCategoryProductResponse.postValue(Resource.Loading())
                searchCategoryProductResponse.postValue(
                    productRepository.searchCategoryWiseProduct(
                        queries
                    )
                )
            } else {
                searchCategoryProductResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val searchCategoryProductResult: MutableLiveData<Resource<SearchProductModel>> =
        searchCategoryProductResponse

    // add to cart API
    fun addToCart(product_id: Int, productQTY: Int, sequence: Int) {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addToCartResult.postValue(Resource.Loading())
                val requestParams = HashMap<String, Any>()
                requestParams[RequestParams.PRODUCT_ID] = product_id
                requestParams[RequestParams.QUANTITY] = productQTY.toString()
                requestParams[RequestParams.SEQUENCE] = sequence
                addToCartResult.postValue(productRepository.addToCart(requestParams))
            } else {
                addToCartResult.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddToCartResponse: LiveData<Resource<Products>> = addToCartResult
}