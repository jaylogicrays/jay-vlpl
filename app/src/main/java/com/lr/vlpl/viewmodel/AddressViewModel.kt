package com.lr.vlpl.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.api.Resource
import com.lr.vlpl.model.Address
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.launch

class AddressViewModel(private val addressRepository: AppRepository) : ViewModel() {

    private val addressResponse: MutableLiveData<Resource<List<Address>>> = MutableLiveData()

    init {
        getAddress()
    }

    //for get All Address from API
    fun getAddress() {
        viewModelScope.launch {
            if (BaseApplication.getInstance().isConnectionAvailable()) {
                addressResponse.postValue(Resource.Loading())
                addressResponse.postValue(addressRepository.getAddress())
            } else {
                addressResponse.postValue(Resource.ConnectionError())
            }
        }
    }

    val getAddressResult: MutableLiveData<Resource<List<Address>>> = addressResponse
}