package com.lr.vlpl.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.lr.vlpl.api.RetrofitBuilder
import com.lr.vlpl.pagination.BasePagingSource
import com.lr.vlpl.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn

class ProductListViewModel(private val repository: AppRepository) : ViewModel() {

    fun getProductDataList(queries: HashMap<String, String>) = Pager(PagingConfig(6)) {
        BasePagingSource(endPoint = { page: Int ->
            RetrofitBuilder.apiService.getProductList(
                page,
                queries
            )
        })
    }.flow
        .flowOn(Dispatchers.IO)
        .cachedIn(viewModelScope)
}