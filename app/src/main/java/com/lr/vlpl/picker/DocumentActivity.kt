package com.lr.vlpl.picker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import com.lr.vlpl.permission.PermissionCheck
import com.lr.vlpl.permission.PermissionHandler
import com.lr.vlpl.utils.Utility

class DocumentActivity : Activity() {
    companion object {
        internal var handler: PickerResultHandler? = null
        const val REQUEST_CODE_PICK_DOCUMENT = 1004
    }

    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        openGallery()
    }

    private fun openGallery() {
        PermissionCheck.check(
            context,
            Utility.getInstance().getPermissionForSDK33(),
            "",
            object : PermissionHandler() {
                override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                    handler!!.onFailure("Permission Denied")
                    super.onPermissionDenied(context, deniedPermissions)
                }

                override fun onGranted() {
                    openDocumentPicker()
                }
            })
    }

    private fun openDocumentPicker() {
        val intent: Intent
        if (Build.MANUFACTURER.equals("samsung", ignoreCase = true)) {
            intent = Intent("com.sec.android.app.myfiles.PICK_DATA")
            intent.putExtra("CONTENT_TYPE", "*/*")
            intent.addCategory(Intent.CATEGORY_DEFAULT)
        } else {
            val mimeTypes = arrayOf(
                "application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // .doc & .docx
                "application/vnd.ms-powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",  // .ppt & .pptx
                "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  // .xls & .xlsx
                "text/plain",
                "application/pdf",
                "application/zip",
                "application/vnd.android.package-archive"
            )
            intent = Intent(Intent.ACTION_GET_CONTENT) // or ACTION_OPEN_DOCUMENT
            intent.type = "*/*"
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        }
        startActivityForResult(intent, REQUEST_CODE_PICK_DOCUMENT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == REQUEST_CODE_PICK_DOCUMENT && resultCode == RESULT_OK && data != null && data.data != null) {
                handleGalleryResult(data)
            } else if (resultCode == RESULT_CANCELED) {
                finish()
            }
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleGalleryResult(data: Intent?) {
        try {
            val uri = data!!.data
            getImage(uri!!)
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
    }

    private fun getImage(uri: Uri) {
        val actualFile = MediaUtility.from(this, uri)
        val actualFileName = MediaUtility.getFileNameFromUri(this, uri)
        handler!!.onSuccess(null, actualFile, actualFileName)
        finish()
    }
}
