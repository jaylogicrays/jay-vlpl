package com.lr.vlpl.picker

import android.content.Context
import android.content.Intent

class MediaPicker {
    companion object {

        fun getImageFromMedia(
            context: Context,
            isFromGallery: Boolean,
            isFromCamera: Boolean,
            isAttachFile: Boolean,
            isCropping: Boolean,
            handler: PickerResultHandler
        ) {

            ImageActivity.handler = handler
            val intent = Intent(context, ImageActivity::class.java)
                .putExtra(ImageActivity.EXTRA_GALLERY, isFromGallery)
                .putExtra(ImageActivity.EXTRA_CAMERA, isFromCamera)
                .putExtra(ImageActivity.EXTRA_FILE, isAttachFile)
                .putExtra(ImageActivity.EXTRA_CROPPING, isCropping)
                .putExtra(ImageActivity.X_ASPECT_RATIO, 1)
                .putExtra(ImageActivity.Y_ASPECT_RATIO, 1)
            context.startActivity(intent)
        }

        fun getImageFromMedia(
            context: Context,
            isFromGallery: Boolean,
            isFromCamera: Boolean,
            isAttachFile: Boolean,
            isCropping: Boolean,
            xAspectRation: Int,
            yAspectRation: Int,
            handler: PickerResultHandler
        ) {
            ImageActivity.handler = handler
            val intent = Intent(context, ImageActivity::class.java)
                .putExtra(ImageActivity.EXTRA_GALLERY, isFromGallery)
                .putExtra(ImageActivity.EXTRA_CAMERA, isFromCamera)
                .putExtra(ImageActivity.EXTRA_FILE, isAttachFile)
                .putExtra(ImageActivity.EXTRA_CROPPING, isCropping)
                .putExtra(ImageActivity.X_ASPECT_RATIO, xAspectRation)
                .putExtra(ImageActivity.Y_ASPECT_RATIO, yAspectRation)
            context.startActivity(intent)
        }
    }
}