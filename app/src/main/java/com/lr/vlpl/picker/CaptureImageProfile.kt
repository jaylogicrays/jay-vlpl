package com.lr.vlpl.picker

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.customclasses.CustomDialog
import com.theartofdev.edmodo.cropper.CropImageView
import id.zelory.compressor.Compressor
import java.io.ByteArrayOutputStream

class CaptureImageProfile : AppCompatActivity() {

    private lateinit var uri: String
    private lateinit var bitmap: Bitmap
    private var degree = 90
    private var xAspectRatio: Int = 1
    private var yAspectRatio: Int = 1

    companion object {
        internal const val X_ASPECT_RATIO = "x_aspect_ratio"
        internal const val Y_ASPECT_RATIO = "y_aspect_ratio"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture_image_profile)
        val ivRotate = findViewById<ImageView>(R.id.ivRotate)
        val ivBack = findViewById<ImageView>(R.id.ivBack)
        val ivCrop = findViewById<ImageView>(R.id.ivCrop)
        val cropImageView = findViewById<CropImageView>(R.id.img_capture)
        getExtras()
        compress()
        cropImageView.setImageBitmap(bitmap)
        cropImageView.setAspectRatio(xAspectRatio, yAspectRatio)
        ivCrop.setOnClickListener {
            try {
                val cropped = cropImageView.croppedImage
                uploadProfile(cropped)
            } catch (e: Exception) {
                e.message
            }
        }
        ivRotate.setOnClickListener { cropImageView.rotateImage(degree) }
        ivBack.setOnClickListener {
            val data = Intent()
            setResult(Activity.RESULT_CANCELED, data)
            finish()
        }
    }

    private fun getExtras() {
        val intent = intent
        uri = intent.getStringExtra("imageBit").toString()
        xAspectRatio = intent.getIntExtra(ImageActivity.X_ASPECT_RATIO, 1)
        yAspectRatio = intent.getIntExtra(ImageActivity.Y_ASPECT_RATIO, 1)
    }


    private fun compress() {
        try {
            val actualImage = MediaUtility.from(this, Uri.parse(uri))
            bitmap = Compressor(this).compressToBitmap(actualImage)
        } catch (ex: Exception) {
        }
    }

    private fun uploadProfile(bitmapCropped: Bitmap) {
        CustomDialog.getInstance().showDialog(this@CaptureImageProfile)
        Thread {
            val stream = ByteArrayOutputStream()
            bitmapCropped.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            BaseApplication.byteArray = stream.toByteArray()
            runOnUiThread {
                CustomDialog.getInstance().hide()
                val data = Intent()
                setResult(Activity.RESULT_OK, data)
                finish()
            }
        }.start()
    }
}
