package com.lr.vlpl.utils

object Constants {
    const val URL_KEY = "urlKey"
    const val PRIVACY_POLICY = "privacy_policy"
    const val TERMS_CONDITION = "term_condition"
    const val PAYMENT_PARAMS = "payment_params"
    const val PAYMENT_COD = "cod"
    const val PAYMENT_RAZORPAY = "razorpay"
}