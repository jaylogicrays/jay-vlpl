package com.lr.vlpl.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.Html
import android.text.Spanned
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.text.HtmlCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.R
import com.lr.vlpl.databinding.DialogViewOffersBinding
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.time.LocalDate
import java.time.format.DateTimeFormatter

//This class is a collection of common methods which are use in daily routine
class Utility {

    companion object {
        private var instance: Utility? = null

        fun getInstance(): Utility {
            if (instance == null) {
                instance = Utility()
            }
            return instance as Utility
        }
    }

    @SuppressLint("HardwareIds")
    //get device token for different devices
    fun getDeviceToken(context: Context?): String {
        return Settings.Secure.getString(context!!.contentResolver, Settings.Secure.ANDROID_ID)
    }

    //this method will return 32 characters authenticate value while api calling
    fun getMD5EncryptedString(inputString: String): String {
        var mdEnc: MessageDigest? = null
        try {
            mdEnc = MessageDigest.getInstance("MD5")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        mdEnc!!.update(inputString.toByteArray(), 0, inputString.length)
        var md5 = BigInteger(1, mdEnc.digest()).toString(16)
        while (md5.length < 32) {
            md5 = "0$md5"
        }
        return md5
    }

    /**
     * The key hash value is used by Facebook as security check for login.
     * To get key hash value of your machine, write following code in onCreate method
     */
    fun getHashKey() {
        try {
            @SuppressLint("PackageManagerGetSignatures")
            val info = BaseApplication.getInstance().packageManager.getPackageInfo(
                BaseApplication.getInstance().packageName, PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
    }

    /*This method is open your keyboard on click of edittext
   * @param pass your context
   * @Param pass your edittext id
   */
    fun launchKeyboard(mContext: Activity?, mEditText: EditText) {
        mEditText.postDelayed({
            val keyboard =
                mContext!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(mEditText, 0)
        }, 100)
    }


    /*This method is hide your keyboard for particular view
     * @Param pass your view id which you want to hide
     */
    private fun hideKeyboard(v: View) {
        val mgr = BaseApplication.getInstance()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        mgr.hideSoftInputFromWindow(v.windowToken, 0)

    }

    /*This method is hide your keyboard on outside touch of screen
    * @Param pass your parent view id of activity of fragment
    */
    fun outSideTouchHideKeyboard(view: View) {
        // Set up touch listener for non-text box views to hideAnimateDialog keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { p0, p1 ->
                when (p1?.action) {
                    MotionEvent.ACTION_DOWN -> {}
                    MotionEvent.ACTION_UP -> p0?.performClick()
                    else -> {}
                }
                true
            }
        }

        // If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                outSideTouchHideKeyboard(innerView)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun hideKeyBoardWhenTouchOutside(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, _ ->
                hideKeyboard(v)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                hideKeyBoardWhenTouchOutside(innerView)
            }
        }
    }

    /**
     * This method give the App versionCode
     *
     * @return (int) version :  it return app version code e.g. 1, 2, 3
     * return version - e.g. 1, 2, 3
     */
    fun getAppVersionCode(): Int {
        var version = 0
        try {
            val pInfo = BaseApplication.getInstance()
                .packageManager.getPackageInfo(BaseApplication.getInstance().packageName, 0)
            version = pInfo.versionCode
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return version
    }

    /**
     * This method give the current date
     *
     * @return (string) currentDate : date formate is MMMM dd, yyyy
     */
    fun getCurrentDate(): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val currentDate = LocalDate.now()
            val formatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy")
            return currentDate.format(formatter)
        }
        return ""
    }

    /**
     * This method return the Application version
     *
     * @return (String) version : it return app version
     * return version - e.g. 1, 2, 3
     */
    fun getAppVersion(): String {
        var version = ""
        try {
            val pInfo = BaseApplication.getInstance()
                .packageManager.getPackageInfo(BaseApplication.getInstance().packageName, 0)
            version = pInfo.versionName
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return version
    }

    fun getTextRequestBody(value: String): RequestBody {
        return value.toRequestBody("text/plain".toMediaTypeOrNull())
    }

    fun getRequestBody(fileKey: String, value: String): MultipartBody.Part {
        return MultipartBody.Part.createFormData(fileKey, value)
    }

    fun getMultipartBody(fileKey: String, file: File): MultipartBody.Part {
        val fileReqBody: RequestBody = file.asRequestBody("*/*".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(fileKey, file.name, fileReqBody)
    }

    fun showConfirmationDialog(
        context: Context,
        headerText: String,
        title: String,
        negativeButtonText: String = "Cancel",
        positiveClick: View.OnClickListener
    ) {
        val dialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_confirmation, null).apply {
                findViewById<TextView>(R.id.tvAlertHeader).text = headerText
                findViewById<TextView>(R.id.tvAlertTitle).text = title
                findViewById<TextView>(R.id.tvNegative).text = negativeButtonText
            }

        val alertDialog = AlertDialog.Builder(context)
            .setView(dialogView)
            .setCancelable(false)
            .create()

        dialogView.apply {
            findViewById<TextView>(R.id.tvNegative).setOnClickListener { alertDialog.dismiss() }
            findViewById<TextView>(R.id.tvPositive).setOnClickListener {
                alertDialog.dismiss()
                positiveClick.onClick(it)
            }
        }
        alertDialog.show()
    }

    fun showNoInternetDialog(context: Context, tryAgainClick: View.OnClickListener) {
        val dialog = Dialog(context, R.style.AppTheme_Theme)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_no_internet, null)
        dialog.setContentView(view)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        view.findViewById<AppCompatButton>(R.id.btnTryAgain)
            .setOnClickListener {
                if (BaseApplication.getInstance().isConnectionAvailable()) {
                    dialog.dismiss()
                    tryAgainClick.onClick(it)
                }
            }
        dialog.show()
    }

    fun showFilterBottomDialog(context: Context, tryAgainClick: View.OnClickListener) {
        val dialog = BottomSheetDialog(context)
        val view = LayoutInflater.from(context).inflate(R.layout.sort_bottom_dialog, null)
        dialog.setCancelable(true)
        dialog.setContentView(view)
        view.findViewById<TextView>(R.id.tvReset).setOnClickListener {
            dialog.dismiss()
            tryAgainClick.onClick(it)
        }
        view.findViewById<TextView>(R.id.tvTitleAtoZ).setOnClickListener {
            dialog.dismiss()
            tryAgainClick.onClick(it)
        }
        view.findViewById<TextView>(R.id.tvTitleZtoA).setOnClickListener {
            dialog.dismiss()
            tryAgainClick.onClick(it)
        }
        view.findViewById<TextView>(R.id.tvPriceLowToHigh).setOnClickListener {
            dialog.dismiss()
            tryAgainClick.onClick(it)
        }
        view.findViewById<TextView>(R.id.tvPriceHighToLow).setOnClickListener {
            dialog.dismiss()
            tryAgainClick.onClick(it)
        }

        dialog.show()
    }

    fun loadImageUrl(context: Context, url: String, imageView: AppCompatImageView) {
        Glide.with(context)
            .asBitmap()
            .load(url).diskCacheStrategy(DiskCacheStrategy.NONE)
            .centerCrop()
            .skipMemoryCache(true)
            .into(imageView)
            .onLoadFailed(context.resources.getDrawable(R.drawable.ic_placeholder))
    }

    fun loadThumbnailImageUrl(context: Context, url: String, webView: WebView) {

        webView.settings.loadWithOverviewMode = true
        webView.settings.javaScriptEnabled = true
        webView.settings.loadsImagesAutomatically = true
        webView.webViewClient = WebViewClient()
        val url1 = "https://docs.google.com/gview?embedded=true&url=$url"
//            val url1 = "http://drive.google.com/viewerng/viewer?embedded=true&url="+url
//            val url1 = "https://docs.google.com/gview?embedded=true&url=http://vlpl.lrdevteam.com/storage/uploads/gst_document/22062720-corean-Learn-pdf.pdf"
        webView.loadUrl(url1)

    }

    // Function for convert html to string
    fun htmlToString(productDescription: String): String {
        var htmlString: Spanned? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            htmlString = HtmlCompat.fromHtml(productDescription, HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            htmlString = Html.fromHtml(productDescription)
        }
        return htmlString.toString()
    }

    fun openDial(
        context: Context,
        number: String = context.resources.getString(R.string.str_contact_number)
    ) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        context.startActivity(intent)
    }

    fun openEmail(context: Context) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", context.resources.getString(R.string.str_vlpl_connect_com), null
            )
        ).apply {
            putExtra(Intent.EXTRA_EMAIL, "address")
            putExtra(Intent.EXTRA_SUBJECT, "Subject")
            putExtra(Intent.EXTRA_TEXT, "Body")
        }
        context.startActivity(Intent.createChooser(emailIntent, "Send Email..."))
    }

    fun showOffersDialog(
        context: Context,
        image: String,
    ) {
        val builder = AlertDialog.Builder(context)
        val viewBinding = DialogViewOffersBinding.inflate(LayoutInflater.from(context))
        builder.setView(viewBinding.root)
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.setCanceledOnTouchOutside(false)

        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        val screenWidthPx: Int = displayMetrics.widthPixels
        val screenImageHeight = screenWidthPx * 0.2
        viewBinding.ivOffers.layoutParams.height = screenImageHeight.toInt()
        viewBinding.offerImage = image
        viewBinding.ivClose.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    fun getPermissionForSDK33(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            Manifest.permission.READ_MEDIA_IMAGES
        } else {
            Manifest.permission.READ_EXTERNAL_STORAGE
        }
    }
}