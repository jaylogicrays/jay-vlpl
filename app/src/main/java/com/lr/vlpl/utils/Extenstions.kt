package com.lr.vlpl.utils

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.lr.vlpl.R
import com.lr.vlpl.helper.LoginHelper

fun TextView.setPriceLableAccordingUserType() {
    text = if (LoginHelper.getInstance()?.getRole().equals("Stockiest")) {
        context.getString(R.string.str_your_price)
    } else {
        context.getString(R.string.str_your_price)
    }
}

fun TextView.setPriceLableInCartAccordingUserType() {
    text = if (LoginHelper.getInstance()?.getRole().equals("Stockiest")) {
        context.getString(R.string.str_your_amount)
    } else {
        context.getString(R.string.str_your_amount)
    }
}

fun AppCompatButton.setVisibilityForMR() {
    visibility = if (LoginHelper.getInstance()?.getRole().equals("MR")) {
        View.INVISIBLE
    } else {
        View.VISIBLE
    }
}

fun LinearLayout.setStockiestViewVisibility() {
    visibility = if (LoginHelper.getInstance()?.getRole().equals("Stockiest")) {
        View.GONE
    } else {
        View.VISIBLE
    }
}