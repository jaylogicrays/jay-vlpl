package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemSearchProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.utils.setPriceLableAccordingUserType

class ProductListAdapter : PagingDataAdapter<Products, ProductListAdapter.MyViewHolder>(DiffObj) {

    var onIncreaseQTY: ((
        products: Products,
        searchProductAdapter: ProductListAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null
    var onDecreaseQTY: ((
        products: Products,
        searchProductAdapter: ProductListAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null

    class MyViewHolder(binding: ItemSearchProductBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = getItem(position)
        holder.myBinding.tvPriceLable.setPriceLableAccordingUserType()
        holder.myBinding.ivIncrementCart.setOnClickListener {
            incrementCartQTY(getItem(position)!!, position, holder.myBinding)
        }
        holder.myBinding.ivDecrementCart.setOnClickListener {
            decrementCartQTY(getItem(position)!!, position, holder.myBinding)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemSearchProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    object DiffObj : DiffUtil.ItemCallback<Products>() {
        override fun areItemsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem == newItem
        }
    }

    private fun decrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemSearchProductBinding
    ) {
        if (products.cart_qty > 0) {
            onDecreaseQTY!!.invoke(products, ProductListAdapter(), myBinding, position)
        }
    }

    private fun incrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemSearchProductBinding
    ) {
        onIncreaseQTY!!.invoke(products, ProductListAdapter(), myBinding, position)
    }

    fun increaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemSearchProductBinding
    ) {
        products.cart_qty += products.ratio
        products.cart_free_qty += products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty + (products.ratio + products.free_product_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence + 1
        }
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }

    fun decreaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemSearchProductBinding
    ) {
        products.cart_qty -= products.ratio
        products.cart_free_qty -= products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty - (products.ratio + products.free_product_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence - 1
        }
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }
}
