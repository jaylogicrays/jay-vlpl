package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemNotificationBinding
import com.lr.vlpl.model.Notification

class NotificationAdapter :
    ListAdapter<Notification, NotificationAdapter.MyViewHolder>(DiffUtil()) {

    class MyViewHolder(binding: ItemNotificationBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Notification>() {
        override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.items == newItem.items
        }

        override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return (oldItem == newItem && (oldItem.date == newItem.date))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.notification = getItem(position)
        if (position == 0) {
            holder.myBinding.rootlayout.setPadding(16, 20, 16, 0);
        } else {
            holder.myBinding.rootlayout.setPadding(16, 0, 16, 0);
        }
        val adapter = NotificationDetailAdapter()
        holder.myBinding.rvNotification.adapter = adapter
        adapter.submitList(getItem(position).items)
    }
}