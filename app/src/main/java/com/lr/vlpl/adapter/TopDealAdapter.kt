package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemTopDealProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.utils.setPriceLableAccordingUserType

class TopDealAdapter(list: List<Products>) :
    RecyclerView.Adapter<TopDealAdapter.MyViewHolder>() {

    private val productList = list

    var onProductClick: ((position: Products) -> Unit)? = null

    class MyViewHolder(binding: ItemTopDealProductBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemTopDealProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = productList[position]
        holder.myBinding.tvPriceLable.setPriceLableAccordingUserType()
        holder.myBinding.root.setOnClickListener {
            onProductClick?.invoke(productList[position])
        }
    }

    override fun getItemCount() = productList.size
}