package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemNotificationDetailsBinding
import com.lr.vlpl.model.NotificationDetail

class NotificationDetailAdapter :
    ListAdapter<NotificationDetail, NotificationDetailAdapter.MyViewHolder>(DiffUtil()) {

    class MyViewHolder(binding: ItemNotificationDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<NotificationDetail>() {
        override fun areItemsTheSame(
            oldItem: NotificationDetail,
            newItem: NotificationDetail
        ): Boolean {
            return oldItem.body == newItem.body
        }

        override fun areContentsTheSame(
            oldItem: NotificationDetail,
            newItem: NotificationDetail
        ): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemNotificationDetailsBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.notificationDetail = getItem(position)
    }
}