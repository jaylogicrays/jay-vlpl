package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemSearchBinding
import com.lr.vlpl.databinding.ItemSearchProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.model.SearchProduct

class SearchCategoryAdapter :
    ListAdapter<SearchProduct, SearchCategoryAdapter.MyViewHolder>(DiffUtil()) {

    var onIncreaseQTY: ((
        products: Products,
        searchProductAdapter: SearchProductAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null
    var onDecreaseQTY: ((
        products: Products,
        searchProductAdapter: SearchProductAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null

    class MyViewHolder(binding: ItemSearchBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<SearchProduct>() {
        override fun areItemsTheSame(oldItem: SearchProduct, newItem: SearchProduct): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: SearchProduct, newItem: SearchProduct): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.searchCategoryList = currentList[position]
        val adapter = SearchProductAdapter()
        adapter.submitList(getItem(position).product_info)
        holder.myBinding.rvSearch.adapter = adapter

        adapter.onIncreaseQTY = { products, searchProductAdapter, myBinding, myPosition ->
            onIncreaseQTY?.invoke(products, searchProductAdapter, myBinding, myPosition)
        }
        adapter.onDecreaseQTY = { products, searchProductAdapter, myBinding, myPosition ->
            onDecreaseQTY?.invoke(products, searchProductAdapter, myBinding, myPosition)
        }
    }
}