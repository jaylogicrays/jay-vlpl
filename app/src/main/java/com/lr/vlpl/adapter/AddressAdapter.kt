package com.lr.vlpl.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemAddressBinding
import com.lr.vlpl.model.Address

class AddressAdapter(list: List<Address>) :
    RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {

    private val productList = list
    private var getAddressId: ((address_id: Int) -> Unit)? = null

    class MyViewHolder(binding: ItemAddressBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemAddressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.addressList = productList[position]

        if (productList[position].is_primary == 1) {
            getAddressId?.invoke(productList[position].id)
        }

        holder.myBinding.root.setOnClickListener {
            for (i in productList.indices) {
                productList[i].is_primary = 0
            }
            productList[position].is_primary = 1
            getAddressId?.invoke(productList[position].id)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = productList.size
}