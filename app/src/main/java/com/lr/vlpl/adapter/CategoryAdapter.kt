package com.lr.vlpl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.R
import com.lr.vlpl.databinding.ItemCategoryBinding
import com.lr.vlpl.model.Category
import java.util.*


class CategoryAdapter(val context: Context, private val listSize: Int) :
    ListAdapter<Category, CategoryAdapter.MyViewHolder>(DiffUtil()) {

    // first define colors
    private val backgroundColors = intArrayOf(
        R.color.colorSubLightPink,
        R.color.colorSubGreen,
        R.color.colorSubSkyLightBlue,
        R.color.colorSubLightBlue
    )

    var onCategoryClick: ((position: Category) -> Unit)? = null

    class MyViewHolder(binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.categoryList = getItem(position)
        val index: Int = position % backgroundColors.size
        val color = ContextCompat.getColor(context, backgroundColors[index])
        holder.myBinding.tvCategoryName.text = getItem(position).title.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString()
        }
        val metrics = context.resources.displayMetrics
        when (listSize) {
            1 -> {}
            2, 3, 4 -> {
                holder.myBinding.cvCategory.layoutParams.width = metrics.widthPixels / 2 - 70
            }
            else -> {
                holder.myBinding.cvCategory.layoutParams.width = metrics.widthPixels / 2 - 95
            }
        }
        holder.myBinding.cvCategory.setCardBackgroundColor(color)
        holder.myBinding.root.setOnClickListener { onCategoryClick?.invoke(getItem(position)) }
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem == newItem
        }
    }
}