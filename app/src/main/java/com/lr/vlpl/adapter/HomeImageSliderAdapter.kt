package com.lr.vlpl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.lr.vlpl.R
import com.lr.vlpl.model.Banner
import com.lr.vlpl.utils.setHomeBannerImage

class HomeImageSliderAdapter(val context: Context, val photo: List<Banner>) : PagerAdapter() {

    private var inflater: LayoutInflater? = null
    var bannerClick: ((productSlug: Int) -> Unit)? = null

    override fun getCount(): Int {
        return photo.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater!!.inflate(R.layout.item_home_slider_image, null)
        val ivSliderImage: AppCompatImageView? = view.findViewById(R.id.ivSliderImage)
        ivSliderImage!!.setHomeBannerImage(photo[position].photo)
//        ivSliderImage.setOnClickListener { bannerClick?.invoke(photo[position].product_id) }
        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}