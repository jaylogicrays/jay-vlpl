package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemHomeArticleBinding
import com.lr.vlpl.model.Articles

class HomeArticleAdapter(private val blog: List<Articles>) :
    RecyclerView.Adapter<HomeArticleAdapter.MyViewHolder>() {

    var rootClick: ((articleList: Articles) -> Unit)? = null

    class MyViewHolder(binding: ItemHomeArticleBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.articleList = blog[position]
        holder.myBinding.root.setOnClickListener {
            rootClick!!.invoke(blog[position])
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemHomeArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return blog.size
    }
}