package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemOrderHistoryBinding
import com.lr.vlpl.model.Order
import com.lr.vlpl.utils.Utility

class OrderAdapter : ListAdapter<Order, OrderAdapter.MyViewHolder>(DiffUtil()) {

    var orderId: ((orderId: Int) -> Unit)? = null
    var reorder: ((orderId: Int) -> Unit)? = null
    var uploadInvoice: ((order: Order, position: Int, itemOrderHistory: ItemOrderHistoryBinding) -> Unit)? =
        null

    class MyViewHolder(binding: ItemOrderHistoryBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Order>() {
        override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemOrderHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.orderList = currentList[position]
        holder.myBinding.root.setOnClickListener {
            orderId!!.invoke(currentList[position].id)
        }
        holder.myBinding.cvReorder.setOnClickListener {
            reorder!!.invoke(currentList[position].id)
        }
        holder.myBinding.cvUploadInvoice.setOnClickListener {
            uploadInvoice!!.invoke(
                currentList[position],
                position,
                holder.myBinding
            )
        }
    }

    fun updateIsInvoiceUploadedValue(
        order: Order,
        position: Int,
        itemOrderHistory: ItemOrderHistoryBinding
    ) {
        if (order.invoice_already_uploaded == 0) {
            order.delivered = Utility.getInstance().getCurrentDate()
        }
        order.apply {
            invoice_already_uploaded = 1
            status = "delivered"
        }
        itemOrderHistory.orderList = order
        notifyItemChanged(position)
    }
}