package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemOrderDetailsBinding
import com.lr.vlpl.model.Products

class OrderDetailProductAdapter :
    ListAdapter<Products, OrderDetailProductAdapter.MyViewHolder>(DiffUtil()) {

    var tvAddReviewClick: ((productInfo: Products, tvAddReview: TextView) -> Unit)? = null

    class MyViewHolder(binding: ItemOrderDetailsBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Products>() {
        override fun areItemsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemOrderDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = currentList[position]
    }
}