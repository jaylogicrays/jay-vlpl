package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemManageAddressBinding
import com.lr.vlpl.model.Address

class ManageAddressAdapter :
    ListAdapter<Address, ManageAddressAdapter.MyViewHolder>(DiffUtil()) {

    var addressDetail: ((position: Address) -> Unit)? = null

    class MyViewHolder(binding: ItemManageAddressBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Address>() {
        override fun areItemsTheSame(oldItem: Address, newItem: Address): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Address, newItem: Address): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemManageAddressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.addressList = getItem(position)
        holder.myBinding.tvEditIcon.setOnClickListener { addressDetail?.invoke(currentList[position]) }
    }
}
