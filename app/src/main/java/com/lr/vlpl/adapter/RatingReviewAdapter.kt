package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemRatingReviewBinding
import com.lr.vlpl.model.ReviewsData

class RatingReviewAdapter(list: List<ReviewsData>) :
    RecyclerView.Adapter<RatingReviewAdapter.MyViewHolder>() {

    private val reviewList = list

    class MyViewHolder(binding: ItemRatingReviewBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemRatingReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.ratingReviewList = reviewList[position]
        if (position == reviewList.size - 1) {
            holder.myBinding.vLine.visibility = View.GONE
        }
    }

    override fun getItemCount() = reviewList.size
}