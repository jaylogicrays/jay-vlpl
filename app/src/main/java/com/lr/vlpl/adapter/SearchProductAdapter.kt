package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemSearchProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.utils.setPriceLableAccordingUserType

class SearchProductAdapter :
    ListAdapter<Products, SearchProductAdapter.MyViewHolder>(DiffUtil()) {

    var onIncreaseQTY: ((
        products: Products,
        searchProductAdapter: SearchProductAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null
    var onDecreaseQTY: ((
        products: Products,
        searchProductAdapter: SearchProductAdapter,
        myBinding: ItemSearchProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null

    class MyViewHolder(binding: ItemSearchProductBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemSearchProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = currentList[position]
        holder.myBinding.listSize = currentList.size
        holder.myBinding.tvPriceLable.setPriceLableAccordingUserType()
        holder.myBinding.ivIncrementCart.setOnClickListener {
            incrementCartQTY(currentList[position], position, holder.myBinding)
        }
        holder.myBinding.ivDecrementCart.setOnClickListener {
            decrementCartQTY(currentList[position], position, holder.myBinding)
        }
    }

    private fun decrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemSearchProductBinding
    ) {
        if (products.cart_qty > 0) {
            onDecreaseQTY!!.invoke(products, SearchProductAdapter(), myBinding, position)
        }
    }

    private fun incrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemSearchProductBinding
    ) {
        onIncreaseQTY!!.invoke(products, SearchProductAdapter(), myBinding, position)
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Products>() {
        override fun areItemsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Products, newItem: Products): Boolean {
            return oldItem == newItem
        }
    }

    fun increaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemSearchProductBinding
    ) {
        products.cart_qty += products.ratio
        products.cart_free_qty += products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty + (products.ratio + products.free_product_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence + 1
        }
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }

    fun decreaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemSearchProductBinding
    ) {
        products.cart_qty -= products.ratio
        products.cart_free_qty -= products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty - (products.ratio + products.free_product_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence - 1
        }
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }
}
