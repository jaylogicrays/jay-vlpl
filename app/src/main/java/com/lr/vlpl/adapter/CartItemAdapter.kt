package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.databinding.ItemAddCartProductBinding
import com.lr.vlpl.model.CartProduct
import com.lr.vlpl.model.Products
import com.lr.vlpl.utils.setPriceLableAccordingUserType
import com.lr.vlpl.viewmodel.CartViewModel

class CartItemAdapter(private val list: List<CartProduct>, val viewModel: CartViewModel) :
    RecyclerView.Adapter<CartItemAdapter.MyViewHolder>() {

    var onIncreaseQTY: ((
        products: Products,
        itemAddCartProductBinding: ItemAddCartProductBinding,
        position: Int
    ) -> Unit)? = null

    var onDecreaseQTY: ((
        products: Products,
        itemAddCartProductBinding: ItemAddCartProductBinding,
        position: Int
    ) -> Unit)? = null

    class MyViewHolder(binding: ItemAddCartProductBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemAddCartProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = list[position].product
        holder.myBinding.position = position
        holder.myBinding.vm = viewModel
        holder.myBinding.tvPriceLable.setPriceLableAccordingUserType()

        holder.myBinding.ivDeleteProduct.setOnClickListener {
            BaseApplication.getInstance().from = "delete"
            viewModel.deleteProduct(list[position].id, list[position].product.id)
        }
        holder.myBinding.ivIncrementCart.setOnClickListener {
            incrementCartQTY(list[position].product, holder.myBinding, position)
        }
        holder.myBinding.ivDecrementCart.setOnClickListener {
            decrementCartQTY(list[position].product, holder.myBinding, position)
        }
    }

    private fun incrementCartQTY(
        products: Products,
        myBinding: ItemAddCartProductBinding,
        position: Int
    ) {
        onIncreaseQTY?.invoke(products, myBinding, position)
    }

    private fun decrementCartQTY(
        products: Products,
        myBinding: ItemAddCartProductBinding,
        position: Int
    ) {
        if (products.cart_qty > products.ratio) {
            onDecreaseQTY?.invoke(products, myBinding, position)
        } else {
            BaseApplication.getInstance().from = "delete"
            BaseApplication.getInstance().productId.set(list[position].product_id)
            viewModel.deleteProductFromCart(cartID = list[position].id)
        }
    }

    override fun getItemCount() = list.size

    fun onIncreaseQTYCalculation(
        products: Products,
        adapterPosition: Int,
        myItemAddCartProductBinding: ItemAddCartProductBinding
    ) {
        products.cart_qty += products.ratio
        products.cart_free_qty += products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty + (products.ratio + products.free_product_qty)
        BaseApplication.getInstance().productFreeQTY.set(products.cart_free_qty)
        BaseApplication.getInstance().productTotalQTY.set(products.total_cart_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence + 1
        }
        BaseApplication.getInstance().sequence.set(products.sequence)
        BaseApplication.getInstance().searchScreenUpdate.set(!BaseApplication.getInstance().searchScreenUpdate.get())
        myItemAddCartProductBinding.productList = products
        notifyItemChanged(adapterPosition)
    }

    fun onDecreaseQTYCalculation(
        products: Products,
        adapterPosition: Int,
        myItemAddCartProductBinding: ItemAddCartProductBinding
    ) {
        products.cart_qty -= products.ratio
        products.cart_free_qty -= products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty - (products.ratio + products.free_product_qty)
        BaseApplication.getInstance().productFreeQTY.set(products.cart_free_qty)
        BaseApplication.getInstance().productTotalQTY.set(products.total_cart_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence - 1
        }
        BaseApplication.getInstance().sequence.set(products.sequence)
        BaseApplication.getInstance().searchScreenUpdate.set(!BaseApplication.getInstance().searchScreenUpdate.get())
        myItemAddCartProductBinding.productList = products
        notifyItemChanged(adapterPosition)
    }
}