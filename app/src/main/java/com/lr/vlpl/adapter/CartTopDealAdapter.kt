package com.lr.vlpl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.BaseApplication
import com.lr.vlpl.databinding.ItemCartTopDealProductBinding
import com.lr.vlpl.model.Products
import com.lr.vlpl.utils.setPriceLableAccordingUserType
import com.lr.vlpl.viewmodel.CartViewModel

class CartTopDealAdapter(list: List<Products>, val viewModel: CartViewModel) :
    RecyclerView.Adapter<CartTopDealAdapter.MyViewHolder>() {

    private val productList = list
    var onIncreaseQTY: ((
        products: Products,
        searchProductAdapter: CartTopDealAdapter,
        myBinding: ItemCartTopDealProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null
    var onDecreaseQTY: ((
        products: Products,
        searchProductAdapter: CartTopDealAdapter,
        myBinding: ItemCartTopDealProductBinding,
        myPosition: Int
    ) -> Unit)? =
        null

    class MyViewHolder(binding: ItemCartTopDealProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemCartTopDealProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = productList[position]
        holder.myBinding.tvPriceLable.setPriceLableAccordingUserType()
        holder.myBinding.ivIncrementCart.setOnClickListener {
            incrementCartQTY(productList[position], position, holder.myBinding)
        }
        holder.myBinding.ivDecrementCart.setOnClickListener {
            decrementCartQTY(productList[position], position, holder.myBinding)
        }
    }

    override fun getItemCount() = productList.size

    private fun decrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemCartTopDealProductBinding
    ) {
        if (products.cart_qty > products.ratio) {
            onDecreaseQTY?.invoke(products, this, myBinding, position)
        } else {
            BaseApplication.getInstance().from = "delete"
            BaseApplication.getInstance().productId.set(products.id)
            viewModel.deleteProductFromCart(cartID = products.cart_id)
        }
    }

    private fun incrementCartQTY(
        products: Products,
        position: Int,
        myBinding: ItemCartTopDealProductBinding
    ) {
        onIncreaseQTY!!.invoke(products, this, myBinding, position)
    }

    fun increaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemCartTopDealProductBinding
    ) {
        products.cart_qty += products.ratio
        products.cart_free_qty += products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty + (products.ratio + products.free_product_qty)
        BaseApplication.getInstance().productFreeQTY.set(products.cart_free_qty)
        BaseApplication.getInstance().productTotalQTY.set(products.total_cart_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence + 1
        }
        BaseApplication.getInstance().sequence.set(products.sequence)
        BaseApplication.getInstance().searchScreenUpdate.set(!BaseApplication.getInstance().searchScreenUpdate.get())
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }

    fun decreaseCartValue(
        products: Products,
        position: Int,
        myItemSearchBinding: ItemCartTopDealProductBinding
    ) {
        products.cart_qty -= products.ratio
        products.cart_free_qty -= products.free_product_qty
        products.total_cart_qty =
            products.total_cart_qty - (products.ratio + products.free_product_qty)
        BaseApplication.getInstance().productFreeQTY.set(products.cart_free_qty)
        BaseApplication.getInstance().productTotalQTY.set(products.total_cart_qty)
        if (products.discount_type == 3) {
            products.sequence = products.sequence - 1
        }
        BaseApplication.getInstance().sequence.set(products.sequence)
        BaseApplication.getInstance().searchScreenUpdate.set(!BaseApplication.getInstance().searchScreenUpdate.get())
        myItemSearchBinding.productList = products
        notifyItemChanged(position)
    }
}