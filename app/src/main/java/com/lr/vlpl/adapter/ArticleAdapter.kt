package com.lr.vlpl.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.lr.vlpl.databinding.ItemArticleBinding
import com.lr.vlpl.model.Articles

class ArticleAdapter : PagingDataAdapter<Articles, ArticleAdapter.MyViewHolder>(DiffObj) {

    var rootClick: ((articleSlug: String) -> Unit)? = null

    class MyViewHolder(binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.articleList = getItem(position)
        holder.myBinding.root.setOnClickListener {
            rootClick?.invoke(getItem(position)!!.slug)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    object DiffObj : DiffUtil.ItemCallback<Articles>() {
        override fun areItemsTheSame(oldItem: Articles, newItem: Articles): Boolean {
            return oldItem.slug == newItem.slug
        }

        override fun areContentsTheSame(oldItem: Articles, newItem: Articles): Boolean {
            return oldItem == newItem
        }
    }
}